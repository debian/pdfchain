/*
 * window_main_bgst.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_BGST_H__
#define __PDFCHAIN_WINDOW_MAIN_BGST_H__

#include "pdfchain.h"
#include "dialog_filechooser.h"


/*** Section : Background / Stamp *********************************************/

class
cSection_BgSt : public Gtk::VBox
{
	public:
	
		// Constructor
		         cSection_BgSt( Gtk::Window& );
		virtual ~cSection_BgSt();

		// Methodes
		void clear();
		std::string createCommand();
		
	private:

		// Methodes 
		void init();

		// Signal Handler
		inline void onFCButton_BgStFile_file_set();

		// Widgets
		Gtk::HBox
			mHBox_SourceFile ,
			mHBox_BgSt;

		Gtk::Label
			mLabel_SourceFile ,
			mLabel_BgStFile;

		Gtk::Frame
			mFrame_BgSt;

		Gtk::Table
			mTable_BgSt;

		Gtk::CheckButton
			mCButton_Multiple;

		Gtk::RadioButton			// Declaration of Gtk::RadioButton before Gtk::RadioButtonGroup!!!
			mRButton_Background ,
			mRButton_Stamp;

		Gtk::RadioButtonGroup		// Declaration of Gtk::RadioButtonGroup behind Gtk::RadioButton!!!
			mRBGroup_BgSt;

		// Derived Widgets
		cFCButton_Pdf
			mFCButton_SourceFile ,
			mFCButton_BgStFile;

		cFCDialog_SaveAs_Pdf
			mFCDialog_SaveAs;
};


#endif

/*
 * window_main.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main.h"


/*** Notebook Sections ********************************************************/

// Constructor
cNotebook_Sections::cNotebook_Sections( Gtk::Window& ref_window )
:
	mSection_Cat(    ref_window ) ,
	mSection_Burst(  ref_window ) ,
	mSection_BgSt(   ref_window ) ,
	mSection_Attach( ref_window ) ,
	mSection_Tool(   ref_window ) ,

	mLabel_Cat(    _("_Catenate")           , true ) ,	// label , mnemonic
	mLabel_Burst(  _("_Burst")              , true ) ,
	mLabel_BgSt(   _("Background / _Stamp") , true ) ,
	mLabel_Attach( _("Attach_ment")         , true ) ,
	mLabel_Tool(   _("_Tools")              , true )
{
	set_scrollable( true );

	mLabel_Cat.set_tooltip_text(    _("Combines selected pages of various documents into a single output document") );
	mLabel_Burst.set_tooltip_text(  _("Splits a single document into individual pages") );
	mLabel_BgSt.set_tooltip_text(   _("Prints a background or a stamp layer on each page of a single document") );
	mLabel_Attach.set_tooltip_text( _("Attaches several files to a single document") );
	mLabel_Tool.set_tooltip_text(   _("Some different tools to work with a single document") );
	
	mAlign_Cat.set_padding(    0                , 0                , 0                , 0                );
	mAlign_Burst.set_padding(  Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER );
	mAlign_BgSt.set_padding(   Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER );
	mAlign_Attach.set_padding( 0                , 0                , 0                , 0                );
	mAlign_Tool.set_padding(   0                , 0                , 0                , 0                );

	mAlign_Cat.add(    mSection_Cat );
	mAlign_Burst.add(  mSection_Burst );
	mAlign_BgSt.add(   mSection_BgSt );
	mAlign_Attach.add( mSection_Attach );
	mAlign_Tool.add(   mSection_Tool );
	
	append_page( mAlign_Cat    , mLabel_Cat );	// child_widget , tab_label_widget
	append_page( mAlign_Burst  , mLabel_Burst );
	append_page( mAlign_BgSt   , mLabel_BgSt );
	append_page( mAlign_Attach , mLabel_Attach );
	append_page( mAlign_Tool   , mLabel_Tool );
}


// Destructor (virtual)
cNotebook_Sections::~cNotebook_Sections()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cNotebook_Sections::~cNotebook_Sections()";	//TEST
#endif
}

// Method (public) : clear()
void
cNotebook_Sections::clear()
{
	mSection_Cat.clear();
	mSection_Burst.clear();
	mSection_BgSt.clear();
	mSection_Attach.clear();
	mSection_Tool.clear();
	
	set_current_page( 0 );
	return;
}


/*** HBox : Execute ***********************************************************/

// Constructor
cHBox_Execute::cHBox_Execute()
:
	Gtk::HBox( false , Pdfchain::SPACING ) ,	// homogenous , spacing
#ifndef PDFCHAIN_GTKMM_OLD
	mIMItem_About( Gtk::Stock::ABOUT ) ,
	mIMItem_Help(  Gtk::Stock::HELP ) ,
	mIMItem_Clear( Gtk::Stock::CLEAR ) ,
#else
	mImg_About( Gtk::Stock::ABOUT , Gtk::ICON_SIZE_BUTTON ) ,	// stock id , size
	mImg_Help ( Gtk::Stock::HELP  , Gtk::ICON_SIZE_BUTTON ) ,
	mImg_Clear( Gtk::Stock::CLEAR , Gtk::ICON_SIZE_BUTTON ) ,
#endif
	mTButton_Permission( _("_Permissions") , true ) ,	// label, mnemonic

//	mButton_Execute( Gtk::Stock::EXECUTE )	//TODO: Change button dependent on notebook section
	mButton_Execute( Gtk::Stock::SAVE_AS )
{
	mTButton_Permission.set_tooltip_text(
		_("Sets passwords, encryption and restrictions for the output document") );
#ifndef PDFCHAIN_GTKMM_OLD
	mMenu.append( mIMItem_About );
	mMenu.append( mIMItem_Help );
	mMenu.append( mMISeparator_A );
	mMenu.append( mIMItem_Clear );
	mMenu.show_all_children();

	mMButton.set_direction( Gtk::ARROW_UP );
	mMButton.set_popup( mMenu );
	pack_start( mMButton            , false , false , 0 );	// widget , expand , fill , padding
#else
	mButton_About.set_image( mImg_About );
	mButton_Help.set_image(  mImg_Help );
	mButton_Clear.set_image( mImg_Clear );

	mButton_About.set_tooltip_text( _("About") );
	mButton_About.set_tooltip_text( _("Online Help") );
	mButton_Clear.set_tooltip_text( _("Clear") );

	pack_start( mButton_About       , false , false , 0 );
	pack_start( mButton_Help       , false , false , 0 );
	pack_start( mButton_Clear       , false , false , 0 );
#endif
	pack_start( mTButton_Permission , false , false , 0 );	// widget , expand , fill , padding
	pack_start( mSBar_Main          , true  , true  , 0 );
	pack_start( mButton_Execute     , false , false , 0 );
	
	Pdfchain::pSBar = &mSBar_Main;
}


// Destructor (virtual)
cHBox_Execute::~cHBox_Execute()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cHBox_Execute::~cHBox_Execute()";	//TEST
#endif
}



/*** Window : Main ************************************************************/

// Constructor
cWindow_Main::cWindow_Main()
:
	mNotebook_Sections( *this ) ,
	mDialog_About( *this ) ,
	mMDialog_PdftkError( *this )
{
	// Set Icon
	Glib::RefPtr<Gio::File>	ref_icon_path_A =
		Gio::File::create_for_path( Pdfchain::APPICON_PATH_A );
		
	Glib::RefPtr<Gio::File>	ref_icon_path_B =
		Gio::File::create_for_path( Pdfchain::APPICON_PATH_B );

	if		( ref_icon_path_A->query_exists() ) {
		set_default_icon_from_file( Pdfchain::APPICON_PATH_A );
	}
	else if	( ref_icon_path_B->query_exists() ) {
		set_default_icon_from_file( Pdfchain::APPICON_PATH_B );
	}
	else {
		std::cerr << std::endl << "Icon file doesn't exist! " <<
			Pdfchain::APPICON_PATH_A << " , " << Pdfchain::APPICON_PATH_B;
	}

	// Setup Window
	set_title( Pdfchain::Window::TITLE );
	set_default_size( Pdfchain::Window::WIDTH , Pdfchain::Window::HEIGHT );
	set_position( Gtk::WIN_POS_CENTER );
	
	mAlign_Sections.set_padding( 0 , 0 , 0 , 0 );	// top , bottom , left , right
	mAlign_Permission.set_padding( 0 , 0 , Pdfchain::BORDER , Pdfchain::BORDER );
	mAlign_Execute.set_padding( Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER , Pdfchain::BORDER );
	
	mAlign_Sections.add(   mNotebook_Sections );
	mAlign_Permission.add( mSection_Permission );
	mAlign_Execute.add(    mHBox_Execute );

	mVBox_Main.pack_start( mAlign_Sections   , true  , true  , 0 );	// widget , expand , fill , padding
	mVBox_Main.pack_start( mAlign_Permission , false , false , 0 );
	mVBox_Main.pack_start( mAlign_Execute    , false , false , 0 );
	
	add( mVBox_Main );

	// Connecting Signal Handler
#ifndef PDFCHAIN_GTKMM_OLD
	mHBox_Execute.mIMItem_About.signal_activate().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_About_activated ) );

	mHBox_Execute.mIMItem_Help.signal_activate().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_Help_activated ) );

	mHBox_Execute.mIMItem_Clear.signal_activate().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_Clear_activated ) );
#else
	mHBox_Execute.mButton_About.signal_clicked().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_About_activated ) );
	
	mHBox_Execute.mButton_Help.signal_clicked().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_Help_activated ) );
	
	mHBox_Execute.mButton_Clear.signal_clicked().connect(
		sigc::mem_fun( *this , &cWindow_Main::onIMItem_Clear_activated ) );
#endif
	mHBox_Execute.mTButton_Permission.signal_toggled().connect(
		sigc::mem_fun( *this , &cWindow_Main::onTButton_Permission_toggled ) );

	mHBox_Execute.mButton_Execute.signal_clicked().connect(
		sigc::mem_fun( *this , &cWindow_Main::onButton_Execute_clicked ) );

	mConnection_NotebookSections_signal_switch_page =
		mNotebook_Sections.signal_switch_page().connect(
			sigc::mem_fun( *this , &cWindow_Main::onNotebook_Sections_switch_page ) );
	
	show_all_children();
	init();
}


// Destructor (virtual)
cWindow_Main::~cWindow_Main()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cWindow_Main::~cWindow_Main()";	//TEST
#endif
	
	// The signal handler cWindow_Main::onNotebook_Sections_switch_page calls
	// methodes (show() and hide()) at the mSection_Permission object.
	// While destructing the mNotebooSections object, it is possible the signal
	// Gtk::Notebook::signal_switch_page() emits, while the mSection_Permission
	// object is already destroyed.
	// To prevent a segmentation fault, the signal handler becomes
	// explicitly disconnected from the signal.
	mConnection_NotebookSections_signal_switch_page.disconnect();
}


// Method (public) : clear()
void
cWindow_Main::clear()
{
	mNotebook_Sections.clear();
	mSection_Permission.clear();
//	mHBox_Execute.clear();

	init();
	return;
}


// Method (protected) : init()
void
cWindow_Main::init()
{
	mSection_Permission.hide();
	mHBox_Execute.mTButton_Permission.set_active( false );
	return;
}


// Method (protected) : confirm pdftk error code
void
cWindow_Main::confirmErrorCode(
	const int error_code )
{
	std::cout << "RETURN: " << error_code << " - ";

	switch ( error_code ) {
	
		case 0:
			std::cout << "Successfully executed!";
			break;
		
		case 3:
			std::cout << "Warning: PDF information not added or PDF form not filled";

			mMDialog_PdftkError.popupMessage(
				_("Warning: PDF information not added or PDF form not filled") , error_code );
			break;

		case 256:
			std::cout << "Input / Output error!";

			mMDialog_PdftkError.popupMessage(
				_("Input / Output error!") , error_code );
			break;
			
		case 512:
			std::cout << "Unhandled Java excepton in PDFtk!";

			mMDialog_PdftkError.popupMessage(
				_("Unhandled Java exception in PDFtk!") , error_code );
			break;

		case 768:
			std::cout << "Warning: input PDF is not an acroform, so its fields were not filled!";

			mMDialog_PdftkError.popupMessage(
				_("Warning: input PDF is not an acroform, so its fields were not filled!") , error_code );
			break;
			
		case 32512:
			std::cout << "PDFtk not found!";

			mMDialog_PdftkError.popupMessage(
				_("PDFtk not found!") , error_code );
			break;
			
		default:
			mMDialog_PdftkError.popupMessage(
				_("Unknown Error") , error_code );
			break;
	}

	std::cout << std::endl << std::endl;
	return;
}


// Signal Handler (protected) : on Button About clicked
void
cWindow_Main::onIMItem_About_activated()
{
	mDialog_About.run();
	mDialog_About.hide();
	return;
}


// Signal Handler (protected) : on Button Help clicked
void
cWindow_Main::onIMItem_Help_activated()
{
	system( "xdg-open http://pdfchain.sourceforge.net/documentation.html" );
	return;
}


// Signal Handler (protected) : on Button Clear clicked
void
cWindow_Main::onIMItem_Clear_activated()
{
	cWindow_Main::clear();
	return;
}


// Signal Handler (protected) : on Toggle Button Permission toggled
void
cWindow_Main::onTButton_Permission_toggled()
{
	if ( true != mHBox_Execute.mTButton_Permission.get_active() ) {
		mSection_Permission.hide();
	} else {
		mSection_Permission.show();
	}

	return;
}	


// Signal Handler (protected) : on Button Execute clicked
void
cWindow_Main::onButton_Execute_clicked()
{
	std::string
		str_command ,
		str_section;
		
	int error_code  = 0;
	
	const Gtk::Widget* pt_page =
		mNotebook_Sections.get_nth_page( mNotebook_Sections.get_current_page() );

	if ( pt_page == &(mNotebook_Sections.mAlign_Cat) ) {
		str_section = mNotebook_Sections.mSection_Cat.createCommand();
	}
	else if ( pt_page == &(mNotebook_Sections.mAlign_Burst) ) {
		str_section = mNotebook_Sections.mSection_Burst.createCommand();
	}
	else if ( pt_page == &(mNotebook_Sections.mAlign_BgSt) ) {
		str_section = mNotebook_Sections.mSection_BgSt.createCommand();
	}
	else if ( pt_page == &(mNotebook_Sections.mAlign_Attach) ) {
		str_section = mNotebook_Sections.mSection_Attach.createCommand();
	}
	else if ( pt_page == &(mNotebook_Sections.mAlign_Tool) ) {
		str_section = mNotebook_Sections.mSection_Tool.createCommand();
	}
	else {
		std::cerr << std::endl << 
			"ERROR: At method cWindow_Main::onButton_Execute_clicked() \
			 - no valid mNotebook_Sections.get_current_page() can't be found!";
	}
	
	if ( ! str_section.empty() ) {
	
		str_command  = Pdfchain::Cmd::PDFTK;
		str_command += str_section;

		if ( pt_page != &(mNotebook_Sections.mAlign_Tool) )
			str_command += mSection_Permission.createCommand();

		confirmErrorCode( Pdfchain::execute_command( str_command ) );
	}

	return;
}


// Signal Handler (protected) : on Notebook Sections switch page
void
cWindow_Main::onNotebook_Sections_switch_page(
	const Widget* pt_page ,
	const guint page_num )
{
/*
	if ( &(mNotebook_Sections.mAlign_Cat) == pt_page ) {
	
	}
	else if ( &(mNotebook_Sections.mAlign_Burst) == pt_page ) {

	}
	else if ( &(mNotebook_Sections.mAlign_BgSt) == pt_page ) {

	}
	else if ( &(mNotebook_Sections.mAlign_Attach) == pt_page ) {

	}
	else if ( &(mNotebook_Sections.mAlign_Tool) == pt_page ) {

	}
	else {

	}
*/

	if ( pt_page == &(mNotebook_Sections.mAlign_Tool) ) {
		mHBox_Execute.mTButton_Permission.set_active( false );
		mHBox_Execute.mTButton_Permission.set_sensitive( false );
		mSection_Permission.hide();
	}
	else {
		mHBox_Execute.mTButton_Permission.set_sensitive( true );
	}

	return;
}

/*
 * pdfchain.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_CONFIG__
#define __PDFCHAIN_CONFIG__

#include "config.h"

#include <iostream>
//#include <limits>
//#include <memory>
//#include <exception>

#include <gtkmm.h>
//#include <giomm.h>
//#include <glibmm.h>
#include <glibmm/i18n.h>



/*** Precompiler definitions **************************************************/

//#define PDFCHAIN_TEST	// if defined shows test messages (e.g. destructor messages)
//#define PDFCHAIN_TEMP	// if defined shows temp widgets (e.g. hidden TreeViewColumns for Tooltips)



/*** Prototypes ***************************************************************/

namespace Pdfchain {

	std::string extract_filename_from_path( const std::string& );
	guint       count_page_numbers( const std::string& );
	
	guint       count_digits_of_guint_oct( guint );
	guint       count_digits_of_guint_dec( guint );
	guint       count_digits_of_guint_hex( guint );
	
	std::string convert_guint_to_string( guint );
	std::string escape_string( const std::string& );
	std::string quote_path( const std::string& );
	int         execute_command( const std::string& );
}



/*** Global objects ***********************************************************/

namespace Pdfchain {
//	extern Gtk::Window*    pWindow;
	extern Gtk::Statusbar* pSBar;
}



/*** Enumerators **************************************************************/

namespace Pdfchain {
	
	namespace Id {

		enum FileType {
			FILE_TYPE_ANY,
			FILE_TYPE_PDF,
			FILE_TYPE_FDF,
			FILE_TYPE_DUMP,
			FILE_TYPE_DIR
		};

		enum EvenOdd {
			PAGES_ALL,
			PAGES_EVEN,
			PAGES_ODD
		};

		enum Rotation {
			ROTATION_N,
			ROTATION_E,
			ROTATION_S,
			ROTATION_W
		};

		enum OutputId {
			ID_NEW,
			ID_FIRST,
			ID_FINAL,
			ID_KEEP
		};

		enum CountingMethode {
			COUNT_OCT,
			COUNT_DEC,
			COUNT_HEX
		};
	}
}



/*** Constant values (extern) *************************************************/

namespace Pdfchain {

	// GUI values
	extern const guint BORDER;
	extern const guint SPACING;
	extern const guint PADDING;

	extern const std::string APPICON_PATH_A;
	extern const std::string APPICON_PATH_B;

	// About
	namespace About {
		extern const Glib::ustring PROGRAM_NAME;
		extern const Glib::ustring PROGRAM_VERSION;
		extern const Glib::ustring PROJECT_URL;
	}

	// Window
	namespace Window {
		extern const Glib::ustring TITLE;
		extern const int           WIDTH;
		extern const int           HEIGHT;
	}

	// Special characters
	namespace Char {
		extern const char EOL;
		extern const char ESC;
	}

	// Special characters as strings
	namespace String {
		extern const Glib::ustring QUOTE_BEGIN;
		extern const Glib::ustring QUOTE_END;
		extern const Glib::ustring COPYRIGHT;
	}
	
	// Colors
	namespace Color {
		extern const Gdk::RGBA RGBA_VALID;
		extern const Gdk::RGBA RGBA_WARNING;
		extern const Gdk::RGBA RGBA_INVALID;
	}

	// Count PDF page numbers
	namespace Count {
		extern const guint BUFFER_SIZE;

		extern const Glib::ustring TYPE_PAGE_A;
		extern const Glib::ustring TYPE_PAGE_B;
		extern const Glib::ustring TYPE_PAGE_C;
		extern const Glib::ustring TYPE_PAGE_D;
	}

	namespace Path {
		//extern const std::string DEFAULT_FOLDER_DOCUMENTS;	// OLD
		extern const char        ESCAPE_CHARS[];

	}

	// Sections
	namespace Cat {
		extern const Glib::ustring ALLOWED_SIGNS;
		extern const Glib::ustring ALLOWED_DIGITS;
		
//		extern const guchar HANDLE_DEFAULT;	//TODO: OLD - delete handles
//		extern const guchar HANDLE_FIRST;
//		extern const guchar HANDLE_FINAL;
	}

	namespace Burst {
		extern const double ADJUST_UPPER_DIGITS;
	}

	namespace Attach {
		extern const double ADJUST_UPPER_PAGE_NUMBERS;
	}

	// File Extensions
	namespace Extension {
		extern const std::string EXT_PDF;
		extern const std::string EXT_FDF;
		extern const std::string EXT_TXT;
		extern const std::string EXT_DUMP;
	}
	
	// PDFTK commands
	namespace Cmd {

		// General
		extern const std::string PDFTK;
		extern const std::string CAT;
		extern const std::string SHUFFLE;
		extern const std::string OUTPUT;
		extern const std::string PASSWORD_INPUT;

		// Cat and Shuffle
		extern const std::string ID_KEEP;
		extern const std::string ID_NEW;
		extern const std::string ID_FIRST;
		extern const std::string ID_FINAL;

		extern const std::string ROTATION_N;
		extern const std::string ROTATION_E;
		extern const std::string ROTATION_S;
		extern const std::string ROTATION_W;

		extern const std::string PAGES_ALL;
		extern const std::string PAGES_EVEN;
		extern const std::string PAGES_ODD;

		// Burst
		extern const std::string BURST;
		extern const std::string TEMPLATE;
		extern const std::string EXTENSION;
		extern const std::string COUNT_OCT;
		extern const std::string COUNT_DEC;
		extern const std::string COUNT_HEX;

		// Background / Stamp
		extern const std::string BACKGROUND;
		extern const std::string BACKGROUND_MULTI;
		extern const std::string STAMP;
		extern const std::string STAMP_MULTI;

		// Attachment
		extern const std::string ATTACH_FILES;
		extern const std::string TO_PAGE;

		// Features
		extern const std::string ALLOW;
		extern const std::string FEATURES_ALL;
//		extern const std::string FEATURES_NONE;

		extern const std::string PRINTING;
		extern const std::string DEGRADED_PRINTING;
		extern const std::string MODIFY_CONTENTS;
		extern const std::string ASSEMBLY;
		extern const std::string COPY_CONTENTS;
		extern const std::string SCREEN_READERS;
		extern const std::string MODIFY_ANNOTATIONS;
		extern const std::string FILL_IN;

		extern const std::string PASSWORD_OWNER;
		extern const std::string PASSWORD_USER;

//		extern const std::string ENCRYPT_NONE;
		extern const std::string ENCRYPT_40BIT;
		extern const std::string ENCRYPT_128BIT;

		// Tools
		extern const std::string UNPACK_FILES;
		extern const std::string COMPRESS;
		extern const std::string UNCOMPRESS;
		extern const std::string DUMP_DATA_ANNOTS;
		extern const std::string DUMP_DATA_FIELDS;
		extern const std::string DUMP_DATA_FIELDS_UTF8;
		extern const std::string DUMP_DATA;
		extern const std::string DUMP_DATA_UTF8;
		extern const std::string UPDATE_INFO;
		extern const std::string UPDATE_INFO_UTF8;
		extern const std::string GENERATE_FDF;
		extern const std::string FILL_FORM;
		extern const std::string FLATTEN;
		extern const std::string NEED_APPEARANCES;
		extern const std::string DROP_XFA;
	}
}


#endif

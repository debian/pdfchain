/*
 * window_main_burst.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_BURST_H__
#define __PDFCHAIN_WINDOW_MAIN_BURST_H__

#include "pdfchain.h"
#include "store.h"
#include "dialog_filechooser.h"


/*** Combobox : Counting Base **********************************************/

class
cCBox_CountingBase : public Gtk::ComboBox
{
	friend class cSection_Burst;
	
	public:

		// Constructor
		         cCBox_CountingBase();
		virtual ~cCBox_CountingBase();
		
	protected:

		// Anchors
		Glib::RefPtr<cLStore_CountingBase> rLStore;
		Gtk::CellRendererText mCRText;
		cTMCRecord_Selection* pTMCRecord;		
};



/*** Section Burst ************************************************************/

class
cSection_Burst : public Gtk::VBox
{
	public:

		// Constructor
		         cSection_Burst( Gtk::Window& );
		virtual ~cSection_Burst();
		
		// Methodes
		void clear();
		std::string createCommand();
		
	private:
	
		// Methodes
		void init();
	
		// Variables
		const Glib::ustring
			sPrefix ,
			sSuffix;

		guint
			vPageNumbers ,
			vDigits_Oct ,
			vDigits_Dec ,
			vDigits_Hex;

		Glib::RefPtr<Gtk::SizeGroup>
			rSGroup_LabelPattern;

		// Widgets
		Gtk::HBox
			mHBox_SourceFile ,
			mHBox_Prefix ,
			mHBox_Digits;

		Gtk::VBox
			mVBox_Pattern;

		Gtk::Table
			mTable_Extended;

		Gtk::Frame
			mFrame_Pattern;

		Gtk::Expander
			mExpander_Extended;

		Gtk::Label
			mLabel_Pattern ,
			mLabel_Extended ,
			mLabel_SourceFile ,
			mLabel_Prefix ,
			mLabel_Base ,
			mLabel_Digits ,
			mLabel_Suffix;

		Gtk::Entry
			mEntry_Prefix ,
		    mEntry_Suffix;

		Glib::RefPtr<Gtk::Adjustment>	// Declaration of Glib::RefPtr<Gtk::Adjustment> before Gtk::SpinButton!!!
			rAdjust_Digits;

		Gtk::SpinButton					// Declaration of Gtk::SpinButton behind Glib::RefPtr<Gtk::Adjustment>!!!
			mSButton_Digits;

		Gtk::RadioButton				// Declaration of Gtk::RadioButton before Gtk::RadioButtonGroup!!!
			mRButton_Auto ,
		    mRButton_Manual;

		Gtk::RadioButtonGroup			// Declaration of Gtk::RadioButtonGroup behind Gtk::RadioButton!!!
			mRBGroup_CounterDigits;

		Gtk::CheckButton
			mCButton_Extension;

		cCBox_CountingBase
			mCBox_CountingBase;

		cFCButton_Pdf
			mFCButton_SourceFile;

		cFCDialog_SelectFolder
			mFCDialog_SelectFolder;

		// Signal Handler
		inline void onFCButton_SourceFile_file_set() {

			vPageNumbers = Pdfchain::count_page_numbers( mFCButton_SourceFile.get_filename() );

			vDigits_Oct = Pdfchain::count_digits_of_guint_oct( vPageNumbers );
			vDigits_Dec = Pdfchain::count_digits_of_guint_dec( vPageNumbers );
			vDigits_Hex = Pdfchain::count_digits_of_guint_hex( vPageNumbers );

			onCBox_CountingBase_changed();
			return;
		}
		
		inline void onEntryIcon_Prefix_pressed( 
			Gtk::EntryIconPosition icon_pos ,
			const GdkEventButton* event )
		{
			mEntry_Prefix.set_text( sPrefix );
			return;
		}
		
		inline void onCBox_CountingBase_changed() {

			Gtk::TreeModel::iterator
				iter = mCBox_CountingBase.get_active();

			cTMCRecord_Selection*
				pt_record = mCBox_CountingBase.rLStore->getTMCRecord();

			if ( mRButton_Auto.get_active() ) {
				switch ( (*iter)[pt_record->mTMColumn_ID] ) {

					case Pdfchain::Id::COUNT_OCT:
						rAdjust_Digits->set_value( static_cast<double>( vDigits_Oct ) );
						break;
				
					case Pdfchain::Id::COUNT_DEC:
						rAdjust_Digits->set_value( static_cast<double>( vDigits_Dec ) );
						break;
					
					case Pdfchain::Id::COUNT_HEX:
						rAdjust_Digits->set_value( static_cast<double>( vDigits_Hex ) );
						break;
				}
			}

			return;
		}

		inline void onRButton_Auto_toggled() {
			mSButton_Digits.set_sensitive( mRButton_Manual.get_active() );
			onCBox_CountingBase_changed();
			return;
		}
		
		inline void onEntryIcon_Suffix_pressed(
			Gtk::EntryIconPosition icon_pos ,
			const GdkEventButton* event )
		{
			mEntry_Suffix.set_text( sSuffix );
			return;
		}
};


#endif

/*
 * store_cat.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_STORE_CAT_H__
#define __PDFCHAIN_STORE_CAT_H__

#include "pdfchain.h"
#include "store.h"
#include "dialog_filechooser.h"
#include "dialog.h"


/*** Tree Model Column Record : Cat *******************************************/

class
cTMCRecord_Cat : public Gtk::TreeModel::ColumnRecord
{
	public:

		cTMCRecord_Cat() {
			add( mTMColumn_Id					);
			add( mTMColumn_Add					);
			add( mTMColumn_Pages				);
			add( mTMColumn_EvenOdd				);
			add( mTMColumn_Rotation				);
			add( mTMColumn_SourceFile			);
			add( mTMColumn_SourcePath			);
			add( mTMColumn_Tooltip				);
			add( mTMColumn_Password				);
			add( mTMColumn_PageNumbers			);
			add( mTMColumn_Cmd_EvenOdd			);
			add( mTMColumn_Cmd_Rotation			);
			add( mTMColumn_Color_Pages			);
			add( mTMColumn_Color_PageNumbers	);
		}

		virtual ~cTMCRecord_Cat() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cTMCRecord_Cat::~cTMCRecord_Cat()";	//TEST
#endif
		}

		Gtk::TreeModelColumn<guint>			mTMColumn_Id;
		Gtk::TreeModelColumn<gboolean>		mTMColumn_Add;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Pages;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_EvenOdd;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Rotation;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_SourceFile;
		Gtk::TreeModelColumn<std::string>	mTMColumn_SourcePath;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Tooltip;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Password;
		Gtk::TreeModelColumn<guint>			mTMColumn_PageNumbers;
		Gtk::TreeModelColumn<std::string>	mTMColumn_Cmd_EvenOdd;
		Gtk::TreeModelColumn<std::string>	mTMColumn_Cmd_Rotation;
		Gtk::TreeModelColumn<Gdk::RGBA>		mTMColumn_Color_Pages;
		Gtk::TreeModelColumn<Gdk::RGBA>		mTMColumn_Color_PageNumbers;
};



/*** List Store : Cat *********************************************************/

class
cLStore_Cat : public Gtk::ListStore
{
	public:
		         cLStore_Cat( Gtk::Window& );
		virtual ~cLStore_Cat();
		
		static Glib::RefPtr<cLStore_Cat>
			create( Gtk::Window& ref_window )
		{
			return Glib::RefPtr<cLStore_Cat>(
				new cLStore_Cat( ref_window ) );
		}
		
		cTMCRecord_Cat* getTMCRecord() {
			return pTMCRecord;
		}
		
		void clear();

		void addRows(           Glib::RefPtr<Gtk::TreeView::Selection> );
		void removeRows(        Glib::RefPtr<Gtk::TreeView::Selection> );
		void copyRows(          Glib::RefPtr<Gtk::TreeView::Selection> );
		void moveRows_Up(       Glib::RefPtr<Gtk::TreeView::Selection> );
		void moveRows_Down(     Glib::RefPtr<Gtk::TreeView::Selection> );
		void moveRows_ToTop(    Glib::RefPtr<Gtk::TreeView::Selection> );
		void moveRows_ToBottom( Glib::RefPtr<Gtk::TreeView::Selection> );

		void toggleAdd(      const Gtk::TreeModel::Path& );
		void editPages(      const Gtk::TreeModel::Path& , const Glib::ustring& );
		void changeEvenOdd(  const Gtk::TreeModel::Path& , const Glib::ustring& , const std::string& );
		void changeRotation( const Gtk::TreeModel::Path& , const Glib::ustring& , const std::string& );
		void editPassword(   const Gtk::TreeModel::Path& , const Glib::ustring& );

		std::string createCommand( bool );

		sigc::signal<void , guchar> signalFileNumber_changed() {
			return mSignal_FileNumber_changed;
		}

	protected:

		cTMCRecord_Cat*       pTMCRecord;

		cFCDialog_AddFile_Pdf mFCDialog;

		void initRow( const Gtk::TreeModel::iterator& , const std::string& );
		void copyRow( const Gtk::TreeModel::iterator& , const Gtk::TreeModel::iterator& );

		bool  validateTMColumn_Pages( const Gtk::TreeModel::iterator& );

		guint         getPageNumbers( const Glib::ustring& );
		Glib::ustring getPassword( const Glib::ustring& );

		void indicateFileIDs();
		void countFiles();

		std::string getHandle( guint );

		sigc::signal<void , guchar> mSignal_FileNumber_changed;
};


#endif

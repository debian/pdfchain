/*
 * dialog_filechooser.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dialog_filechooser.h"


/*** File Chooser Dialog ******************************************************/

/*** File Chooser Dialog - Add File *******************************************/

// Constructor
cFCDialog_AddFile::cFCDialog_AddFile(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	Gtk::FileChooserDialog( ref_window , str_title , Gtk::FILE_CHOOSER_ACTION_OPEN )
{
	set_select_multiple( true );
	
	add_button( Gtk::Stock::CANCEL , Gtk::RESPONSE_CANCEL );
	add_button( Gtk::Stock::ADD    , Gtk::RESPONSE_OK );
	
	init();
}


// Destructor
cFCDialog_AddFile::~cFCDialog_AddFile()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_AddFile::~cFCDialog_AddFile()";	//TEST
#endif
}


// Method (public) : clear
void
cFCDialog_AddFile::clear()
{
	init();
	return;
}


// Method (public, overwrite) : run
int
cFCDialog_AddFile::run()
{
	set_current_folder( get_current_folder() );
	return Gtk::FileChooserDialog::run();
}


// Method (protected) : init
void
cFCDialog_AddFile::init()
{
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );	
	return;
}



/*** File Chooser Dialog - Add File - Any *************************************/

// Constructor
cFCDialog_AddFile_Any::cFCDialog_AddFile_Any(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	cFCDialog_AddFile( ref_window , str_title )
{
	add_filter( Glib::RefPtr<cFFilter_Any>( new cFFilter_Any() ) );
	add_filter( Glib::RefPtr<cFFilter_Pdf>( new cFFilter_Pdf() ) );
}


// Destructor
cFCDialog_AddFile_Any::~cFCDialog_AddFile_Any()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_AddFile_Any::~cFCDialog_AddFile_Any()";	//TEST
#endif
}



/*** File Chooser Dialog - Add File - Pdf *************************************/

// Constructor
cFCDialog_AddFile_Pdf::cFCDialog_AddFile_Pdf(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	cFCDialog_AddFile( ref_window , str_title )
{
	add_filter( Glib::RefPtr<cFFilter_Pdf>( new cFFilter_Pdf() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>( new cFFilter_Any() ) );
}


// Destructor
cFCDialog_AddFile_Pdf::~cFCDialog_AddFile_Pdf()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_AddFile_Pdf::~cFCDialog_AddFile_Pdf()";	//TEST
#endif
}



/*** File Chooser Dialog - Save As ********************************************/

// Constructor
cFCDialog_SaveAs::cFCDialog_SaveAs(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title ,
	const Glib::ustring& str_extension )
:
	Gtk::FileChooserDialog( ref_window , str_title , Gtk::FILE_CHOOSER_ACTION_SAVE ),
	sExtension( str_extension ),
	mCButton_AddExtension( _("Add _extension") , true ),	// label , mnemonic
	mMDialog_OverwriteConfirm( ref_window , _("Overwrite file?") , false ,
		Gtk::MESSAGE_QUESTION , Gtk::BUTTONS_YES_NO , false )
{
	set_select_multiple( false );
	
	add_button( Gtk::Stock::CANCEL , Gtk::RESPONSE_CANCEL );
	add_button( Gtk::Stock::SAVE   , Gtk::RESPONSE_OK     );

	set_extra_widget( mCButton_AddExtension );
	
	init();
}


// Destructor
cFCDialog_SaveAs::~cFCDialog_SaveAs()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_SaveAs::~cFCDialog_SaveAs()";	//TEST
#endif
}


// Method (public) : clear
void
cFCDialog_SaveAs::clear()
{
	init();
	return;
}


// Method (public, overwrite) : run
int
cFCDialog_SaveAs::run()
{
	set_current_folder( get_current_folder() );
	return Gtk::FileChooserDialog::run();
}


// Method (public) : get filename
std::string
cFCDialog_SaveAs::get_filename()
{
	std::string str_filename = Gtk::FileChooser::get_filename();
	
	if ( true == mCButton_AddExtension.get_active() )
	{
		addExtension( str_filename );
	}

	if ( Gio::File::create_for_path( str_filename )->query_exists() )
	{
		mMDialog_OverwriteConfirm.set_secondary_text(
			Glib::filename_to_utf8( str_filename ) );

		switch ( mMDialog_OverwriteConfirm.run() )
		{
			case Gtk::RESPONSE_NO:
				str_filename = "";
				break;
		}
		
		mMDialog_OverwriteConfirm.hide();
	}

	return str_filename;
}


// Method (protected) : init
void
cFCDialog_SaveAs::init()
{
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );
	mCButton_AddExtension.set_active( true );
	return;
}


// Method (protected) : add extension
void
cFCDialog_SaveAs::addExtension( std::string& str_filename )
{
	std::string::size_type size_extension = sExtension.length();
	std::string::size_type size_filename  = str_filename.length();

	if ( str_filename.compare( size_filename - size_extension , size_filename , sExtension ) )
		str_filename.append( sExtension );
	
	return;
}



/*** File Chooser Dialog - Save As - PDF **************************************/

// Constructor
cFCDialog_SaveAs_Pdf::cFCDialog_SaveAs_Pdf(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	cFCDialog_SaveAs( ref_window , str_title , Pdfchain::Extension::EXT_PDF )
{
	add_filter( Glib::RefPtr<cFFilter_Pdf>( new cFFilter_Pdf() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>( new cFFilter_Any() ) );
	
	init();
}


// Destructor
cFCDialog_SaveAs_Pdf::~cFCDialog_SaveAs_Pdf()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_SaveAs_Pdf::~cFCDialog_SaveAs_Pdf()";	//TEST
#endif
}



/*** File Chooser Dialog - Save As - FDF **************************************/

// Constructor
cFCDialog_SaveAs_Fdf::cFCDialog_SaveAs_Fdf(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	cFCDialog_SaveAs( ref_window , str_title , Pdfchain::Extension::EXT_FDF )
{
	add_filter( Glib::RefPtr<cFFilter_Fdf>(  new cFFilter_Fdf()  ) );
	add_filter( Glib::RefPtr<cFFilter_Text>( new cFFilter_Text() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>(  new cFFilter_Any()  ) );
}


// Destructor
cFCDialog_SaveAs_Fdf::~cFCDialog_SaveAs_Fdf()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_SaveAs_Fdf::~cFCDialog_SaveAs_Fdf()";	//TEST
#endif
}



/*** File Chooser Dialog - Save As - Dump *************************************/

// Constructor
cFCDialog_SaveAs_Dump::cFCDialog_SaveAs_Dump(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	cFCDialog_SaveAs( ref_window , str_title , Pdfchain::Extension::EXT_DUMP )
{
	add_filter( Glib::RefPtr<cFFilter_Dump>( new cFFilter_Dump() ) );
	add_filter( Glib::RefPtr<cFFilter_Text>( new cFFilter_Text() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>(  new cFFilter_Any()  ) );
}


// Destructor
cFCDialog_SaveAs_Dump::~cFCDialog_SaveAs_Dump()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_SaveAs_Dump::~cFCDialog_SaveAs_Dump()";	//TEST
#endif
}



/*** File Chooser Dialog - Select - Folder ************************************/

// Constructor
cFCDialog_SelectFolder::cFCDialog_SelectFolder(
	Gtk::Window& ref_window ,
	const Glib::ustring& str_title )
:
	Gtk::FileChooserDialog( ref_window , str_title , Gtk::FILE_CHOOSER_ACTION_SELECT_FOLDER )
{
	add_button( Gtk::Stock::CANCEL    , Gtk::RESPONSE_CANCEL );
//	add_button( Gtk::Stock::DIRECTORY , Gtk::RESPONSE_OK );	// Gtk::Stock::DIRECTORY doesn't work right
	add_button( Gtk::Stock::OPEN      , Gtk::RESPONSE_OK );

	add_filter( Glib::RefPtr<cFFilter_Any>( new cFFilter_Any() ) );
	add_filter( Glib::RefPtr<cFFilter_Pdf>( new cFFilter_Pdf() ) );
	
	set_select_multiple( false );
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );
}


// Destructor
cFCDialog_SelectFolder::~cFCDialog_SelectFolder()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCDialog_SelectFolder::~cFCDialog_SelectFolder()";	//TEST
#endif
}

// Method (public) : clear
void
cFCDialog_SelectFolder::clear()
{
	init();
	return;
}


// Method (public, overwrite) : run
int
cFCDialog_SelectFolder::run()
{
	set_current_folder( get_current_folder() );
	return Gtk::FileChooserDialog::run();
}


// Method (protected) : init
void
cFCDialog_SelectFolder::init()
{
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );	
	return;
}



/*** File Chooser Button ******************************************************/

/*** File Chooser Button - PDF ************************************************/

// Constructor
cFCButton_Pdf::cFCButton_Pdf( Gtk::Window& ref_window , const Glib::ustring& str_title )
:
	Gtk::FileChooserButton( str_title , Gtk::FILE_CHOOSER_ACTION_OPEN )
{
//	set_transient_for( ref_window );

	add_filter( Glib::RefPtr<cFFilter_Pdf>( new cFFilter_Pdf() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>( new cFFilter_Any() ) );
	
	clear();
}


// Destructor
cFCButton_Pdf::~cFCButton_Pdf()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCButton_Pdf::~cFCButton_Pdf()";	//TEST
#endif
}


// Method (public) : clear
void
cFCButton_Pdf::clear()
{
	unselect_all();
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );	
	return;
}



/*** File Chooser Button - FDF ************************************************/

// Constructor
cFCButton_Fdf::cFCButton_Fdf( Gtk::Window& ref_window , const Glib::ustring& str_title )
:
	Gtk::FileChooserButton( str_title , Gtk::FILE_CHOOSER_ACTION_OPEN )
{
	add_filter( Glib::RefPtr<cFFilter_Fdf>(  new cFFilter_Fdf()  ) );
	add_filter( Glib::RefPtr<cFFilter_Text>( new cFFilter_Text() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>(  new cFFilter_Any()  ) );
	
	clear();
}


// Destructor
cFCButton_Fdf::~cFCButton_Fdf()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCButton_Fdf::~cFCButton_Fdf()";	//TEST
#endif
}


// Method (public) : clear
void
cFCButton_Fdf::clear()
{
	unselect_all();
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );	
	return;
}



/*** File Chooser Button - Dump ***********************************************/

// Constructor
cFCButton_Dump::cFCButton_Dump( Gtk::Window& ref_window , const Glib::ustring& str_title )
:
	Gtk::FileChooserButton( str_title , Gtk::FILE_CHOOSER_ACTION_OPEN )
{
	add_filter( Glib::RefPtr<cFFilter_Dump>( new cFFilter_Dump() ) );
	add_filter( Glib::RefPtr<cFFilter_Text>( new cFFilter_Text() ) );
	add_filter( Glib::RefPtr<cFFilter_Any>(  new cFFilter_Any()  ) );
	
	clear();
}


// Destructor
cFCButton_Dump::~cFCButton_Dump()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cFCButton_Dump::~cFCButton_Dump()";	//TEST
#endif
}


// Method (public) : clear
void
cFCButton_Dump::clear()
{
	unselect_all();
	set_current_folder( Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS ) );	
	return;
}


/*
 * constant.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_CONSTANT__
#define __PDFCHAIN_CONSTANT__

#include <iostream>
//#include <limits>
#include <gtkmm.h>

#include "config.h"


/*** Precompiler definitions **************************************************/

#define PDFCHAIN "PDF Chain"

// config.h
#ifndef VERSION
#define VERSION 0.3.x
#endif

#ifndef PACKAGE_URL
#define PACKAGE_URL "http://pdfchain.sourceforge.net"
#endif




/*** Constant values **********************************************************/

namespace Pdfchain {

	// GUI values
	const guint BORDER  = 10;
	const guint SPACING = 10;
	const guint PADDING = 5;
/*
	const guint BORDER  = 12;
	const guint SPACING = 12;
	const guint PADDING = 5;
*/
	const std::string APPICON_PATH_A = "/usr/share/pixmaps/pdfchain.png";
	const std::string APPICON_PATH_B = "/usr/local/share/pixmaps/pdfchain.png";

	// About
	namespace About {
		const Glib::ustring PROGRAM_NAME    = PDFCHAIN;
		const Glib::ustring PROGRAM_VERSION = VERSION;
		const Glib::ustring PROJECT_URL     = PACKAGE_URL;
	}

	// Window
	namespace Window {
		const Glib::ustring TITLE  = PDFCHAIN;
		const int           WIDTH  = 650;
		const int           HEIGHT = 530;
	}

	// Special characters
	namespace Char {
		const char EOL = '\0';
		const char ESC = '\\';
	}
	
	// Special characters as strings
	namespace String {
		const Glib::ustring QUOTE_BEGIN = "\xC2\xBB";	// '>>'
		const Glib::ustring QUOTE_END   = "\xC2\xAB";	// '<<'
		const Glib::ustring COPYRIGHT   = "\xC2\xA9";	// (c)
	}

	// Colors
	namespace Color {
		const Gdk::RGBA RGBA_VALID   = Gdk::RGBA( NULL );		// default
		const Gdk::RGBA RGBA_WARNING = Gdk::RGBA( "#ff8000" );	// orange
		const Gdk::RGBA RGBA_INVALID = Gdk::RGBA( "#ff0000" );	// red
	}

	// Count PDF page numbers
	namespace Count {
		const guint BUFFER_SIZE = 1048576;	// 1 Mebibyte (MiB)

		const Glib::ustring TYPE_PAGE_A = "/Type/Page";
		const Glib::ustring TYPE_PAGE_B = "/Type /Page";
		const Glib::ustring TYPE_PAGE_C = "/Type/Pages";
		const Glib::ustring TYPE_PAGE_D = "/Type /Pages";
	}

	namespace Path {
		//const std::string DEFAULT_FOLDER_DOCUMENTS = Glib::get_user_special_dir( G_USER_DIRECTORY_DOCUMENTS );

		// Escape characters for the bash
		// '\\' backslash is the escape character - must be the first char at this array!
		// '\"' doublequote is the quote character for paths
		// '`'	backquote
		// '$'	dollar
		// '\0' NULL is the string terminator - must be the final char at this array!
		const char ESCAPE_CHARS[5] = { '\\' , '\"' , '`', '$', '\0' };
	}

	// Sections
	namespace Cat {
		const Glib::ustring ALLOWED_SIGNS  = "0123456789 -";
		const Glib::ustring ALLOWED_DIGITS = "0123456789";
		
//		const guchar HANDLE_DEFAULT = '0';	//TODO: OLD - delete handles
//		const guchar HANDLE_FIRST   = 'A';
//		const guchar HANDLE_FINAL   = 'Z';
	}

	namespace Burst {
		//const double ADJUST_UPPER_DIGITS = std::numeric_limits<double>::max();
		const double ADJUST_UPPER_DIGITS = 99.0;
	}

	namespace Attach {
		//const double ADJUST_UPPER_PAGE_NUMBERS = std::numeric_limits<double>::max();
		const double ADJUST_UPPER_PAGE_NUMBERS = 999999.0;
	}

	// File Extensions
	namespace Extension {
		const std::string EXT_PDF  = ".pdf";
		const std::string EXT_FDF  = ".fdf";
		const std::string EXT_TXT  = ".txt";
		const std::string EXT_DUMP = ".dump";
	}

	// PDFTK commands
	namespace Cmd {

		// General
		const std::string PDFTK          = "pdftk";
		const std::string CAT            = "cat";
		const std::string SHUFFLE        = "shuffle";
		const std::string OUTPUT         = "output";
		const std::string PASSWORD_INPUT = "input_pw";

		// Cat and Shuffle
		const std::string ID_KEEP  = "";						// keep ID
		const std::string ID_NEW   = "";						// new ID
		const std::string ID_FIRST = "keep_first_id";			// ID of first PDF
		const std::string ID_FINAL = "keep_final_id";			// ID of final PDF

		const std::string ROTATION_N = "north";					//   0°
		const std::string ROTATION_E = "east";					//  90°
		const std::string ROTATION_S = "south";					// 180°
		const std::string ROTATION_W = "west";					// 270°
/*		
		const std::string ROTATION_N = "N";						//   0°	// TODO: OLD - delete rotation
		const std::string ROTATION_E = "E";						//  90°
		const std::string ROTATION_S = "S";						// 180°
		const std::string ROTATION_W = "W";						// 270°
*/
		const std::string PAGES_ALL  = "";						// 1,2,3,4,5,6
		const std::string PAGES_EVEN = "even";					// 2,4,6
		const std::string PAGES_ODD  = "odd";					// 1,3,5

		// Burst
		const std::string BURST     = "burst";
		const std::string TEMPLATE  = "%0";						// Complete suffix example: "%04d.pdf"
		const std::string EXTENSION = ".pdf";
		const std::string COUNT_OCT = "o";
		const std::string COUNT_DEC = "d";
		const std::string COUNT_HEX = "x";

		// Background / Stamp
		const std::string BACKGROUND       = "background";
		const std::string BACKGROUND_MULTI = "multibackground";
		const std::string STAMP            = "stamp";
		const std::string STAMP_MULTI      = "multistamp";

		// Attachment
		const std::string ATTACH_FILES = "attach_files";
		const std::string TO_PAGE      = "to_page";

		// Features
		const std::string ALLOW         = "allow";
		const std::string FEATURES_ALL  = "AllFeatures";

		const std::string PRINTING           = "Printing";
		const std::string DEGRADED_PRINTING  = "DegradedPrinting";
		const std::string MODIFY_CONTENTS    = "ModifyContents";
		const std::string ASSEMBLY           = "Assembly";
		const std::string COPY_CONTENTS      = "CopyContents";
		const std::string SCREEN_READERS     = "ScreenReaders";
		const std::string MODIFY_ANNOTATIONS = "ModifyAnnotations";
		const std::string FILL_IN            = "FillIn";

		const std::string PASSWORD_OWNER = "owner_pw";
		const std::string PASSWORD_USER  = "user_pw";

		//const std::string ENCRYPT_NONE   = "";
		const std::string ENCRYPT_40BIT  = "encrypt_40bit";
		const std::string ENCRYPT_128BIT = "encrypt_128bit";

		// Tools
		const std::string UNPACK_FILES          = "unpack_files";
		const std::string COMPRESS              = "compress";
		const std::string UNCOMPRESS            = "uncompress";
		const std::string DUMP_DATA_ANNOTS      = "dump_data_annots";
		const std::string DUMP_DATA_FIELDS      = "dump_data_fields";
		const std::string DUMP_DATA_FIELDS_UTF8 = "dump_data_fields_utf8";
		const std::string DUMP_DATA             = "dump_data";
		const std::string DUMP_DATA_UTF8        = "dump_data_utf8";
		const std::string UPDATE_INFO           = "update_info";
		const std::string UPDATE_INFO_UTF8      = "update_info_utf8";
		const std::string GENERATE_FDF          = "generate_fdf";
		const std::string FILL_FORM             = "fill_form";
		const std::string FLATTEN               = "flatten";
		const std::string NEED_APPEARANCES      = "need_appearances";
		const std::string DROP_XFA              = "drop_xfa";		
	}
}


#endif

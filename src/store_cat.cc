/*
 * store_cat.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "store_cat.h"


/*** List Store : Cat *********************************************************/

// Constructor
cLStore_Cat::cLStore_Cat(
	Gtk::Window& ref_window )
:
	mFCDialog( ref_window , _("Concatenate - Add PDF files") ),
	pTMCRecord( NULL )
{
	pTMCRecord = new cTMCRecord_Cat;
	set_column_types( *pTMCRecord );
}


// Destructor
cLStore_Cat::~cLStore_Cat()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cLStore_Cat::~cLStore_Cat()";	//TEST
#endif

	if( pTMCRecord != NULL ) {
		delete pTMCRecord;
		pTMCRecord = NULL;
	}
}


// Method (public) : clear
void
cLStore_Cat::clear()
{
	Gtk::ListStore::clear();
	mFCDialog.clear();
	countFiles();
	return;
}


// Method (public) : add new rows (files)
void
cLStore_Cat::addRows(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	switch ( mFCDialog.run() ) {
		case Gtk::RESPONSE_OK: {
		
			mFCDialog.hide();

			std::string str_sourcepath;
			
			std::vector<std::string>
				vect_sourcepaths = mFCDialog.get_filenames();
				
			std::vector<std::string>::iterator
				iter_sourcepaths = vect_sourcepaths.begin();
			
			Gtk::TreeModel::iterator
				iter_selections,
				iter_new;
				
			// Get begin of TreeView selection and get TreeModel iterator of selection
			if ( 0 != ref_tree_selections->count_selected_rows() ) {

				std::vector<Gtk::TreeModel::Path>
					list_selections = ref_tree_selections->get_selected_rows();
					
				Gtk::TreeModel::Path
					path_selections = *( list_selections.begin() );

				iter_selections = get_iter( path_selections );
			}

			//For every file to add from filechooser do ...
			while ( vect_sourcepaths.end() != iter_sourcepaths ) {
				str_sourcepath = *iter_sourcepaths++;

				// ... insert a row after last selected file at store ...
				if ( iter_selections ) {
					iter_new = insert_after( iter_selections );
					iter_selections = iter_new;
				}
				// ... or at the end of the store
				else {
					iter_new = append();
				}

				initRow( iter_new , str_sourcepath );
			}
			break;
		}
		default:
			mFCDialog.hide();
			break;
	}

	countFiles();
	return;
}


// Method (public) : remove selected rows
void
cLStore_Cat::removeRows(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();
		
	Gtk::TreeModel::iterator
		iter_next;

	if ( list_selections.size() ) {

		// Define row to select as soon as selected rows are removed
		iter_next = get_iter( list_selections.back() );
		if ( ++iter_next == children().end() ) {
			if ( children().begin() != get_iter( list_selections.front() ) ) {
				iter_next = get_iter( list_selections.front() );
				iter_next--;
			}
		}

		// Remove selected rows
		while ( list_selections.size() ) {
			erase( get_iter( list_selections.back() ) );
			list_selections.erase( list_selections.end() );
		}

		if ( iter_next ) ref_tree_selections->select( iter_next );
	}

	countFiles();
	return;
}


// Method (public) : copy selected Rows
void
cLStore_Cat::copyRows(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {
		Gtk::TreeModel::iterator iter_pos = get_iter( list_selections.back() );
		Gtk::TreeModel::iterator iter_org;
		Gtk::TreeModel::iterator iter_cpy;

		while ( list_selections.size() ) {
			iter_org = get_iter( list_selections.back() );
			iter_cpy = insert_after( iter_pos );

			copyRow( iter_cpy , iter_org );
			list_selections.erase( list_selections.end() );
		}
	}
	
	return;
}


// Method (public) : move selected rows up
void
cLStore_Cat::moveRows_Up(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {
	
		Gtk::TreeModel::iterator
			iter_selections ,
			iter_upper;

		// Move every row only if first selected row is not the first row at the ListStore
		if ( children().begin() !=  get_iter( list_selections.front() ) ) {

			while ( list_selections.size() ) {
				iter_upper = iter_selections = get_iter( list_selections.front() );
				
				iter_swap( iter_selections , --iter_upper );
				list_selections.erase( list_selections.begin() );
			}
		}
	}

	return;
}


// Method (public) : move selected rows down
void
cLStore_Cat::moveRows_Down(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {
	
		Gtk::TreeModel::iterator
			iter_selections ,
			iter_lower;

		// Move every row only if last selected row is not the last row at the ListStore
		if ( --children().end() != get_iter( list_selections.back() ) ) {
			
			while ( list_selections.size() ) {
				iter_lower = iter_selections = get_iter( list_selections.back() );

				iter_swap( iter_selections , ++iter_lower );
				list_selections.erase( list_selections.end() );
			}
		}
	}
	
	return;
}


// Method (public) : move selected rows up to top
void
cLStore_Cat::moveRows_ToTop(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {

		// Move every row till first selected row is the first row at the ListStore
		while ( children().begin() !=  get_iter( list_selections.front() ) ) {
			moveRows_Up( ref_tree_selections );
			list_selections = ref_tree_selections->get_selected_rows();
		}
	}

	return;
}


// Method (public) : move selected rows down to bottom
void
cLStore_Cat::moveRows_ToBottom(
	Glib::RefPtr<Gtk::TreeView::Selection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {

		// Move every row till first selected row is the first row at the ListStore
		while ( --children().end() !=  get_iter( list_selections.back() ) ) {
			moveRows_Down( ref_tree_selections );
			list_selections = ref_tree_selections->get_selected_rows();
		}
	}

	return;
}


// Method (protected) : indicate File ID
// Idicates every document at an active row at the store with an unique file id
void
cLStore_Cat::indicateFileIDs()
{
	guint
		file_id = 0;

	std::string
		str_path_a,
		str_path_b;

	Gtk::TreeModel::iterator
		iter_a,
		iter_b;

	// Init IDs with default value
	for ( iter_a = children().begin() ; children().end() != iter_a ; ++iter_a )
		(*iter_a)[pTMCRecord->mTMColumn_Id] = G_MAXUINT;

	// Set new ID ...
	file_id = 0;
	for ( iter_a = children().begin() ; children().end() != iter_a ; ++iter_a ) {

		// ... only if CheckBox is enabled ...
		if ( TRUE == (*iter_a)[pTMCRecord->mTMColumn_Add] ) {

			// ... and a ID number has not yet been allocated (if file id has default value)
			if ( G_MAXUINT == (*iter_a)[pTMCRecord->mTMColumn_Id] ) {
				str_path_a = (*iter_a)[pTMCRecord->mTMColumn_SourcePath];

				(*iter_a)[pTMCRecord->mTMColumn_Id] = file_id;

				// Set this ID to other entries ...
				for ( iter_b = iter_a ; children().end() != iter_b ; ++iter_b ) {

					// ... only if CheckBox is enabled ...
					if ( TRUE == (*iter_b)[pTMCRecord->mTMColumn_Add] ) {
						str_path_b = (*iter_b)[pTMCRecord->mTMColumn_SourcePath];

						// ... and have the same path
						// std::string::compare() returns 0 on comparing strings
						if ( ! str_path_a.compare( str_path_b ) ) {
							(*iter_b)[pTMCRecord->mTMColumn_Id] = file_id;
						}
					}
				}
				file_id++;
			}
		}
	}

	return;
}


// Method (protected) : count files 
// Counts different files (not records) at list using the pathname.
// The method emits a signal which commits 0 for no, 1 for one and 2 for much files
void
cLStore_Cat::countFiles()
{
	guchar num = 0;

	std::string
		str_a,
		str_b;

	Gtk::TreeModel::iterator
		iter_a,
		iter_b;

	if ( ! children().begin() ) {
		// no file was found at list
		mSignal_FileNumber_changed.emit( 0 );
		return;
	}
	else {

		// search for a first, active file
		for ( iter_a = children().begin() ; children().end() != iter_a ; ++iter_a ) {
			if ( TRUE == (*iter_a)[pTMCRecord->mTMColumn_Add] ) {
				str_a = (*iter_a)[pTMCRecord->mTMColumn_SourcePath];

				// search for a second, active file
				for ( iter_b = iter_a ; children().end() != iter_b ; ++iter_b ) {
					if ( TRUE == (*iter_b)[pTMCRecord->mTMColumn_Add] ) {
						str_b = (*iter_b)[pTMCRecord->mTMColumn_SourcePath];

						// std::string::compare() returns 0 on comparing strings
						if ( str_a.compare( str_b ) ) {

							// a second, active file was found
							mSignal_FileNumber_changed.emit( 2 );	
							return;
						}
					}
				}
				// a second, active file was not found (but a first was)
				mSignal_FileNumber_changed.emit( 1 );								
				return;
			}
		}
		// a first, active file was not found
		mSignal_FileNumber_changed.emit( 0 );	
		return;
	}

	return;
}


// Method (protected) : validate pages
// Checks TMColumn_Pages for valid user inputs
bool
cLStore_Cat::validateTMColumn_Pages(
	const Gtk::TreeModel::iterator& iter )
{
	Glib::ustring
		str_pages = iter->get_value( pTMCRecord->mTMColumn_Pages ) ,
		str_sub;

	Glib::ustring::size_type
		pos_a = 0 ,
		pos_b = 0 ,
		pos   = 0;

	const guint
		page_numbers = iter->get_value( pTMCRecord->mTMColumn_PageNumbers );

	guint
		number = 0;

	// Extend string at begin and end with space ' '
	str_pages = " " + str_pages + " ";

	// Check for invalid sub string " -"
	if ( str_pages.find( " -" ) != Glib::ustring::npos ) {
		return false;
	}

	// Check for invalid sub string "- "
	if ( str_pages.find( "- " ) != Glib::ustring::npos ) {
		return false;
	}

	// Search for ' ' separator to ...
	for ( pos_a = 0 ;
		Glib::ustring::npos != ( pos_b = str_pages.find( ' ' , pos_a ) ) ;
		pos_a = pos_b + 1 )
	{
		// ... split into sub strings ...
		str_sub = str_pages.substr( pos_a , pos_b - pos_a );

		// ... and validate sub string if it is longer than zero
		if ( 0 != str_sub.length() ) {

			// Count '-' at sub string
			for ( number = 0 , pos = 0 ;
				Glib::ustring::npos != ( pos = str_sub.find( '-' , pos ) ) ;
				number++ , pos++ );

			// Max one '-' is allowed per section
			if ( 1 < number ) {
				return false;
			}
		}
	}

	// Check and erase sub string "end"
	while ( Glib::ustring::npos != ( pos = str_pages.find( "end" ) ) ) {

		// Check character bevore "end" (must be ' ' or '-')
		if ( 0 < pos ) {
			str_sub = str_pages.substr( pos-1 , 1 );

			if ( " " != str_sub && "-" != str_sub ) {
				return false;
			}
		}

		// Check chracter after "end" (must be ' ' or '-')
		if ( Glib::ustring::npos != pos+3 ) {
			str_sub = str_pages.substr( pos+3 , 1 );

			if ( " " != str_sub && "-" != str_sub ) {
				return false;
			}
		}

		// Erase sub string "end"
		str_pages.erase( pos , 3 );
	}

	// Check and erase sub string "r"
	while ( Glib::ustring::npos != ( pos = str_pages.find( "r" ) ) ) {

		// Check character bevore "r" (must be ' ' or '-')
		if ( 0 < pos ) {
			str_sub = str_pages.substr( pos-1 , 1 );

			if ( " " != str_sub && "-" != str_sub ) {
				return false;
			}
		}

		// Check chracter after "r" (must be a char between '0' and '9')
		if ( Glib::ustring::npos != pos+1 ) {
			str_sub = str_pages.substr( pos+1 , 1 );

			if ( "0" > str_sub || "9" < str_sub ) {
				return false;
			}
		}

		// Erase sub string "end"
		str_pages.erase( pos , 1 );
	}

	// Check for valid signs: "01234567890 -"
	for ( pos = 0 ; str_pages.length() > pos ; pos++ ){

		if ( ! ( Glib::ustring::npos != 
			Pdfchain::Cat::ALLOWED_SIGNS.find( str_pages.substr( pos , 1 ) ) ) )
		{
			return false;
		}
	}

	// Replace sub string "-" with " "
	while ( Glib::ustring::npos != ( pos = str_pages.find( '-' ) ) )
		str_pages.replace( pos , 1 , 1 , ' ' );

	// Cleanup with doubled " "
	while ( Glib::ustring::npos != ( pos = str_pages.find( "  " ) ) )
		str_pages.replace( pos , 2 , " " );

	// Check page numbers (begin at pos=1 , pos=0 is ' ')
	for ( pos = 1 , number = 0 ; str_pages.length() > pos ; pos++ ) {

		// If digit --> count
		if ( Glib::ustring::npos != 
			Pdfchain::Cat::ALLOWED_DIGITS.find( str_pages.substr( pos , 1 ) ) )
		{
			number *= 10;	// FIXME: variable could overflow
			number += ( str_pages.c_str() )[pos] - '0';
		}

		// If separator ' ' ---> check
		else {

			// Check for invalid page number '0'
			if( 0 == number ) {
				return false;
			}

			// Check for invalid big page number
			// (If counted page_number is 0, file is not countable, but accepted by user)
			if( ( 0 < page_numbers ) && ( number > page_numbers ) ) {
				return false;
			}

			number = 0;
		}
	}

	// Entry is valid
	return true;
}


/*
// Method (protected) : validate pages
// Checks TMColumn_Pages for valid user inputs
bool
cLStore_Cat::validateTMColumn_Pages(
	const Gtk::TreeModel::iterator& iter )
{
	Glib::ustring
		str_pages = iter->get_value( pTMCRecord->mTMColumn_Pages ) ,
		str_sub;

	Glib::ustring::size_type
		pos_a = 0 ,
		pos_b = 0 ,
		pos   = 0;

	const guint
		page_numbers = iter->get_value( pTMCRecord->mTMColumn_PageNumbers );

	guint
		number = 0;

	// Extend string at begin and end with space ' '
	str_pages = " " + str_pages + " ";

	// Check for invalid sub string " -"
	if ( str_pages.find( " -" ) != Glib::ustring::npos ) {
		return false;
	}

	// Check for invalid sub string "- "
	if ( str_pages.find( "- " ) != Glib::ustring::npos ) {
		return false;
	}

	// Search for ' ' separator to ...
	for ( pos_a = 0 ;
		Glib::ustring::npos != ( pos_b = str_pages.find( ' ' , pos_a ) ) ;
		pos_a = pos_b + 1 )
	{
		// ... split into sub strings ...
		str_sub = str_pages.substr( pos_a , pos_b - pos_a );

		// ... and validate sub string if it is longer than zero
		if ( 0 != str_sub.length() ) {

			// Count '-' at sub string
			for ( number = 0 , pos = 0 ;
				Glib::ustring::npos != ( pos = str_sub.find( '-' , pos ) ) ;
				number++ , pos++ );

			// Max one '-' is allowed per section
			if ( 1 < number ) {
				return false;
			}
		}
	}

	// Check and erase sub string "end"
	while ( Glib::ustring::npos != ( pos = str_pages.find( "end" ) ) ) {

		// Check character bevore "end" (must be ' ' or '-')
		if ( 0 < pos ) {
			str_sub = str_pages.substr( pos-1 , 1 );

			if ( " " != str_sub && "-" != str_sub ) {
				return false;
			}
		}

		// Check chracter after "end" (must be ' ' or '-')
		if ( Glib::ustring::npos != pos+3 ) {
			str_sub = str_pages.substr( pos+3 , 1 );

			if ( " " != str_sub && "-" != str_sub ) {
				return false;
			}
		}

		// Erase sub string "end"
		str_pages.erase( pos , 3 );
	}

	// Check for valid signs: "01234567890 -"
	for ( pos = 0 ; str_pages.length() > pos ; pos++ ){

		if ( ! ( Glib::ustring::npos != 
			Pdfchain::Cat::ALLOWED_SIGNS.find( str_pages.substr( pos , 1 ) ) ) )
		{
			return false;
		}
	}

	// Replace sub string "-" with " "
	while ( Glib::ustring::npos != ( pos = str_pages.find( '-' ) ) )
		str_pages.replace( pos , 1 , 1 , ' ' );

	// Cleanup with doubled " "
	while ( Glib::ustring::npos != ( pos = str_pages.find( "  " ) ) )
		str_pages.replace( pos , 2 , " " );

	// Check page numbers (begin at pos=1 , pos=0 is ' ')
	for ( pos = 1 , number = 0 ; str_pages.length() > pos ; pos++ ) {

		// If digit --> count
		if ( Glib::ustring::npos != 
			Pdfchain::Cat::ALLOWED_DIGITS.find( str_pages.substr( pos , 1 ) ) )
		{
			number *= 10;	// FIXME: variable could overflow
			number += ( str_pages.c_str() )[pos] - '0';
		}

		// If separator ' ' ---> check
		else {

			// Check for invalid page number '0'
			if( 0 == number ) {
				return false;
			}

			// Check for invalid big page number
			// (If counted page_number is 0, file is not countable, but accepted by user)
			if( ( 0 < page_numbers ) && ( number > page_numbers ) ) {
				return false;
			}

			number = 0;
		}
	}

	// Entry is valid
	return true;
}
*/

// Method (protected) : get the number of pages of a document
// If the document is already listed, return the value of PageNumbers from this row
// If not, count the number of pages and return this value
guint
cLStore_Cat::getPageNumbers(
	const Glib::ustring& ustr_sourcepath )
{
	Gtk::TreeModel::iterator
		iter_row;

	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row )
		if ( iter_row->get_value( pTMCRecord->mTMColumn_SourcePath ) == ustr_sourcepath )
			return iter_row->get_value( pTMCRecord->mTMColumn_PageNumbers );

	return Pdfchain::count_page_numbers( ustr_sourcepath );
}


// Method (protected) : get the password of a document
// If the document is already listed, return the value of Password from this row
// If not, return an empty string
Glib::ustring
cLStore_Cat::getPassword(
	const Glib::ustring& ustr_sourcepath )
{
	Gtk::TreeModel::iterator
		iter_row;

	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row )
		if ( iter_row->get_value( pTMCRecord->mTMColumn_SourcePath ) == ustr_sourcepath )
			return iter_row->get_value( pTMCRecord->mTMColumn_Password );

	return "";
}


// Method (protected) : get a pdftk-handle from file id
// Converts the file id (b=10) to an character based handle (b=26)
// A conforms to 0 ; Z conforms 25
// A leading A isn't visible (like a leading 0)
std::string
cLStore_Cat::getHandle(
	guint id )
{
	if ( G_MAXUINT == id )
		std::cerr << std::endl << "WARNING: \
			Method cLStore_Cat::getHandle(...) detected default file id G_MAXUINT";

	std::string
		str_handle;

	// Convert number base from 10 [0 to 9] to 26 [A to Z]
	while ( id/26 ) {
		str_handle.append( 1 , 'A' + id%26 );
		id /= 26;
	}
	str_handle.append( 1 , 'A' + id%26 );

	return str_handle;
}


// Method (protected) : init a row with default values
void
cLStore_Cat::initRow(
	const Gtk::TreeModel::iterator& iter_row ,
	const std::string&              str_sourcepath )
{
	Glib::ustring
		ustr_sourcefile = Glib::filename_to_utf8(
			Pdfchain::extract_filename_from_path( str_sourcepath ) ) ,

		ustr_tooltip = _("<b>Path: </b>") + Glib::Markup::escape_text(
			Glib::filename_to_utf8( str_sourcepath ) ) ,
	
		ustr_password = getPassword( str_sourcepath );

	guint
		page_num = getPageNumbers( str_sourcepath );

	(*iter_row)[pTMCRecord->mTMColumn_Id]          = G_MAXUINT;
	(*iter_row)[pTMCRecord->mTMColumn_Add]         = TRUE;
	(*iter_row)[pTMCRecord->mTMColumn_Pages]       = "1-end";
	(*iter_row)[pTMCRecord->mTMColumn_EvenOdd]     = _("all");	//FIXME: get value from cLStore_EvenOdd
	(*iter_row)[pTMCRecord->mTMColumn_Rotation]    = _("0°");	//FIXME: get value from cLStore_Rotation
	(*iter_row)[pTMCRecord->mTMColumn_SourceFile]  = ustr_sourcefile;
	(*iter_row)[pTMCRecord->mTMColumn_SourcePath]  = str_sourcepath;
	(*iter_row)[pTMCRecord->mTMColumn_Tooltip]     = ustr_tooltip;
	(*iter_row)[pTMCRecord->mTMColumn_Password]    = ustr_password;
	(*iter_row)[pTMCRecord->mTMColumn_PageNumbers] = page_num;
	(*iter_row)[pTMCRecord->mTMColumn_Cmd_EvenOdd]  = Pdfchain::Cmd::PAGES_ALL;
	(*iter_row)[pTMCRecord->mTMColumn_Cmd_Rotation] = Pdfchain::Cmd::ROTATION_N;
	(*iter_row)[pTMCRecord->mTMColumn_Color_Pages]       = Pdfchain::Color::RGBA_VALID;
	(*iter_row)[pTMCRecord->mTMColumn_Color_PageNumbers] = Pdfchain::Color::RGBA_VALID;

	if ( 0 == page_num )
		iter_row->set_value( pTMCRecord->mTMColumn_Color_PageNumbers , Pdfchain::Color::RGBA_WARNING );

	return;
}


// Method (protected) : copy a row
void
cLStore_Cat::copyRow(
	const Gtk::TreeModel::iterator& iter_copy ,
	const Gtk::TreeModel::iterator& iter_org )
{
	iter_copy->set_value( pTMCRecord->mTMColumn_Id ,
		iter_org->get_value( pTMCRecord->mTMColumn_Id ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Add , 
		iter_org->get_value( pTMCRecord->mTMColumn_Add ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Pages , 
		iter_org->get_value( pTMCRecord->mTMColumn_Pages ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_EvenOdd , 
		iter_org->get_value( pTMCRecord->mTMColumn_EvenOdd ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Rotation , 
		iter_org->get_value( pTMCRecord->mTMColumn_Rotation ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_SourceFile , 
		iter_org->get_value( pTMCRecord->mTMColumn_SourceFile ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_SourcePath , 
		iter_org->get_value( pTMCRecord->mTMColumn_SourcePath ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Tooltip , 
		iter_org->get_value( pTMCRecord->mTMColumn_Tooltip ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Password , 
		iter_org->get_value( pTMCRecord->mTMColumn_Password ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_PageNumbers , 
		iter_org->get_value( pTMCRecord->mTMColumn_PageNumbers ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Cmd_EvenOdd , 
		iter_org->get_value( pTMCRecord->mTMColumn_Cmd_EvenOdd ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Cmd_Rotation , 
		iter_org->get_value( pTMCRecord->mTMColumn_Cmd_Rotation ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Color_Pages , 
		iter_org->get_value( pTMCRecord->mTMColumn_Color_Pages ) );
		
	iter_copy->set_value( pTMCRecord->mTMColumn_Color_PageNumbers , 
		iter_org->get_value( pTMCRecord->mTMColumn_Color_PageNumbers ) );
	
	return;
}


// Method (public) : toggle Add
void
cLStore_Cat::toggleAdd(
	const Gtk::TreeModel::Path& treepath )
{
	Gtk::TreeModel::iterator
		iter = get_iter( treepath );
	
	(*iter)[pTMCRecord->mTMColumn_Add] = ! (*iter)[pTMCRecord->mTMColumn_Add];
	countFiles();

	return;
}


// Method (public) : edit Pages
void
cLStore_Cat::editPages(
	const Gtk::TreeModel::Path& treepath ,
	const Glib::ustring& str_pages )
{
	Gtk::TreeModel::iterator
		iter = get_iter( treepath );

	iter->set_value( pTMCRecord->mTMColumn_Pages , str_pages );
	
	if ( true == validateTMColumn_Pages( iter ) ) {
		iter->set_value(
			pTMCRecord->mTMColumn_Color_Pages ,
			Pdfchain::Color::RGBA_VALID );
	}
	else {
		iter->set_value(
			pTMCRecord->mTMColumn_Color_Pages ,
			Pdfchain::Color::RGBA_INVALID );
	}
			
	return;
}


// Method (public) : change EvenOdd
void
cLStore_Cat::changeEvenOdd(
	const Gtk::TreeModel::Path& treepath ,
	const Glib::ustring& str_label ,
	const std::string& str_command )
{
	Gtk::TreeModel::iterator iter = get_iter( treepath );
	iter->set_value( pTMCRecord->mTMColumn_EvenOdd , str_label );
	iter->set_value( pTMCRecord->mTMColumn_Cmd_EvenOdd , str_command );
	return;
}


// Method (public) : change Rotation
void
cLStore_Cat::changeRotation(
	const Gtk::TreeModel::Path& treepath ,
	const Glib::ustring& str_label ,
	const std::string& str_command )
{
	Gtk::TreeModel::iterator
		iter = get_iter( treepath );

	iter->set_value( pTMCRecord->mTMColumn_Rotation , str_label );
	iter->set_value( pTMCRecord->mTMColumn_Cmd_Rotation , str_command );

	return;
}


// Method (public) : edit Password
void
cLStore_Cat::editPassword(
	const Gtk::TreeModel::Path& treepath ,
	const Glib::ustring& str_password )
{
	Gtk::TreeModel::iterator
		iter = get_iter( treepath ) ,
		iter_test;

	std::string
		str_sourcepath = iter->get_value( pTMCRecord->mTMColumn_SourcePath );

	// Entries with same sourcepaths have the same password
	for ( iter_test = children().begin() ; children().end() != iter_test ; ++iter_test )
		if ( iter_test->get_value( pTMCRecord->mTMColumn_SourcePath ) == str_sourcepath )
			(*iter_test)[pTMCRecord->mTMColumn_Password] = str_password;
	
	return;
}


// Method (public) : create command
std::string
cLStore_Cat::createCommand(
	bool shuffle )
{
	guint
		file_id = 0;

	std::string
		str_command;

	Gtk::TreeModel::iterator
		iter_row;

	// Indicate each active file (not record) at list with an unique file id number
	indicateFileIDs();

	// Assign a handle to each active file (created from the file id number)
	file_id = 0;
	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row ) {
		if ( file_id == iter_row->get_value( pTMCRecord->mTMColumn_Id ) ) {
			str_command.append( " " );
			str_command.append( getHandle( file_id++ ) );
			str_command.append( "=" );
			str_command.append( Pdfchain::quote_path(
				iter_row->get_value( pTMCRecord->mTMColumn_SourcePath ) ) );
		}
	}
	if ( str_command.empty() )
		return str_command;

	// Add the pdftk option "input_pw" only if at least one active file has an input password
	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row ) {
		if ( G_MAXUINT != iter_row->get_value( pTMCRecord->mTMColumn_Id ) ) {
			if ( ! iter_row->get_value( pTMCRecord->mTMColumn_Password ).empty() ) {
				str_command.append( " " );
				str_command.append( Pdfchain::Cmd::PASSWORD_INPUT );
				break;
			}
		}
	}

	// Assign the input passowrd on each handle
	file_id = 0;
	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row ) { 
		if ( file_id == iter_row->get_value( pTMCRecord->mTMColumn_Id ) ) {
			if ( ! iter_row->get_value( pTMCRecord->mTMColumn_Password ).empty() ) {
				str_command.append( " " );
				str_command.append( getHandle( file_id ) );
				str_command.append( "=" );
				str_command.append( Glib::filename_from_utf8(
					iter_row->get_value( pTMCRecord->mTMColumn_Password ) ) );
			}
			file_id++;
		}
	}

	// Select Cat or Shuffle
	str_command.append( " " );
	if ( true == shuffle ) {
		str_command.append( Pdfchain::Cmd::SHUFFLE );
	}
	else {
		str_command.append( Pdfchain::Cmd::CAT );
	}

	// Assign Pages, EvenOdd, Rotation on each handle
	for ( iter_row = children().begin() ; children().end() != iter_row ; ++iter_row ) {
		if ( G_MAXUINT > ( file_id = iter_row->get_value( pTMCRecord->mTMColumn_Id ) ) ) {

			// Create command
			if ( true != validateTMColumn_Pages( iter_row ) ) {
				return "";
			}
			else {
				std::string::size_type
					pos_a = 0,
					pos_b = 0;

				std::string
					str_pages = Glib::filename_from_utf8(
						iter_row->get_value( pTMCRecord->mTMColumn_Pages ) );
					
				while ( str_pages.length() > pos_a
					&& str_pages.length() > pos_b ) {

					// Find begin and end of an argument (arguments are separated by ' ')
					for ( pos_a = pos_b ;
						str_pages.length() > pos_a && 0 == str_pages.compare( pos_a , 1 , " " ) ;
						pos_a++ );	// Skips all ' '

					for ( pos_b = pos_a ;
						str_pages.length() > pos_b && 0 != str_pages.compare( pos_b , 1 , " " ) ;
						pos_b++ );	// Skips all characters, without ' '

					// Append argument
					str_command.append( " " );
					str_command.append( getHandle( file_id ) );
					str_command.append( str_pages.substr( pos_a , pos_b - pos_a ) );

					// EvenOdd (only it there is a page range sign "-" at pages string)
					if ( std::string::npos != ( str_pages.substr( pos_a , pos_b - pos_a )).find( "-" ) )
						str_command.append( iter_row->get_value( pTMCRecord->mTMColumn_Cmd_EvenOdd ) );

					// Rotation
					str_command.append( iter_row->get_value( pTMCRecord->mTMColumn_Cmd_Rotation ) );
				}
			}
		}
	}

	return str_command;
}


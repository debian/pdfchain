/*
 * window_main_burst.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_burst.h"


/*** Combobox : Output ID *****************************************************/

cCBox_CountingBase::cCBox_CountingBase()
{
	rLStore		= cLStore_CountingBase::create();
	pTMCRecord	= rLStore->getTMCRecord();

	pack_start( mCRText );
	add_attribute( mCRText.property_text() , pTMCRecord->mTMColumn_Label );	// necessarily after 'pack_start()' !!!
	set_model( rLStore );
	set_active( 1 );
}


// Destructor
cCBox_CountingBase::~cCBox_CountingBase()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cCBox_CountingBase::~cCBox_CountingBase()";	//TEST
#endif
}


/*** Section Burst ************************************************************/

// Constructor
cSection_Burst::cSection_Burst( Gtk::Window& ref_window )
:
	Gtk::VBox( false , Pdfchain::SPACING ) ,	// homogeneous , spacing

	mHBox_SourceFile( false , Pdfchain::SPACING ) ,	// homogeneous , spacing
	mHBox_Prefix(     false , Pdfchain::SPACING ) ,
	mHBox_Digits(     false , Pdfchain::SPACING ) ,

	mVBox_Pattern(    false , Pdfchain::SPACING ) ,

	mLabel_Pattern(    _("Output PDF pattern") , Gtk::ALIGN_CENTER , Gtk::ALIGN_CENTER , false ) ,
	mLabel_Extended(   _("Extended options")   , Gtk::ALIGN_CENTER , Gtk::ALIGN_CENTER , false ) ,
	mLabel_SourceFile( _("Document:") , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),	// label , xalign , yalign , mnemonic
	mLabel_Prefix(     _("Prefix:")   , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),
	mLabel_Base(       _("Base:")     , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),
	mLabel_Digits(     _("Digits:")   , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),
	mLabel_Suffix(     _("Suffix:")   , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),

	mRBGroup_CounterDigits( mRButton_Auto.get_group() ),
	mRButton_Auto(   mRBGroup_CounterDigits , _("Auto")	   , false ),	// group , label , mnemonic
	mRButton_Manual( mRBGroup_CounterDigits , _("Manual:") , false ),

	rAdjust_Digits( Gtk::Adjustment::create(
		0.0 , 0.0 , Pdfchain::Burst::ADJUST_UPPER_DIGITS , 1.0 , 1.0 , 0.0 ) ),	// value , lower , upper , step_inc , page_inc , page_size
	mSButton_Digits( rAdjust_Digits ),

	mCButton_Extension( _("Extension") , false ) ,	// label , mnemonic

	sPrefix( _("Page") ),
	sSuffix( _("") ),
	vPageNumbers( 0 ),

	mFCButton_SourceFile(   ref_window , _("Burst - Select source PDF file ...") ) ,	// parent_window , title
	mFCDialog_SelectFolder( ref_window , _("Burst - Select output folder ...") ) ,

	rSGroup_LabelPattern( Gtk::SizeGroup::create( Gtk::SIZE_GROUP_HORIZONTAL ) )
{
	// Assemble Widgets
	mLabel_Pattern.set_tooltip_text(     _("Prepare the pattern for the output PDF file names") );
	mLabel_Extended.set_tooltip_text(    _("Extended options to format the pattern") );
	mLabel_SourceFile.set_tooltip_text(  _("The source PDF file. "
		"Every single page of this document will be saved in a seperate PDF file") );
	mLabel_Prefix.set_tooltip_text(	     _("Prefix part of the file name pattern") );
	mLabel_Base.set_tooltip_text(        _("Numbering base for the file counter part") );
	mLabel_Digits.set_tooltip_text(      _("Number of digits for the file counter part") );  
	mLabel_Suffix.set_tooltip_text(      _("Suffix part of the file name pattern") );  
	mCButton_Extension.set_tooltip_text( _("Appends the extension \".pdf\" part") );
	mRButton_Auto.set_tooltip_text(      _("Calculate number of counter digits automatically") );
	mRButton_Manual.set_tooltip_text(    _("Define number of counter digits manually") );

	mFrame_Pattern.set_label_widget( mLabel_Pattern );
	mExpander_Extended.set_label_widget( mLabel_Extended );

	mEntry_Prefix.set_icon_from_stock(	Gtk::Stock::CLEAR , Gtk::ENTRY_ICON_SECONDARY );
	mEntry_Suffix.set_icon_from_stock(	Gtk::Stock::CLEAR , Gtk::ENTRY_ICON_SECONDARY );

	mFCButton_SourceFile.set_halign( Gtk::ALIGN_START );
	mEntry_Prefix.set_halign(        Gtk::ALIGN_START );
	mCBox_CountingBase.set_halign(   Gtk::ALIGN_START );
	mHBox_Digits.set_halign(         Gtk::ALIGN_START );
	mEntry_Suffix.set_halign(        Gtk::ALIGN_START );

	rSGroup_LabelPattern->add_widget( mLabel_Prefix );
	rSGroup_LabelPattern->add_widget( mLabel_Suffix );
	rSGroup_LabelPattern->add_widget( mLabel_Digits );
	rSGroup_LabelPattern->add_widget( mLabel_Base );

	mHBox_SourceFile.pack_start( mLabel_SourceFile    , false , false , 0 );	// widget , expand , fill , padding
	mHBox_SourceFile.pack_start( mFCButton_SourceFile , false , true  , 0 );

	mHBox_Prefix.pack_start( mLabel_Prefix , false , false , 0 );
	mHBox_Prefix.pack_start( mEntry_Prefix , false , true  , 0 );

	mHBox_Digits.pack_start( mRButton_Auto   , false , false , 0 );
	mHBox_Digits.pack_start( mRButton_Manual , false , false , 0 );
	mHBox_Digits.pack_start( mSButton_Digits , false , false , 0 );

	mTable_Extended.set_margin_top( Pdfchain::BORDER );
	mTable_Extended.set_spacings( Pdfchain::SPACING );
	mTable_Extended.attach( mLabel_Base         , 0 , 1 , 0 , 1 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mCBox_CountingBase  , 1 , 2 , 0 , 1 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mLabel_Digits       , 0 , 1 , 1 , 2 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mHBox_Digits        , 1 , 2 , 1 , 2 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mLabel_Suffix       , 0 , 1 , 2 , 3 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mEntry_Suffix       , 1 , 2 , 2 , 3 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Extended.attach( mCButton_Extension  , 1 , 2 , 3 , 4 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );

	mExpander_Extended.add( mTable_Extended );

	mVBox_Pattern.set_border_width( Pdfchain::BORDER );
	mVBox_Pattern.pack_start( mHBox_Prefix );
	mVBox_Pattern.pack_start( mExpander_Extended );

	mFrame_Pattern.add( mVBox_Pattern );

	pack_start( mHBox_SourceFile , false , false , 0 );
	pack_start( mFrame_Pattern   , false , false , 0 );

	// Connect File Handler
	mEntry_Prefix.signal_icon_press().connect( sigc::mem_fun(
		*this , &cSection_Burst::onEntryIcon_Prefix_pressed ) );
		
	mEntry_Suffix.signal_icon_press().connect( sigc::mem_fun(
		*this , &cSection_Burst::onEntryIcon_Suffix_pressed ) );
		
	mCBox_CountingBase.signal_changed().connect( sigc::mem_fun(
		*this , &cSection_Burst::onCBox_CountingBase_changed ) );
		
	mRButton_Auto.signal_toggled().connect( sigc::mem_fun(
		*this , &cSection_Burst::onRButton_Auto_toggled ) );
		
	mFCButton_SourceFile.signal_file_set().connect( sigc::mem_fun(
		*this , &cSection_Burst::onFCButton_SourceFile_file_set ) );
	
	// Init Object
	init();
}


// Destructor
cSection_Burst::~cSection_Burst()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_Burst::~cSection_Burst()";	//TEST
#endif
}


// Method (public) : clear
void
cSection_Burst::clear()
{
	init();
	
	vPageNumbers = 0;
	vDigits_Oct  = 0;
	vDigits_Dec  = 0;
	vDigits_Hex  = 0;
	
	return;
}


// Method (public) : create command
std::string
cSection_Burst::createCommand()
{
	std::string str_command			= "";
	std::string str_sourcefile		= "";
	std::string str_targetfolder	= "";
	std::string str_bursttemplate	= "";
	std::string str_prefix			= Glib::filename_from_utf8( mEntry_Prefix.get_text() );
	std::string str_suffix			= Glib::filename_from_utf8( mEntry_Suffix.get_text() );

	if ( "" != ( str_sourcefile = mFCButton_SourceFile.get_filename() ) ) {
		
		switch ( mFCDialog_SelectFolder.run() ) {
			case Gtk::RESPONSE_OK: {
				mFCDialog_SelectFolder.hide();

				if ( "" != ( str_targetfolder = mFCDialog_SelectFolder.get_current_folder() ) ) {

					Gtk::TreeModel::iterator iter      = mCBox_CountingBase.get_active();
					cTMCRecord_Selection*    pt_record = mCBox_CountingBase.rLStore->getTMCRecord();

					str_bursttemplate  = str_targetfolder;
					str_bursttemplate += G_DIR_SEPARATOR_S;
					str_bursttemplate += str_prefix;
					str_bursttemplate += Pdfchain::Cmd::TEMPLATE;

					if ( true == mRButton_Auto.get_active() ) {
						switch ( (*iter)[pt_record->mTMColumn_ID] ) {
							case Pdfchain::Id::COUNT_OCT :
								str_bursttemplate += Pdfchain::convert_guint_to_string( vDigits_Oct );
								break;
							case Pdfchain::Id::COUNT_DEC :
								str_bursttemplate += Pdfchain::convert_guint_to_string( vDigits_Dec );
								break;
							case Pdfchain::Id::COUNT_HEX :
								str_bursttemplate += Pdfchain::convert_guint_to_string( vDigits_Hex );
								break;
						}
					} else {
						str_bursttemplate += mSButton_Digits.get_text();
					}

					str_bursttemplate += iter->get_value( pt_record->mTMColumn_Command );
					str_bursttemplate += str_suffix;

					if ( mCButton_Extension.get_active() )
						str_bursttemplate += Pdfchain::Cmd::EXTENSION;

					str_command  = " " + Pdfchain::quote_path( str_sourcefile );
					str_command += " " + Pdfchain::Cmd::BURST;
					str_command += " " + Pdfchain::Cmd::OUTPUT;
					str_command += " " + Pdfchain::quote_path( str_bursttemplate );
				}
				break;
			}
			default:
				mFCDialog_SelectFolder.hide();
				break;
		}
	}

	return str_command;
}


// Method (protected) : init()
void 
cSection_Burst::init()
{
/*
	vPageNumbers = 0;
	vDigits_Oct  = 0;
	vDigits_Dec  = 0;
	vDigits_Hex  = 0;
*/
	mFCButton_SourceFile.clear();
	mFCDialog_SelectFolder.clear();

	mEntry_Prefix.set_text( sPrefix );
	mEntry_Suffix.set_text( sSuffix );
	mExpander_Extended.set_expanded( false );
	mRButton_Auto.set_active( true );
	rAdjust_Digits->set_value( 0.0 );
	mSButton_Digits.set_sensitive( false );
	mCBox_CountingBase.set_active( 1 );
	mCButton_Extension.set_active( true );
	
	return;
}



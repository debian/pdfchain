/*
 * window_main_permission.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_permission.h"


/*** Section : Permission *****************************************************/

// Constructor
cSection_Permission::cSection_Permission()
:
	Gtk::HBox( false , Pdfchain::SPACING ),	// homogenous , spacing

	mTable_Allow( 4 , 2 , false ),	// y_rows , x_columns , homogenous

	mLabel_PasswordUser(  _("User password")  , false ),
	mLabel_PasswordOwner( _("Owner password") , false ),
	mLabel_Encryption(    _("Encryption")     , false ),
	mLabel_Allow(         _("Allow")          , false ),

	mCButton_Printing(          _("Printing")           , false ),	// label , mnemonic
	mCButton_DegradedPrinting(  _("Degraded printing")  , false ),
	mCButton_ModifyContents(    _("Modify contents")    , false ),
	mCButton_Assembly(          _("Assembly")           , false ),
	mCButton_CopyContents(      _("Copy contents")      , false ),
	mCButton_Screenreaders(     _("Screenreaders")      , false ),
	mCButton_ModifyAnnotations( _("Modify annotations") , false ),
	mCButton_FillIn(            _("Fill in")            , false ),

	mRButton_EncryptNone(   _("None")    , false ),	// label , mnemonic
	mRButton_Encrypt40Bit(  _("RC4 40")  , false ),
	mRButton_Encrypt128Bit( _("RC4 128") , false )
{
	// Tooltips
	mLabel_PasswordUser.set_tooltip_text(
		_("Protects from unauthorized reading "
			"\nMax. 32 signs (Latin-1)") );
	mLabel_PasswordOwner.set_tooltip_text(
		_("Protects the restrictions "
			"\nMax. 32 signs (Latin-1)") );
	mLabel_Encryption.set_tooltip_text(
		_("If an encryption strength is given but no passwords are suplied, then the "
			"owner and user passwords remain empty, which  means that the resulting "
			"PDF may be opened and its security parameters altered by anybody.") );
	mLabel_Allow.set_tooltip_text(
		_("Permissions are applied to the output document only if an encryption "
			"strength is specified or an owner or user password is given "
			"(doesn't work with all document readers)") );

	mCButton_Printing.set_tooltip_text(          _("Top quality printing") );
	mCButton_DegradedPrinting.set_tooltip_text(  _("Lower quality printing") );
	mCButton_CopyContents.set_tooltip_text(      _("Allows to copy contents\n(also allows screenreaders)") );
	mCButton_Screenreaders.set_tooltip_text(     _("Allows to read contents with screenreaders") );
	mCButton_ModifyContents.set_tooltip_text(    _("Allows to modify contents\n(also allows assembly)") );
	mCButton_Assembly.set_tooltip_text(          _("Allows to assemble contents") );
	mCButton_ModifyAnnotations.set_tooltip_text( _("Allows to modify annotations\n(also allows to fill in)") );
	mCButton_FillIn.set_tooltip_text(            _("Allows to fill in annotations") );

	mRButton_EncryptNone.set_tooltip_text(   _("Don't encrypt output document") );
	mRButton_Encrypt40Bit.set_tooltip_text(  _("Encrypt output document with RC4 40-bit cypher (PDF 1.1 - 1.3)") );
	mRButton_Encrypt128Bit.set_tooltip_text( _("Encrypt output document with RC4 128-bit cypher (PDF 1.4)") );

	// Assembly GUI
	mLabel_PasswordUser.set_use_markup(  false  );
	mLabel_PasswordOwner.set_use_markup( false  );
	mLabel_Encryption.set_use_markup(    false  );
	mLabel_Allow.set_use_markup(         false  );

	mFrame_PasswordUser.set_label_widget(  mLabel_PasswordUser );
	mFrame_PasswordOwner.set_label_widget( mLabel_PasswordOwner );
	mFrame_Encryption.set_label_widget(    mLabel_Encryption );
	mFrame_Allow.set_label_widget(         mLabel_Allow );
	
	mEntry_PasswordOwner.set_icon_from_stock( Gtk::Stock::CLEAR , Gtk::ENTRY_ICON_SECONDARY );
	mEntry_PasswordUser.set_icon_from_stock(  Gtk::Stock::CLEAR , Gtk::ENTRY_ICON_SECONDARY );

	mEntry_PasswordOwner.set_max_length( 32 );	// Password length is max. 32 signs (latin-1)
	mEntry_PasswordUser.set_max_length( 32 );

	mRBGroup_Encrypt = mRButton_EncryptNone.get_group();
	mRButton_Encrypt40Bit.set_group(  mRBGroup_Encrypt );
	mRButton_Encrypt128Bit.set_group( mRBGroup_Encrypt );

	mTable_Allow.set_homogeneous( true );
	mTable_Allow.set_col_spacings( Pdfchain::SPACING );
	mTable_Allow.attach( mCButton_Printing          , 0 , 1 , 0 , 1 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_DegradedPrinting  , 1 , 2 , 0 , 1 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_CopyContents      , 0 , 1 , 1 , 2 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_Screenreaders     , 1 , 2 , 1 , 2 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_ModifyContents    , 0 , 1 , 2 , 3 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_Assembly          , 1 , 2 , 2 , 3 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_ModifyAnnotations , 0 , 1 , 3 , 4 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );
	mTable_Allow.attach( mCButton_FillIn            , 1 , 2 , 3 , 4 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::EXPAND , 0 , 0 );

	mVBox_Encryption.pack_start( mRButton_EncryptNone   , true , true , 0 );	// widget , expand , fill , padding
	mVBox_Encryption.pack_start( mRButton_Encrypt40Bit  , true , true , 0 );
	mVBox_Encryption.pack_start( mRButton_Encrypt128Bit , true , true , 0 );

	mAlign_PasswordOwner.set_padding( 0 , 0 , Pdfchain::PADDING , Pdfchain::PADDING );	// top , bottom , left , right
	mAlign_PasswordUser.set_padding(  0 , 0 , Pdfchain::PADDING , Pdfchain::PADDING );
	mAlign_Encryption.set_padding(    0 , 0 , Pdfchain::PADDING , Pdfchain::PADDING );
	mAlign_Allow.set_padding(         0 , 0 , Pdfchain::PADDING , Pdfchain::PADDING );

	mAlign_PasswordOwner.add( mEntry_PasswordOwner );
	mAlign_PasswordUser.add(  mEntry_PasswordUser );
	mAlign_Encryption.add(    mVBox_Encryption );
	mAlign_Allow.add(         mTable_Allow );

	mFrame_PasswordUser.add(  mAlign_PasswordUser );
	mFrame_PasswordOwner.add( mAlign_PasswordOwner );
	mFrame_Encryption.add(    mAlign_Encryption );
	mFrame_Allow.add(         mAlign_Allow );
	
	mVBox_Passwords.pack_start( mFrame_PasswordUser  , true , true , 0 );	// widget , expand , fill , padding
	mVBox_Passwords.pack_start( mFrame_PasswordOwner , true , true , 0 );

	pack_start( mVBox_Passwords    , true  , true  , 0 );	// widget , expand , fill , padding
	pack_start( mFrame_Encryption  , false , false , 0 );		
	pack_start( mFrame_Allow       , false , false , 0 );

	// Connect Signal Handler
	mEntry_PasswordOwner.signal_icon_press().connect(
		sigc::mem_fun( *this , &cSection_Permission::onEntryIcon_PasswordOwner_pressed ) );

	mEntry_PasswordUser.signal_icon_press().connect(
		sigc::mem_fun( *this , &cSection_Permission::onEntryIcon_PasswordUser_pressed ) );

	mEntry_PasswordUser.signal_changed().connect(
		sigc::mem_fun( *this , &cSection_Permission::onEntry_Password_changed ) );

	mEntry_PasswordOwner.signal_changed().connect(
		sigc::mem_fun( *this , &cSection_Permission::onEntry_Password_changed ) );

	mRButton_EncryptNone.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Permission::onRButton_EncryptNone_toggled ) );

	mCButton_ModifyContents.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Permission::onCButton_ModifyContents_toggled ) );

	mCButton_CopyContents.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Permission::onCButton_CopyContents_toggled ) );

	mCButton_ModifyAnnotations.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Permission::onCButton_ModifyAnnotations_toggled ) );

	// Init Section
	init();
}


// Destructor
cSection_Permission::~cSection_Permission()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_Permission::~cSection_Permission()";	//TEST
#endif
}


// Method (public) : clear()
void
cSection_Permission::clear()
{
	init();
	return;
}


// Method (public) : create command
Glib::ustring
cSection_Permission::createCommand()
{
	Glib::ustring
		str_command;

	
	// Passwords
	if ( mEntry_PasswordOwner.get_text_length() || mEntry_PasswordUser.get_text_length() ) { 

		if ( mEntry_PasswordOwner.get_text() == mEntry_PasswordUser.get_text() ) {
			std::cerr << "WARNING: User and owner passwords are the same. "
				"Supply user password only!" << std::endl;

			str_command += " " + Pdfchain::Cmd::PASSWORD_USER
				+ " " + mEntry_PasswordUser.get_text();
		}
		else {
		
			if ( mEntry_PasswordUser.get_text_length() ) {
				str_command += " " + Pdfchain::Cmd::PASSWORD_USER
					+ " " + Pdfchain::quote_path( mEntry_PasswordUser.get_text() );
			}

			if ( mEntry_PasswordOwner.get_text_length() ) {
				str_command += " " + Pdfchain::Cmd::PASSWORD_OWNER 
					+ " " + Pdfchain::quote_path( mEntry_PasswordOwner.get_text() );
			}
		}
	}

	// Allow
	if ( ! mRButton_EncryptNone.get_active() ) {

		if ( ! ( // if NOT all NOT active (means if one or more active)
			! mCButton_Printing.get_active()             &&  ! mCButton_DegradedPrinting.get_active()    &&
			! mCButton_CopyContents.get_active()      /* &&  ! mCButton_Screenreaders.get_active()    */ &&
			! mCButton_ModifyContents.get_active()    /* &&  ! mCButton_Assembly.get_active()         */ &&
			! mCButton_ModifyAnnotations.get_active() /* &&  ! mCButton_FillIn.get_active()           */ ) )
		{
			str_command += " " + Pdfchain::Cmd::ALLOW;

			if (	// if all active
				mCButton_Printing.get_active()             && mCButton_DegradedPrinting.get_active()    &&
				mCButton_CopyContents.get_active()      /* && mCButton_Screenreaders.get_active()    */ &&
				mCButton_ModifyContents.get_active()    /* && mCButton_Assembly.get_active()         */ &&
				mCButton_ModifyAnnotations.get_active() /* && mCButton_FillIn.get_active()           */ )
			{
			     str_command += " " + Pdfchain::Cmd::FEATURES_ALL;
			}
			else {
				if ( mCButton_Printing.get_active() )
					str_command += " " + Pdfchain::Cmd::PRINTING;
			
				if ( mCButton_DegradedPrinting.get_active() )
					str_command += " " + Pdfchain::Cmd::DEGRADED_PRINTING;
				
				if ( mCButton_CopyContents.get_active() )
					str_command += " " + Pdfchain::Cmd::COPY_CONTENTS;		// CopyContens also allows Screenreaders
				else if ( mCButton_Screenreaders.get_active() )
					str_command += " " + Pdfchain::Cmd::SCREEN_READERS;

				if ( mCButton_ModifyContents.get_active() )
					str_command += " " + Pdfchain::Cmd::MODIFY_CONTENTS;	// ModifyContents also allows Assembly
				else if ( mCButton_Assembly.get_active() )
					str_command += " " + Pdfchain::Cmd::ASSEMBLY;

				if ( mCButton_ModifyAnnotations.get_active() )
					str_command += " " + Pdfchain::Cmd::MODIFY_ANNOTATIONS;	// ModifyAnnotations also allows FillIn
				else if ( mCButton_FillIn.get_active() )
					str_command += " " + Pdfchain::Cmd::FILL_IN;
			}
		}
	}

	// Encryption
	if ( mRButton_Encrypt40Bit.get_active() ) {
		str_command += " " + Pdfchain::Cmd::ENCRYPT_40BIT;
	}
	else if ( mRButton_Encrypt128Bit.get_active() ) {
		str_command += " " + Pdfchain::Cmd::ENCRYPT_128BIT;
	}

	return str_command;
}


// Method (protected) : init()
void
cSection_Permission::init()
{
	mCButton_Printing.set_active( true );
	mCButton_DegradedPrinting.set_active( true );
	mCButton_ModifyContents.set_active( true );
	mCButton_Assembly.set_active( true );
	mCButton_CopyContents.set_active( true );
	mCButton_Screenreaders.set_active( true );
	mCButton_ModifyAnnotations.set_active( true );
	mCButton_FillIn.set_active( true );

	mEntry_PasswordOwner.set_text( "" );
	mEntry_PasswordUser.set_text( "" );
	
	mRButton_EncryptNone.set_active( true );
	mRButton_EncryptNone.set_sensitive( true );
	mFrame_Allow.set_sensitive( false );

	return;
}

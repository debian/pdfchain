/*
 * window_main_attachment.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_attachment.h"


/*** Tree View : Attachment ***************************************************/

// Constructor
cTView_Attach::cTView_Attach( Gtk::Window& ref_window )
:
	pTMCRecord_Attach( NULL ) ,

#ifdef PDFCHAIN_TEMP
	mTVColumn_AttachPath( "Attachment path" ) ,	//TEMP
	mTVColumn_AttachTooltip( "Tooltip" ) ,		//TEMP
#endif

	mTVColumn_AddPages(   _("Add") ) ,
	mTVColumn_AttachFile( _("Attachment files") )
{
	// Creating List Store
	rLStore_Attach = cLStore_Attach::create( ref_window );
	pTMCRecord_Attach = rLStore_Attach->getTMCRecord();
	get_selection()->set_mode( Gtk::SELECTION_MULTIPLE );

	// Setup Tree View
	set_model( rLStore_Attach );
	set_reorderable( true );
	set_rules_hint( true );
//	set_rubber_banding( true );
	set_rubber_banding( false );
	set_enable_search( true );
	set_search_column( pTMCRecord_Attach->mTMColumn_AttachFile );
	set_tooltip_column( get_search_column() + 2 );
	
	// Setup Widgets (Columns)
	mTVColumn_AttachFile.set_expand( true );
	mTVColumn_AttachFile.set_resizable( true );

#ifdef PDFCHAIN_TEMP
	mTVColumn_AttachPath.set_expand( true );		//TEMP
	mTVColumn_AttachPath.set_resizable( true );		//TEMP
	mTVColumn_AttachTooltip.set_expand( true );		//TEMP
	mTVColumn_AttachTooltip.set_resizable( true );	//TEMP
#endif
	
	mCRToggle_Add.property_activatable() = true;

	mTVColumn_AddPages.pack_start(   mCRToggle_Add      , false );	// cell renderer , expand
	mTVColumn_AttachFile.pack_start( mCRText_AttachFile , true );

#ifdef PDFCHAIN_TEMP
	mTVColumn_AttachPath.pack_start(    mCRText_AttachPath    , true );	//TEMP
	mTVColumn_AttachTooltip.pack_start( mCRText_AttachTooltip , true );	//TEMP
#endif
	
	mTVColumn_AddPages.add_attribute(
		mCRToggle_Add.property_active() ,
		pTMCRecord_Attach->mTMColumn_Add );

	mTVColumn_AttachFile.add_attribute(
		mCRText_AttachFile.property_text() ,
		pTMCRecord_Attach->mTMColumn_AttachFile );

#ifdef PDFCHAIN_TEMP
	mTVColumn_AttachPath.add_attribute(
		mCRText_AttachPath.property_text() ,
		pTMCRecord_Attach->mTMColumn_AttachPath );		//TEMP

	mTVColumn_AttachTooltip.add_attribute(
		mCRText_AttachTooltip.property_text() ,
		pTMCRecord_Attach->mTMColumn_AttachTooltip );	//TEMP
#endif
	
	append_column( mTVColumn_AddPages );
	append_column( mTVColumn_AttachFile );

#ifdef PDFCHAIN_TEMP
	append_column( mTVColumn_AttachPath );		//TEMP
	append_column( mTVColumn_AttachTooltip );	//TEMP
#endif

	// Connect Signal Handler
	mCRToggle_Add.signal_toggled().connect(	sigc::mem_fun(
		*this , &cTView_Attach::onCRToggle_Add_toggled	) );
}


// Destructor
cTView_Attach::~cTView_Attach()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cTView_Attach::~cTView_Attach()";	//TEST
#endif
}


// Method (public) : clear
void
cTView_Attach::clear()
{
	rLStore_Attach->clear();
	return;
}



/*** Toolbar : Attach **********************************************************/

// Constructor
cToolbar_Attach::cToolbar_Attach( cTView_Attach* pt_tview_attach )
:
	rAdjust_PageNumber( Gtk::Adjustment::create( 
		1.0 , 1.0 , Pdfchain::Attach::ADJUST_UPPER_PAGE_NUMBERS , 1.0 , 1.0 , 0.0 ) ) ,	// value , lower , upper , step_inc , page_inc , page_size

	mSButton_PageNumber( rAdjust_PageNumber	) ,

	mToolButton_Add(    Gtk::Stock::ADD ) ,
	mToolButton_Remove( Gtk::Stock::REMOVE ) ,
	mToolButton_Up(     Gtk::Stock::GO_UP ) ,
	mToolButton_Down(   Gtk::Stock::GO_DOWN )
{
	// Setup Widgets
	mSeparator_B.set_draw( false );
	mSeparator_B.set_expand( true );
	mSeparator_C.set_draw( false );

	mRTBGroup_AddTo	= mRTButton_AddToFile.get_group();
	mRTButton_AddToPage.set_group( mRTBGroup_AddTo );
	mRTButton_AddToFile.set_label( _("Add to file") );
	mRTButton_AddToPage.set_label( _("Add to page") );
	mRTButton_AddToFile.set_is_important( true );
	mRTButton_AddToPage.set_is_important( true );

	mToolButton_Add.set_tooltip_text(    _("Add some files to attach") );
	mToolButton_Remove.set_tooltip_text( _("Remove selected files") );
	mToolButton_Up.set_tooltip_text(     _("Move selected files up") );
	mToolButton_Down.set_tooltip_text(   _("Move selected files down") );
	                                  
	mToolItem_PageNumber.add( mSButton_PageNumber );
	
	add( mToolButton_Add );
	add( mToolButton_Remove );
	add( mSeparator_A );
	add( mToolButton_Up );
	add( mToolButton_Down );
	add( mSeparator_B );
	add( mRTButton_AddToFile );
	add( mRTButton_AddToPage );
	add( mSeparator_C );
	add( mToolItem_PageNumber );
	
	// Init Object
	clear();

	// Connect Signal Handler
	mRTButton_AddToPage.signal_toggled().connect( sigc::mem_fun(
		*this , &cToolbar_Attach::onRTButton_AddToPage_toggled ) );

	mToolButton_Add.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_attach , &cTView_Attach::onToolButton_Add_clicked ) );
		
	mToolButton_Remove.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_attach , &cTView_Attach::onToolButton_Remove_clicked ) );
		
	mToolButton_Up.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_attach , &cTView_Attach::onToolButton_Up_clicked ) );
		
	mToolButton_Down.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_attach , &cTView_Attach::onToolButton_Down_clicked ) );
}


// Destructor
cToolbar_Attach::~cToolbar_Attach()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cToolbar_Attach::~cToolbar_Attach()";	//TEST
#endif
}


// Method (public) : clear
void
cToolbar_Attach::clear()
{
	mRTButton_AddToFile.set_active( true );
	mSButton_PageNumber.set_sensitive( false );
	return;
}

/*** Section : Attach *********************************************************/

// Constructor
cSection_Attach::cSection_Attach( Gtk::Window& ref_window )
:
	mHBox_SourceFile( false , Pdfchain::SPACING ) ,	// homogenous , spacing
	mLabel_SourceFile( _("Document:") , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ) ,	// label , xalign , yalign , mnemonic
	mFCButton_SourceFile( ref_window , _("Attachment - Select source PDF file ...") ) ,
	mFCDialog_SaveAs(     ref_window , _("Attachment - Save output PDF file as ...") ) ,
	mTView_AttachFiles( ref_window ) ,
	mToolbar_AttachFiles( &mTView_AttachFiles ) ,
	vPageNumbers( 0 )
{
	mFCButton_SourceFile.set_halign( Gtk::ALIGN_START );

	mHBox_SourceFile.set_border_width( Pdfchain::BORDER );
	mHBox_SourceFile.pack_start( mLabel_SourceFile    , false , false , 0 );	// widget , expand , fill , padding
	mHBox_SourceFile.pack_start( mFCButton_SourceFile , true  , true ,  0 );
	                            
	mSWindow_AttachFiles.add( mTView_AttachFiles );

	pack_start( mHBox_SourceFile     , false , false , 0 );	// widget , expand , fill , padding
	pack_start( mSWindow_AttachFiles , true  , true  , 0 );
	pack_start( mToolbar_AttachFiles , false , false , 0 );

	// Connect Signal Handler
	mFCButton_SourceFile.signal_file_set().connect( sigc::mem_fun(
		*this , &cSection_Attach::onFCButton_SourceFile_file_set ) );
}


// Destructor
cSection_Attach::~cSection_Attach()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_Attach::~cSection_Attach()";	//TEST
#endif
}


// Method (public) : clear
void
cSection_Attach::clear()
{
	mFCButton_SourceFile.clear();
	mToolbar_AttachFiles.clear();
	mTView_AttachFiles.clear();
	mFCDialog_SaveAs.clear();   
	return;
}

// Method (public) : create command
std::string
cSection_Attach::createCommand()
{
	std::string
		str_command ,
		str_sourcefile ,
		str_attachfiles ,
		str_targetfile;

	if ( "" != ( str_sourcefile = mFCButton_SourceFile.get_filename() ) ) {

		if ( "" != ( str_attachfiles = mTView_AttachFiles.rLStore_Attach->createCommand() ) ) {

			switch ( mFCDialog_SaveAs.run() ) {

				case Gtk::RESPONSE_OK:
					mFCDialog_SaveAs.hide();
					
					str_targetfile = mFCDialog_SaveAs.get_filename();
					if ( ! str_targetfile.empty() ) {

						str_command  = " " + Pdfchain::quote_path( str_sourcefile );
						str_command += str_attachfiles;
						
						if ( mToolbar_AttachFiles.mRTButton_AddToPage.get_active() ) {
							str_command += " " + Pdfchain::Cmd::TO_PAGE + " " +
								static_cast<std::string>( mToolbar_AttachFiles.mSButton_PageNumber.get_text() );
						}

						str_command += " " + Pdfchain::Cmd::OUTPUT;
						str_command += " " + Pdfchain::quote_path( str_targetfile );
					}
					break;
				
				default:
					mFCDialog_SaveAs.hide();
					break;
			}
		}
	}
	
	return str_command;
}



// Signal Handler (protected)
void
cSection_Attach::onFCButton_SourceFile_file_set()
{
	vPageNumbers = Pdfchain::count_page_numbers( 
		mFCButton_SourceFile.get_filename() );
			
	if ( 0 != vPageNumbers ) {

//		mToolbar_AttachFiles.mSButton_PageNumber.override_color(
//			Pdfchain::Color::RGBA_VALID );

		mToolbar_AttachFiles.rAdjust_PageNumber->set_upper(
			static_cast<double>( vPageNumbers ) );

		if ( static_cast<guint>(
			mToolbar_AttachFiles.mSButton_PageNumber.get_value() ) > vPageNumbers )
		{
			mToolbar_AttachFiles.mSButton_PageNumber.set_value(
				static_cast<double>( vPageNumbers ) );
		}
	}
	else {
//		mToolbar_AttachFiles.mSButton_PageNumber.override_color(
//			Pdfchain::Color::RGBA_WARNING );

		mToolbar_AttachFiles.rAdjust_PageNumber->set_upper(
			Pdfchain::Attach::ADJUST_UPPER_PAGE_NUMBERS );
	}
	
	return;
}

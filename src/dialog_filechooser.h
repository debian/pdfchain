/*
 * dialog_filechooser.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_DIALOG_FILECHOOSER_H__
#define __PDFCHAIN_DIALOG_FILECHOOSER_H__

#include "pdfchain.h"
#include "dialog_filechooser_extension.h"


/*** File Chooser Dialog ******************************************************/

/*** File Chooser Dialog - Add File *******************************************/

// Class : File Chooser Dialog - Add File
class
cFCDialog_AddFile : public Gtk::FileChooserDialog
{
	public:
		         cFCDialog_AddFile( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_AddFile();
		
		void clear();
		int run();

	protected:
	
		void init();
};


// Class : File Chooser Dialog - Add File - Any
class
cFCDialog_AddFile_Any : public cFCDialog_AddFile
{
	public:
		         cFCDialog_AddFile_Any( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_AddFile_Any();
};


// Class : File Chooser Dialog - Add File - Pdf
class
cFCDialog_AddFile_Pdf : public cFCDialog_AddFile
{
	public:
		         cFCDialog_AddFile_Pdf( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_AddFile_Pdf();
};


/*** File Chooser Dialog - Save As ********************************************/

// Class : File Chooser Dialog - Save As
class
cFCDialog_SaveAs : public Gtk::FileChooserDialog
{
	public:
		         cFCDialog_SaveAs( Gtk::Window& , const Glib::ustring& , const Glib::ustring& );
		virtual ~cFCDialog_SaveAs();
		
		void clear();
		int run();
		std::string	get_filename();
	
	protected:
	
		const std::string sExtension;
	
		void init();
		void addExtension( std::string& );

		Gtk::CheckButton	mCButton_AddExtension;
		Gtk::MessageDialog	mMDialog_OverwriteConfirm;
};


// Class : File Chooser Dialog - Save As - PDF
class
cFCDialog_SaveAs_Pdf : public cFCDialog_SaveAs
{
	public:
		         cFCDialog_SaveAs_Pdf( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_SaveAs_Pdf();
};


// Class : File Chooser Dialog - Save As - FDF
class
cFCDialog_SaveAs_Fdf : public cFCDialog_SaveAs
{
	public:
		         cFCDialog_SaveAs_Fdf( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_SaveAs_Fdf();
};


// Class : File Chooser Dialog - Save As - Dump
class
cFCDialog_SaveAs_Dump : public cFCDialog_SaveAs
{
	public:
		         cFCDialog_SaveAs_Dump( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_SaveAs_Dump();
};



/*** File Chooser Dialog - Select Folder **************************************/

// Class : File Chooser Dialog - Select Folder
class
cFCDialog_SelectFolder : public Gtk::FileChooserDialog
{
	public:
		         cFCDialog_SelectFolder( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCDialog_SelectFolder();

		void clear();
		int run();

	protected:
	
		void init();
};



/*** File Chooser Button ******************************************************/

// Class : FileChooserButton - PDF
class
cFCButton_Pdf : public Gtk::FileChooserButton
{
	public:
		         cFCButton_Pdf( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCButton_Pdf();

		void clear();
};


// Class : FileChooserButton - FDF
class
cFCButton_Fdf : public Gtk::FileChooserButton
{
	public:
		         cFCButton_Fdf( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCButton_Fdf();

		void clear();
};


// Class : FileChooserButton - Dump
class
cFCButton_Dump : public Gtk::FileChooserButton
{
	public:
		         cFCButton_Dump( Gtk::Window& , const Glib::ustring& );
		virtual ~cFCButton_Dump();

		void clear();
};


#endif

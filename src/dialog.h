/*
 * dialog.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_DIALOG_H__
#define __PDFCHAIN_DIALOG_H__

#include "pdfchain.h"


/*** About Dialog  ************************************************************/

class
cDialog_About : public Gtk::AboutDialog
{
	public:
		         cDialog_About( Gtk::Window& );
		virtual ~cDialog_About();
};



/*** Message Dialog : PDFTK Error *********************************************/

class
cMDialog_PdftkError : public Gtk::MessageDialog
{
	public:
		         cMDialog_PdftkError( Gtk::Window& );
		virtual ~cMDialog_PdftkError();

		void popupMessage( const Glib::ustring& , int );
		
	protected:
};

#endif

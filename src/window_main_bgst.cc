/*
 * window_main_bgst.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_bgst.h"


/*** Section : Background / Stamp *********************************************/

// Constructor
cSection_BgSt::cSection_BgSt( Gtk::Window& ref_window )
:
	Gtk::VBox( false , Pdfchain::SPACING ) ,	// homogeneous , spacing

	mHBox_SourceFile( false , Pdfchain::SPACING ) ,	// homogeneous , spacing
	mHBox_BgSt(       false , Pdfchain::SPACING ) ,
	mTable_BgSt( 2 , 2 , false ) ,	// y_rows , x_columns , homogenous

	mFCButton_SourceFile( ref_window , _("Background / Stamp - Select source PDF file ...") ) ,				// parent_window , title
	mFCButton_BgStFile(   ref_window , _("Background / Stamp - Select background or stamp PDF file ...") ) ,
	mFCDialog_SaveAs(     ref_window , _("Background / Stamp - Save output PDF file as ...") ) ,

	mLabel_SourceFile( _("Document:")    , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ) ,	// label , xalign , yalign , mnemonic
	mLabel_BgStFile(   _("Layer (PDF):") , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ) ,

	mRBGroup_BgSt( mRButton_Background.get_group() ) ,
	mRButton_Background(	mRBGroup_BgSt , _("Background")	, false ) ,	// group , label , mnemonic
	mRButton_Stamp(			mRBGroup_BgSt , _("Stamp")		, false ) ,

	mCButton_Multiple( "Use multiple pages" , false )	// label , mnemonic
{
	mLabel_SourceFile.set_tooltip_text(
		_("The source PDF file. This document gets the background or stamp imprinted.") );
	mLabel_BgStFile.set_tooltip_text(
		_("The layer PDF file. This document contains the background or stamp layer(s).") );
	mRButton_Background.set_tooltip_text(
		_("Select to print a background layer behind each page of the document.") );
	mRButton_Stamp.set_tooltip_text(
		_("Select to print a stamp layer on top of each page of the document.") );
	mCButton_Multiple.set_tooltip_text(
		_("If active, all pages of the layer PDF will be used "
		  "for printing the background or stamp.\n"
		  "If not active, only the first page of the layer PDF will be used.") );

	mFCButton_SourceFile.set_halign( Gtk::ALIGN_START );
	mFCButton_BgStFile.set_halign(   Gtk::ALIGN_START );
	mCButton_Multiple.set_halign(    Gtk::ALIGN_START );

	mHBox_SourceFile.pack_start( mLabel_SourceFile    , false , false , 0 );	// widget , expand , fill , padding
	mHBox_SourceFile.pack_start( mFCButton_SourceFile , false , true  , 0 );

	mHBox_BgSt.pack_start( mRButton_Background , false , false , 0 );
	mHBox_BgSt.pack_start( mRButton_Stamp      , false , false , 0 );

	mTable_BgSt.set_border_width( Pdfchain::BORDER );
	mTable_BgSt.set_spacings( Pdfchain::SPACING );
	mTable_BgSt.attach( mLabel_BgStFile    , 0 , 1 , 0 , 1 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_BgSt.attach( mFCButton_BgStFile , 1 , 2 , 0 , 1 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_BgSt.attach( mCButton_Multiple  , 1 , 2 , 1 , 2 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );

	mFrame_BgSt.set_label_widget( mHBox_BgSt );
	mFrame_BgSt.add( mTable_BgSt );

	pack_start( mHBox_SourceFile , false , false , 0 );
	pack_start( mFrame_BgSt      , false , false , 0 );

	// Connect Signal Handler
	mFCButton_BgStFile.signal_file_set().connect( sigc::mem_fun(
		*this , &cSection_BgSt::onFCButton_BgStFile_file_set ) );

	// Init Object
	init();
}


// Destructor
cSection_BgSt::~cSection_BgSt()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_BgSt()::~cSection_BgSt()";	//TEST
#endif
}


// Method (public) : clear
void
cSection_BgSt::clear()
{
	init();
	return;
}


// Method (public) : create command
std::string
cSection_BgSt::createCommand()
{
	std::string
		str_command ,
		str_sourcefile ,
		str_bgstfile ,
		str_targetfile;

	if ( "" != ( str_sourcefile = mFCButton_SourceFile.get_filename() ) ) {
		if ( "" != ( str_bgstfile = mFCButton_BgStFile.get_filename() ) ) {

			switch ( mFCDialog_SaveAs.run() ) {

				case Gtk::RESPONSE_OK:
					mFCDialog_SaveAs.hide();

					if ( "" != ( str_targetfile = mFCDialog_SaveAs.get_filename() ) ) {
						str_command = " " + Pdfchain::quote_path( str_sourcefile );

						if ( true == mRButton_Background.get_active() ) {
							if ( true == mCButton_Multiple.get_active() ) {
								str_command += " " + Pdfchain::Cmd::BACKGROUND_MULTI;
							} else {
								str_command += " " + Pdfchain::Cmd::BACKGROUND;
							}
						} else {
							if ( true == mCButton_Multiple.get_active() ) {
								str_command += " " + Pdfchain::Cmd::STAMP_MULTI;
							} else {
								str_command += " " + Pdfchain::Cmd::STAMP;
							}
						}

						str_command += " " + Pdfchain::quote_path( str_bgstfile );
						str_command += " " + Pdfchain::Cmd::OUTPUT;
						str_command += " " + Pdfchain::quote_path( str_targetfile );
					}
					break;
					
				default:
					mFCDialog_SaveAs.hide();
					break;
			}
		}
	}

	return str_command;
}


// Method (protected) : init
void
cSection_BgSt::init()
{
	mFCButton_SourceFile.clear();
	mFCButton_BgStFile.clear();
	mFCDialog_SaveAs.clear();

	mRButton_Background.set_active( true );
	mCButton_Multiple.set_active( true );

	return;
}


// Signal Handler (protected) : on FileChooserButton BgStFile file set
void 
cSection_BgSt::onFCButton_BgStFile_file_set()
{
	if ( 1 < Pdfchain::count_page_numbers( mFCButton_BgStFile.get_filename() ) )
		mCButton_Multiple.set_active( true );
	else
		mCButton_Multiple.set_active( false );
													
	return;
}

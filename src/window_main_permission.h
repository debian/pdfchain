/*
 * window_main_permission.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_PERMISSION_H__
#define __PDFCHAIN_WINDOW_MAIN_PERMISSION_H__

#include "pdfchain.h"


/*** Section : Permission *****************************************************/

class
cSection_Permission : public Gtk::HBox
{
	public:

		         cSection_Permission();
		virtual ~cSection_Permission();

		void clear();
		Glib::ustring createCommand();

	protected:

		Gtk::VBox
			mVBox_Passwords,
			mVBox_Encryption;

		Gtk::Table
			mTable_Allow;

		Gtk::Frame
			mFrame_PasswordUser,
			mFrame_PasswordOwner,
			mFrame_Encryption,
			mFrame_Allow;

		Gtk::Label
			mLabel_PasswordUser,
			mLabel_PasswordOwner,
			mLabel_Encryption,
			mLabel_Allow;

		Gtk::CheckButton
			mCButton_Printing,
			mCButton_DegradedPrinting,
			mCButton_ModifyContents,
			mCButton_Assembly,
			mCButton_CopyContents,
			mCButton_Screenreaders,
			mCButton_ModifyAnnotations,
			mCButton_FillIn;

		Gtk::RadioButton
			mRButton_EncryptNone,
			mRButton_Encrypt40Bit,
			mRButton_Encrypt128Bit;

		Gtk::Entry
			mEntry_PasswordUser,
			mEntry_PasswordOwner;

		Gtk::RadioButtonGroup
			mRBGroup_Encrypt;

		Gtk::Alignment
			mAlign_PasswordUser,
			mAlign_PasswordOwner,
			mAlign_Encryption,
			mAlign_Allow;

		void init();

		// Signal Handler
		void onEntryIcon_PasswordOwner_pressed( Gtk::EntryIconPosition icon_pos , const GdkEventButton* event ) {
			mEntry_PasswordOwner.set_text("");
			return;
		}

		void onEntryIcon_PasswordUser_pressed( Gtk::EntryIconPosition icon_pos , const GdkEventButton* event ) {
			mEntry_PasswordUser.set_text("");
			return;
		}

		void onEntry_Password_changed() {
			if ( mEntry_PasswordUser.get_text_length() || mEntry_PasswordOwner.get_text_length() ) {

				if ( mRButton_EncryptNone.get_sensitive() ) {
					mRButton_EncryptNone.set_sensitive( false );
			
					if ( mRButton_EncryptNone.get_active() )
						mRButton_Encrypt128Bit.set_active();
				}
			}
			else {
				mRButton_EncryptNone.set_sensitive( true );
			}

			return;
		}

		void onRButton_EncryptNone_toggled() {
			if ( mRButton_EncryptNone.get_active() )
				mFrame_Allow.set_sensitive( false );
			else
				mFrame_Allow.set_sensitive( true );
			return;
		}

		void onCButton_CopyContents_toggled() {
			if ( mCButton_CopyContents.get_active() ) {
				mCButton_Screenreaders.set_sensitive( false );
				mCButton_Screenreaders.set_active( true );
			}
			else
				mCButton_Screenreaders.set_sensitive( true );
			return;
		}

		void onCButton_ModifyContents_toggled() {
			if ( mCButton_ModifyContents.get_active() ) {
				mCButton_Assembly.set_sensitive( false );
				mCButton_Assembly.set_active( true );
			}
			else
				mCButton_Assembly.set_sensitive( true );
			return;
		}

		void onCButton_ModifyAnnotations_toggled() {
			if ( mCButton_ModifyAnnotations.get_active() ) {
				mCButton_FillIn.set_sensitive( false );
				mCButton_FillIn.set_active( true );
				}
			else
				mCButton_FillIn.set_sensitive( true );
			return;
		}
};


#endif

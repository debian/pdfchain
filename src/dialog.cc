/*
 * dialog.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "dialog.h"


/*** About Dialog *************************************************************/

// Constructor
cDialog_About::cDialog_About( Gtk::Window& ref_window )
{
	Glib::RefPtr<Gio::File>	ref_icon_path_A =
		Gio::File::create_for_path( Pdfchain::APPICON_PATH_A );
		
	Glib::RefPtr<Gio::File>	ref_icon_path_B =
		Gio::File::create_for_path( Pdfchain::APPICON_PATH_B );

	if ( ref_icon_path_A->query_exists() )
		set_logo( Gdk::Pixbuf::create_from_file( Pdfchain::APPICON_PATH_A ) );
	else if	( ref_icon_path_B->query_exists() )
		set_logo( Gdk::Pixbuf::create_from_file( Pdfchain::APPICON_PATH_B ) );
	else
		std::cerr << std::endl << "Icon file doesn't exist! " <<
			Pdfchain::APPICON_PATH_A << " , " << Pdfchain::APPICON_PATH_B;

	std::vector<Glib::ustring>	vect_authors;
	vect_authors.push_back( "Martin Singer <m_power3@users.sourceforge.net>" );
	
	set_transient_for( ref_window );
	set_default_response( Gtk::RESPONSE_CLOSE );
	
	set_program_name( Pdfchain::About::PROGRAM_NAME );
	set_version( Pdfchain::About::PROGRAM_VERSION );
	set_comments( _("A graphical user interface for the PDF Toolkit") );
	set_authors( vect_authors );
	//set_documenters( vet_documenters );
	//set_artists( vet_artists );
	//set_translator_credits( str_translators );
	set_copyright( "Copyright " + Pdfchain::String::COPYRIGHT + " 2009-2015 Martin Singer" );
	set_license_type( Gtk::LICENSE_GPL_3_0 );
	set_website( Pdfchain::About::PROJECT_URL );
	set_website_label( Pdfchain::About::PROJECT_URL );
}


// Destructor
cDialog_About::~cDialog_About()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cDialog_About::~cDialog_About()";	//TEST
#endif
}



/*** Message Dialog : PDFTK Error *********************************************/

// Constructor
cMDialog_PdftkError::cMDialog_PdftkError( Gtk::Window& ref_window )
:
	Gtk::MessageDialog( ref_window , _("The PDFtk returned") , false ,  Gtk::MESSAGE_ERROR , Gtk::BUTTONS_CLOSE , false )
{
}


// Destructor
cMDialog_PdftkError::~cMDialog_PdftkError()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cMDialog_PdftkError::~cMDialog_PdftkError()";	//TEST
#endif
}


// Methode (public) : popup message
void
cMDialog_PdftkError::popupMessage( const Glib::ustring& str_error_text , int error_code )
{
//	std::cerr << std::endl << str_error_text << " - PDFTK returned: " << error_code << std::endl;

	Glib::ustring str_secondary_text = str_error_text + + "\n" + _("Error code:") + " "
		+ static_cast<Glib::ustring>( Pdfchain::convert_guint_to_string( static_cast<guint>( error_code ) ) );
	
	set_secondary_text( str_secondary_text , false );

	run();
	hide();
	return;
}

/*
 * window_main_cat.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_cat.h"


/*** Combobox : Output ID *****************************************************/

// Constructor
cCBox_OutputId::cCBox_OutputId()
{
	rLStore    = cLStore_OutputId::create();
	pTMCRecord = rLStore->getTMCRecord();
	
	pack_start( mCRText );
	add_attribute( mCRText.property_text() , pTMCRecord->mTMColumn_Label );	// necessarily after 'pack_start()' !!!
	set_model( rLStore );
	
	clear();
}


// Destructor
cCBox_OutputId::~cCBox_OutputId()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cCBox_OutputId::~cCBox_OutputId()";	//TEST
#endif
}

// Method (public) : clear()
void
cCBox_OutputId::clear()
{
	set_active( rLStore->select( Pdfchain::Id::ID_NEW ) );
	set_sensitive( false );
	unset_active();
	vOutputId = 0;
	return;
}



// Signal Handler (protected) : on FileNumber changed (at ListStore Cat)
void
cCBox_OutputId::onFileNumber_changed( guint file_number )
{
	Gtk::TreeModel::iterator iter = get_active();
	
	if ( 1 > file_number ) {
//		if ( get_sensitive() ) {
//			if ( iter )	vOutputId = (*iter)[pTMCRecord->mTMColumn_ID];
//		}
		if ( get_sensitive() && iter )	
			vOutputId = (*iter)[pTMCRecord->mTMColumn_ID];
		unset_active();
		set_sensitive( false );
	}
	else if ( 1 == file_number ) {
		if ( get_sensitive() && iter )
			vOutputId = (*iter)[pTMCRecord->mTMColumn_ID];
		set_sensitive( true );
		set_active( rLStore->select( Pdfchain::Id::ID_KEEP ) );
		set_sensitive( false );
	}
	else if ( 1 < file_number ) {
		if ( ! get_sensitive() ) {
//			set_active( rLStore->select( Pdfchain::Id::ID_NEW ) );
			set_active( rLStore->select( vOutputId ) );
			set_sensitive( true );
		}
	}

	return;
}



/*** Popup Menu : Cat *********************************************************/

// Constructor
cPMenu_Cat::cPMenu_Cat( cTView_Cat* pt_tview_cat )
:
	mMItem_PagesAll(     _("All") , false ) ,	// label, mnemonic
	mMItem_PagesEven(    _("Even") , false ) ,
	mMItem_PagesOdd(     _("Odd") , false ) ,
	mMItem_RotNorth(     _("0°") , false ), 
	mMItem_RotEast(      _("90°") , false ) ,
	mMItem_RotSouth(     _("180°") , false ) ,
	mMItem_RotWest(      _("270°") , false ) ,
	mMItem_SelectAll(    _("Select all entries") , false ) ,
	mMItem_SelectNone(   _("Unselect all entries") , false ) ,
	mMItem_SelectInvert( _("Invert selection") , false )
{
	Glib::RefPtr<Gtk::SizeGroup>
		ref_sizegroup_rot = Gtk::SizeGroup::create( Gtk::SIZE_GROUP_HORIZONTAL );

	ref_sizegroup_rot->add_widget( mMItem_RotNorth );
	ref_sizegroup_rot->add_widget( mMItem_RotEast );
	ref_sizegroup_rot->add_widget( mMItem_RotSouth );
	ref_sizegroup_rot->add_widget( mMItem_RotWest );

	attach( mMItem_PagesAll     , 0 , 1 , 0 , 1 );	// widget, left, right, top, bottom
	attach( mMItem_PagesEven    , 1 , 2 , 0 , 1 );
	attach( mMItem_PagesOdd     , 2 , 3 , 0 , 1 );
	attach( mMItem_RotNorth     , 0 , 1 , 1 , 2 );
	attach( mMItem_RotEast      , 1 , 2 , 1 , 2 );
	attach( mMItem_RotSouth     , 2 , 3 , 1 , 2 );
	attach( mMItem_RotWest      , 3 , 4 , 1 , 2 );
	attach( mMISeparator        , 0 , 4 , 2 , 3 );
	attach( mMItem_SelectAll    , 0 , 4 , 3 , 4 );
	attach( mMItem_SelectNone   , 0 , 4 , 4 , 5 );
	attach( mMItem_SelectInvert , 0 , 4 , 5 , 6 );

	// Singal Hanlder
	mMItem_PagesAll.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_EvenOdd_All_activate ) );

	mMItem_PagesEven.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_EvenOdd_Even_activate ) );

	mMItem_PagesOdd.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_EvenOdd_Odd_activate ) );

	mMItem_RotNorth.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_Rotation_North_activate ) );

	mMItem_RotEast.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_Rotation_East_activate ) );

	mMItem_RotSouth.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_Rotation_South_activate ) );

	mMItem_RotWest.signal_activate().connect(
		sigc::mem_fun( *this , &cPMenu_Cat::onMItem_Rotation_West_activate ) );
}


// Destructor
cPMenu_Cat::~cPMenu_Cat()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cPMenu_Cat::~cPMenu_Cat()";	//TEST
#endif
}



/*** Tree View : Cat **********************************************************/

// Constructor
cTView_Cat::cTView_Cat( Gtk::Window& ref_window )
:
	mPMenu_Cat( this ),

	pTMCRecord_Cat( NULL ),

#ifdef PDFCHAIN_TEMP
	mTVColumn_Id(         "Id" ),			//TEMP
	mTVColumn_SourcePath( "Source path" ),	//TEMP
	mTVColumn_Tooltip(    "Tooltip" ),		//TEMP
	mTVColumn_Commands(   "Commands" ),		//TEMP
//	mTVColumn_Colors(     "Colors" ),		//TEMP
#endif

	mLabel_Add(         _("Add")            , false ),	// label , mnemonic
	mLabel_Pages(       _("Page selection") , false ),
	mLabel_EvenOdd(     _("Page filter")    , false ),
	mLabel_Rotation(    _("Rotation")       , false ),
	mLabel_SourceFile(  _("Document")       , false ),
	mLabel_Password(    _("Password")       , false ),
	mLabel_PageNumbers( _("PN")             , false )
{
	mPMenu_Cat.show_all();
	mPMenu_Cat.accelerate( *this );

	rLStore_Cat      = cLStore_Cat::create( ref_window );
	rLStore_EvenOdd  = cLStore_EvenOdd::create();
	rLStore_Rotation = cLStore_Rotation::create();

	pTMCRecord_Cat = rLStore_Cat->getTMCRecord();

	get_selection()->set_mode( Gtk::SELECTION_MULTIPLE );
	
	set_model( rLStore_Cat );
	set_reorderable( true );
	set_rules_hint( true );
//	set_rubber_banding( true );
	set_rubber_banding( false );
	set_enable_search( true );
	set_search_column( pTMCRecord_Cat->mTMColumn_SourceFile );
	set_tooltip_column( get_search_column() + 2 );
	
	mLabel_Add.set_tooltip_text(         _("Add entry for processing") );
	mLabel_Pages.set_tooltip_text(       _("Select and filter pages"
		"\nE.g.: '1-3 5 7-end' or '1 r5 r3-end' or 'end-3 3 3 3-end'"
		"\n'r' means 'reverse' (e.g. 'r2' is the second last page).") );
	mLabel_EvenOdd.set_tooltip_text( _("Filters pages with an AND logic to the page selection."
		"\nThe 'even' and 'odd' filters only affect to page ranges (not to single selected pages)!") );
/*
	mLabel_Pages.set_tooltip_text(       _("Select and filter pages\n"
		"Single pages and page ranges are separated by the space sign (e.g.: '3 5 4-6'). "
		"Page ranges are connected by the hyphen sign '-' and are able to invert the page order (e.g: '5-1'). "
		"Use the keyword 'end' for the last page and e.g. 'r2' (r stands for 'reverse') for the second last page of a document.\n"
		"The filters 'even' and 'odd' affect only to page ranges (not to single pages).\n"
		"For example: \"2 4-6 10-7 11 r5-12 r4 end-r3\"") );
*/		
	mLabel_Rotation.set_tooltip_text(    _("Rotate selected pages") );
	mLabel_SourceFile.set_tooltip_text(  _("The source PDF file names ") );
	mLabel_SourcePath.set_tooltip_text(  _("The source PDF file paths") );
	mLabel_Password.set_tooltip_text(    _("Input password") );
	mLabel_PageNumbers.set_tooltip_text( _("Number of pages") );

	mTVColumn_Add.set_widget(         mLabel_Add );
	mTVColumn_Pages.set_widget(       mLabel_Pages );
	mTVColumn_EvenOdd.set_widget(     mLabel_EvenOdd );
	mTVColumn_Rotation.set_widget(    mLabel_Rotation );
	mTVColumn_SourceFile.set_widget(  mLabel_SourceFile );
	mTVColumn_SourcePath.set_widget(  mLabel_SourcePath );
	mTVColumn_Password.set_widget(    mLabel_Password );
	mTVColumn_PageNumbers.set_widget( mLabel_PageNumbers );

	mLabel_Add.show();
	mLabel_Pages.show();
	mLabel_EvenOdd.show();
	mLabel_Rotation.show();
	mLabel_SourceFile.show();
	mLabel_SourcePath.show();
	mLabel_Password.show();
	mLabel_PageNumbers.show();

	mTVColumn_Add.set_resizable( false );
	mTVColumn_Pages.set_expand( true );
	mTVColumn_Pages.set_resizable( true );
	mTVColumn_EvenOdd.set_resizable( false );
	mTVColumn_Rotation.set_resizable( false );
	mTVColumn_SourceFile.set_expand( true );
	mTVColumn_SourceFile.set_resizable( true );
	mTVColumn_Password.set_resizable( true );
	
#ifdef PDFCHAIN_TEMP
	mTVColumn_Id.set_expand( true );			//TEMP
	mTVColumn_SourcePath.set_expand( true );	//TEMP
	mTVColumn_SourcePath.set_resizable( true );	//TEMP
	mTVColumn_Tooltip.set_expand( true );		//TEMP
	mTVColumn_Tooltip.set_resizable( true );	//TEMP
#endif

	mCRToggle_Add.property_activatable()     = true;
	mCRToggle_Add.property_xalign()          = 0.0;
	mCRText_Pages.property_editable()        = true;
	mCRText_Pages.property_xalign()          = 0.0;
	mCRCombo_EvenOdd.property_model()        = rLStore_EvenOdd;	// the liststore
	mCRCombo_EvenOdd.property_text_column()  = 1;				// text column at the liststore
	mCRCombo_EvenOdd.property_editable()     = true;
	mCRCombo_EvenOdd.property_has_entry()    = false;			// no manual entry possible
	mCRCombo_EvenOdd.property_xalign()       = 1.0;				// align: right side
	mCRCombo_Rotation.property_model()       = rLStore_Rotation;
	mCRCombo_Rotation.property_text_column() = 1;
	mCRCombo_Rotation.property_editable()    = true;
	mCRCombo_Rotation.property_has_entry()   = false;
	mCRCombo_Rotation.property_xalign()      = 1.0;
	mCRText_Password.property_editable()     = true;
	mCRText_PageNumbers.property_xalign()    = 1.0;

	mTVColumn_Add.pack_start(         mCRToggle_Add               , false );	// cell renderer , expand
	mTVColumn_Pages.pack_start(       mCRText_Pages	              , true  );
	mTVColumn_EvenOdd.pack_start(     mCRCombo_EvenOdd            , false );
	mTVColumn_Rotation.pack_start(    mCRCombo_Rotation	          , false );
	mTVColumn_SourceFile.pack_start(  mCRText_SourceFile          , true  );
	mTVColumn_Password.pack_start(    mCRText_Password            , true  );
	mTVColumn_PageNumbers.pack_start( mCRText_PageNumbers         , true  );

#ifdef PDFCHAIN_TEMP
	mTVColumn_Id.pack_start(          mCRText_Id                  , true  );	//TEMP
	mTVColumn_SourcePath.pack_start(  mCRText_SourcePath          , true  );	//TEMP
	mTVColumn_Tooltip.pack_start(     mCRText_Tooltip             , true  );	//TEMP
	mTVColumn_Commands.pack_start(    mCRText_Cmd_Handle          , false );	//TEMP
	mTVColumn_Commands.pack_start(    mCRText_Cmd_EvenOdd         , false );	//TEMP
	mTVColumn_Commands.pack_start(    mCRText_Cmd_Rotation        , false );	//TEMP
//	mTVColumn_Colors.pack_start(      mCRPixbuf_Color_Pages       , false );	//TEMP
//	mTVColumn_Colors.pack_start(      mCRPixbuf_Color_PageNumbers , false );	//TEMP
#endif
	
	mTVColumn_Add.add_attribute(
		mCRToggle_Add.property_active() ,
		pTMCRecord_Cat->mTMColumn_Add );
		
	mTVColumn_Pages.add_attribute(
		mCRText_Pages.property_text() ,
		pTMCRecord_Cat->mTMColumn_Pages );
	
	mTVColumn_Pages.add_attribute(
		mCRText_Pages.property_foreground_rgba() ,
		pTMCRecord_Cat->mTMColumn_Color_Pages );
	
	mTVColumn_EvenOdd.add_attribute(
		mCRCombo_EvenOdd.property_text() ,
		pTMCRecord_Cat->mTMColumn_EvenOdd );
	
	mTVColumn_Rotation.add_attribute(
		mCRCombo_Rotation.property_text() ,
		pTMCRecord_Cat->mTMColumn_Rotation );
	
	mTVColumn_SourceFile.add_attribute(
		mCRText_SourceFile.property_text() ,
		pTMCRecord_Cat->mTMColumn_SourceFile );
	
	mTVColumn_Password.add_attribute(
		mCRText_Password.property_text() ,
		pTMCRecord_Cat->mTMColumn_Password );
	
	mTVColumn_PageNumbers.add_attribute(
		mCRText_PageNumbers.property_text() ,
		pTMCRecord_Cat->mTMColumn_PageNumbers );
	
	mTVColumn_PageNumbers.add_attribute(
		mCRText_PageNumbers.property_foreground_rgba() ,
		pTMCRecord_Cat->mTMColumn_Color_PageNumbers );
	
#ifdef PDFCHAIN_TEMP
	mTVColumn_Id.add_attribute(
		mCRText_Id.property_text() ,
		pTMCRecord_Cat->mTMColumn_Id );									//TEMP

	mTVColumn_SourcePath.add_attribute(
		mCRText_SourcePath.property_text() ,
		pTMCRecord_Cat->mTMColumn_SourcePath );							//TEMP
	
	mTVColumn_Tooltip.add_attribute(
		mCRText_Tooltip.property_text() ,
		pTMCRecord_Cat->mTMColumn_Tooltip );							//TEMP
	
	mTVColumn_Commands.add_attribute(
		mCRText_Cmd_EvenOdd.property_text() ,
		pTMCRecord_Cat->mTMColumn_Cmd_EvenOdd );						//TEMP
	
	mTVColumn_Commands.add_attribute(
		mCRText_Cmd_Rotation.property_text() ,
		pTMCRecord_Cat->mTMColumn_Cmd_Rotation );						//TEMP
	
//	mTVColumn_Colors.add_attribute(
//		mCRPixbuf_Color_Pages.property_cell_background_gdk() ,
//		pTMCRecord_Cat->mTMColumn_Color_Pages );						//TEMP
	
//	mTVColumn_Colors.add_attribute(
//		mCRPixbuf_Color_PageNumbers.property_cell_background_gdk() ,
//		pTMCRecord_Cat->mTMColumn_Color_PageNumbers );					//TEMP
#endif
	
	append_column( mTVColumn_Add         );
	append_column( mTVColumn_Pages       );
	append_column( mTVColumn_EvenOdd     );
	append_column( mTVColumn_Rotation    );
	append_column( mTVColumn_SourceFile  );
	append_column( mTVColumn_Password    );
	append_column( mTVColumn_PageNumbers );
	
#ifdef PDFCHAIN_TEMP
	append_column( mTVColumn_Id          );	//TEMP
	append_column( mTVColumn_SourcePath  );	//TEMP
	append_column( mTVColumn_Tooltip     );	//TEMP
	append_column( mTVColumn_Commands    );	//TEMP
//	append_column( mTVColumn_Colors      );	//TEMP
#endif

	// Singal Hanlder - Toolbar
	mCRToggle_Add.signal_toggled().connect( sigc::mem_fun(
		*this , &cTView_Cat::onCRToggle_Add_toggled ) );
		
	mCRText_Pages.signal_edited().connect( sigc::mem_fun(
		*this , &cTView_Cat::onCRText_Pages_edited ) );
		
	mCRCombo_EvenOdd.signal_changed().connect( sigc::mem_fun(
		*this , &cTView_Cat::onCRCombo_EvenOdd_changed ) );
		
	mCRCombo_Rotation.signal_changed().connect( sigc::mem_fun(
		*this , &cTView_Cat::onCRCombo_Rotation_changed ) );
		
	mCRText_Password.signal_edited().connect( sigc::mem_fun(
		*this , &cTView_Cat::onCRText_Password_edited ) );

	// Singal Hanler - Popup Menu
	mPMenu_Cat.signalEvenOdd_activate().connect( sigc::mem_fun(
		*this , &cTView_Cat::onMItems_EvenOdd_activated ) );
	
	mPMenu_Cat.signalRotation_activate().connect( sigc::mem_fun(
		*this , &cTView_Cat::onMItems_Rotation_activated ) );

	mPMenu_Cat.mMItem_SelectAll.signal_activate().connect(
		sigc::mem_fun( *this , &cTView_Cat:: onMItem_SelectAll_activated ) );

	mPMenu_Cat.mMItem_SelectNone.signal_activate().connect(
		sigc::mem_fun( *this , &cTView_Cat:: onMItem_SelectNone_activated ) );

	mPMenu_Cat.mMItem_SelectInvert.signal_activate().connect(
		sigc::mem_fun( *this , &cTView_Cat:: onMItem_SelectInvert_activated ) );
}


// Destructor
cTView_Cat::~cTView_Cat()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cTView_Cat::~cTView_Cat()";	//TEST
#endif
}


// Method (public) : clear
void
cTView_Cat::clear()
{
	rLStore_Cat->clear();
	return;
}


// Signal Handler (protected, overwrite) : on button press event
bool
cTView_Cat::on_button_press_event( GdkEventButton* event )
{
	// Right Click opens Popup Menu
	if ( ( GDK_BUTTON_PRESS == event->type ) && ( 3 == event->button ) ) {
		mPMenu_Cat.popup( event->button , event->time );
		return true;
	}

	return Gtk::TreeView::on_button_press_event( event );
}


// Signal Handler (protected) : on a evenodd menu item is activatetd
void 
cTView_Cat::onMItems_EvenOdd_activated(
	Pdfchain::Id::EvenOdd enum_pages )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = get_selection()->get_selected_rows();

	if ( list_selections.size() ) {

		cTMCRecord_Selection* pt_record_evenodd = rLStore_EvenOdd->getTMCRecord();
		Gtk::TreeModel::iterator iter_store_evenodd = rLStore_EvenOdd->children().begin();

		// Find the right row at evenodd store by using Pdfchain::Id::EvenOdd enum
		for ( iter_store_evenodd = rLStore_EvenOdd->children().begin() ;
			rLStore_EvenOdd->children().end() != iter_store_evenodd ;
			iter_store_evenodd++ )
		{
			if ( enum_pages == iter_store_evenodd->get_value( pt_record_evenodd->mTMColumn_ID ) )
				break;
		}

		// copy the label and the command from evenodd store to all selected rows at cat store
		for ( std::vector<Gtk::TreeModel::Path>::iterator iter_store_cat = list_selections.begin() ; 
			list_selections.end() != iter_store_cat ; 
			iter_store_cat++ )
		{
			rLStore_Cat->changeEvenOdd( *iter_store_cat ,
				iter_store_evenodd->get_value( pt_record_evenodd->mTMColumn_Label ) ,
				iter_store_evenodd->get_value( pt_record_evenodd->mTMColumn_Command ) );
		}
	}	

	return;
}


// Signal Handler (protected) : on a rotation menu item is activatetd
void
cTView_Cat::onMItems_Rotation_activated(
	Pdfchain::Id::Rotation enum_rotation )
{
	std::vector<Gtk::TreeModel::Path>
		list_selections = get_selection()->get_selected_rows();

	if ( list_selections.size() ) {

		cTMCRecord_Selection* pt_record_rotation = rLStore_Rotation->getTMCRecord();
		Gtk::TreeModel::iterator iter_store_rotation = rLStore_Rotation->children().begin();

		// Find the right row at rotation store by using Pdfchain::Id::Rotation enum
		for ( iter_store_rotation = rLStore_Rotation->children().begin() ;
			rLStore_Rotation->children().end() != iter_store_rotation ;
			iter_store_rotation++ )
		{
			if ( enum_rotation == iter_store_rotation->get_value( pt_record_rotation->mTMColumn_ID ) )
				break;
		}

		// copy the label and the command from rotation store to all selected rows at cat store
		for ( std::vector<Gtk::TreeModel::Path>::iterator iter_store_cat = list_selections.begin() ; 
			list_selections.end() != iter_store_cat ; 
			iter_store_cat++ )
		{
			rLStore_Cat->changeRotation( *iter_store_cat ,
				iter_store_rotation->get_value( pt_record_rotation->mTMColumn_Label ) ,
				iter_store_rotation->get_value( pt_record_rotation->mTMColumn_Command ) );
		}
	}	

	return;
}


// Signal Handler (protectd) : on menu item select all rows is activated
void
cTView_Cat::onMItem_SelectAll_activated()
{
	Glib::RefPtr<Gtk::TreeView::Selection>
		ref_tree_selection = get_selection();

	ref_tree_selection->select_all();

	return;
}


// Signal Handler (protectd) : on menu item unselect all rows is activated
void
cTView_Cat::onMItem_SelectNone_activated()
{
	Glib::RefPtr<Gtk::TreeView::Selection>
		ref_tree_selection = get_selection();

	ref_tree_selection->unselect_all();

	return;
}


// Signal Handler (protectd) : on menu item invert selection of rows is activated
void
cTView_Cat::onMItem_SelectInvert_activated()
{
	Glib::RefPtr<Gtk::TreeView::Selection>
		ref_tree_selection = get_selection();

	Gtk::TreeModel::iterator
		iter_store_entry = rLStore_Cat->children().begin();

	while( rLStore_Cat->children().end() != iter_store_entry ) {

		if ( ref_tree_selection->is_selected( iter_store_entry) )
			ref_tree_selection->unselect( iter_store_entry );
		else
			ref_tree_selection->select( iter_store_entry );

		++iter_store_entry;
	}
	
	return;
}



/*** Toolbar : Cat ************************************************************/

// Constructor
cToolbar_Cat::cToolbar_Cat( cTView_Cat* pt_tview_cat )
:
	mToolButton_Add(    Gtk::Stock::ADD ),
	mToolButton_Remove( Gtk::Stock::REMOVE ),
	mToolButton_Copy(   Gtk::Stock::COPY ),
	mToolButton_Up(     Gtk::Stock::GO_UP ),
	mToolButton_Down(   Gtk::Stock::GO_DOWN ),
	mToolButton_Top(    Gtk::Stock::GOTO_TOP ),
	mToolButton_Bottom( Gtk::Stock::GOTO_BOTTOM ),
	mTToolButton_Shuffle( _("Shuffle") )	// label
//	mTToolButton_Shuffle( Gtk::Stock::DND_MULTIPLE )	//TEST maybe a icon for this button
{
	mSeparator_B.set_draw( false );
	mSeparator_B.set_expand( true );
	mSeparator_C.set_draw( false );

	mTToolButton_Shuffle.set_is_important( true );

	mToolButton_Add.set_tooltip_text(      _("Add some documents to catenate") );
	mToolButton_Remove.set_tooltip_text(   _("Remove selected entries") );
	mToolButton_Copy.set_tooltip_text(     _("Duplicate selected entries at list") );
	mToolButton_Up.set_tooltip_text(       _("Move selected entries up") );
	mToolButton_Down.set_tooltip_text(     _("Move selected entries down") );
	mToolButton_Top.set_tooltip_text(      _("Move selected entries to top of the list") );
	mToolButton_Bottom.set_tooltip_text(   _("Move selected entries to bottom of the list") );
	mTToolButton_Shuffle.set_tooltip_text( _("Interleaves pages from input ranges" ) );
/*
	mTToolButton_Shuffle.set_tooltip_text( _("Interleaves pages from input ranges"
		"\nE.g.: Doc1-Page1, D2-P1, D1-P2, D2-P2, D1-P3, D2-P3, ..." ) );
*/

	mToolItem_OutputId.add( mCBox_OutputId );
	
	add( mToolButton_Add      );
	add( mToolButton_Remove   );
	add( mToolButton_Copy     );
	add( mSeparator_A         );
	add( mToolButton_Top );
	add( mToolButton_Up       );
	add( mToolButton_Down     );
	add( mToolButton_Bottom );
	add( mSeparator_B         );
	add( mTToolButton_Shuffle );
	add( mSeparator_C         );
	add( mToolItem_OutputId   );
	
	mToolButton_Add.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Add_clicked ) );
		
	mToolButton_Remove.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Remove_clicked ) );
		
	mToolButton_Copy.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Copy_clicked ) );
		
	mToolButton_Up.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Up_clicked ) );
		
	mToolButton_Down.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Down_clicked ) );

	mToolButton_Top.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Top_clicked ) );

	mToolButton_Bottom.signal_clicked().connect( sigc::mem_fun(
		*pt_tview_cat , &cTView_Cat::onToolButton_Bottom_clicked ) );
}


// Destructor
cToolbar_Cat::~cToolbar_Cat()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cToolbar_Cat::~cToolbar_Cat()";	//TEST
#endif
}


// Method (public) : clear()
void
cToolbar_Cat::clear()
{
	mTToolButton_Shuffle.set_active( false );
	mCBox_OutputId.clear();
	return;
}



/*** Section : Cat  ***************************************************/

// Constructor
cSection_Cat::cSection_Cat( Gtk::Window& ref_window )
:
mTView( ref_window ),
mToolbar( &mTView ),
mFCDialog_SaveAs( ref_window , _("Catenate - Save output PDF file as ...") )
{
	mSWindow.add( mTView );
	
	pack_start( mSWindow , true  , true  , 0 );	// widget , expand , fill , padding
	pack_start( mToolbar , false , false , 0 );

	mTView.rLStore_Cat->signalFileNumber_changed().connect( sigc::mem_fun(
		mToolbar.mCBox_OutputId , &cCBox_OutputId::onFileNumber_changed ) );
}


// Destructor
cSection_Cat::~cSection_Cat()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_Cat::~cSection_Cat()";	//TEST
#endif
}


// Method (public) : clear
void
cSection_Cat::clear()
{
	mTView.clear();
	mToolbar.clear();
	mFCDialog_SaveAs.clear();
	return;
}


// Method (public) : create command
std::string
cSection_Cat::createCommand()
{
	std::string str_targetfile = "";
	std::string str_command    = "";

	if ( "" != ( str_command = mTView.rLStore_Cat->createCommand(
		mToolbar.mTToolButton_Shuffle.get_active() ) ) )
	{
		switch ( mFCDialog_SaveAs.run() ) {
			case Gtk::RESPONSE_OK: {
				mFCDialog_SaveAs.hide();

				if ( "" != ( str_targetfile = mFCDialog_SaveAs.get_filename() ) ) {

					Gtk::TreeModel::iterator iter      = mToolbar.mCBox_OutputId.get_active();
					cTMCRecord_Selection*    pt_record = mToolbar.mCBox_OutputId.rLStore->getTMCRecord();

					str_command += " " + Pdfchain::Cmd::OUTPUT;
					str_command += " " + Pdfchain::quote_path( str_targetfile );

					if ( iter ) {
						if ( Pdfchain::Id::ID_NEW != (*iter)[pt_record->mTMColumn_ID]
							&& Pdfchain::Id::ID_KEEP != (*iter)[pt_record->mTMColumn_ID] )
						{
							str_command += " " + iter->get_value( pt_record->mTMColumn_Command );
						}
					}
				}
				else {
					str_command = "";
				}
				break;
			}
			default:
				mFCDialog_SaveAs.hide();
				str_command = "";
				break;
		}
	}
	
	return str_command;
}


/*
 * window_main_tool.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window_main_tool.h"


/*** Section : Tool ***********************************************************/

// Constructor
cSection_Tool::cSection_Tool( Gtk::Window& ref_window )
:
mHBox_SourceFile( false , Pdfchain::SPACING ),	// homogenous , spacing
mHBox_UpdateInfo( false , Pdfchain::SPACING ),
mHBox_FillForm(   false , Pdfchain::SPACING ),

mTable_Tool( 15 , 4 , false ),	// y_rows , x_columns , homogenous )

mFCButton_SourceFile(   ref_window , _("Tools - Select source PDF file ...") ),	// parent_window , title
mFCButton_DumpDataFile( ref_window , _("Tools - Select dump data file ...")  ),
mFCButton_FdfFile(      ref_window , _("Tools - Select FDF file ...")        ),

mFCDialog_SelectFolder( ref_window , _("Tools - Select output folder ...")     ),	// parent_window , title
mFCDialog_SaveAs_Pdf(   ref_window , _("Tools - Save output PDF file as ...")  ),
mFCDialog_SaveAs_Fdf(   ref_window , _("Tools - Save output FDF file as ...")  ),
mFCDialog_SaveAs_Dump(  ref_window , _("Tools - Save output dump file as ...") ),

mLabel_SourceFile(   _("Document:")       , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),	// label , xalign , yalign , mnemonic
mLabel_DumpDataFile( _("Dump data file:") , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),
mLabel_FdfFile(      _("FDF file:")       , Gtk::ALIGN_END , Gtk::ALIGN_CENTER , false ),

mCButton_DumpDataFields_Utf8( _("UTF-8")            , false ),	// label , mnemonic
mCButton_DumpData_Utf8(       _("UTF-8")            , false ),
mCButton_UpdateInfo_Utf8(     _("UTF-8")            , false ),
mCButton_FillForm_DropXfa(    _("Drop XFA")         , false ),
mCButton_FillForm_Flatten(    _("Flatten")          , false ),
mCButton_FillForm_Appearance( _("Need appearances") , false ),

mRBGroup_Tool( mRButton_UnpackFiles.get_group() ),

mRButton_Repair(         mRBGroup_Tool , _("Repair docu_ment")                     , true ),	// group , label , mnemonic
mRButton_UnpackFiles(    mRBGroup_Tool , _("Un_pack attached files from document") , true ),
mRButton_Uncompress(     mRBGroup_Tool , _("_Uncompress document")                 , true ),
mRButton_Compress(       mRBGroup_Tool , _("C_ompress document")                   , true ),
mRButton_DumpDataAnnots( mRBGroup_Tool , _("Dump a_nnotation data from document")  , true ),
mRButton_DumpDataFields( mRBGroup_Tool , _("Dump data _fields from document")      , true ),
mRButton_DumpData(       mRBGroup_Tool , _("_Dump data from document")             , true ),
mRButton_UpdateInfo(     mRBGroup_Tool , _("Update _info of document")             , true ),
mRButton_GenerateFdf(    mRBGroup_Tool , _("_Generate FDF from document")          , true ),
mRButton_FillForm(       mRBGroup_Tool , _("Fill fo_rm of document")               , true ),
mRButton_Flatten(        mRBGroup_Tool , _("Document f_latten")                    , true ),
mRButton_DropXfa(        mRBGroup_Tool , _("Drop _XFA from document")              , true ),

mHSeparator_A( Gtk::ORIENTATION_HORIZONTAL ),	// orientation
mHSeparator_B( Gtk::ORIENTATION_HORIZONTAL ),
mHSeparator_C( Gtk::ORIENTATION_HORIZONTAL ),
mHSeparator_D( Gtk::ORIENTATION_HORIZONTAL ),

mVSeparator_FillForm( Gtk::ORIENTATION_VERTICAL ),

rSGroup_FCButton( Gtk::SizeGroup::create( Gtk::SIZE_GROUP_HORIZONTAL ) )
{
	
	mRButton_Repair.set_tooltip_text(         _("Repairs corrupted XREF table and stream lengths, if possible") );
	mRButton_UnpackFiles.set_tooltip_text(    _("Copys Documents attachments into output directory") );
	mRButton_Uncompress.set_tooltip_text(     _("Uncompess for editing PDF code with text editor (like vim or emacs)") );
	mRButton_Compress.set_tooltip_text(       _("Restores compession") );
	mRButton_DumpDataAnnots.set_tooltip_text( _("Reports link annotations of document") );
	mRButton_DumpDataFields.set_tooltip_text( _("Reports form field data of document") );
	mRButton_DumpData.set_tooltip_text(       _("Reports statistics of document") );
	mRButton_UpdateInfo.set_tooltip_text(     _("Changes the bookmarks and metadata at document") );
	mRButton_GenerateFdf.set_tooltip_text(    _("Generates a dummy FDF from document") );
	mRButton_FillForm.set_tooltip_text(       _("Fills document's form fields with the data from an FDF file or XFDF file") );
	mRButton_Flatten.set_tooltip_text(        _("Merges document's interactive form fields (and their data) with the document's pages") );
	mRButton_DropXfa.set_tooltip_text(        _("Removes the form's XFA data") );

	mCButton_DumpDataFields_Utf8.set_tooltip_text( _("Generate UTF-8 encoding") );
	mCButton_DumpData_Utf8.set_tooltip_text(       _("Generate UTF-8 encoding") );
	mCButton_UpdateInfo_Utf8.set_tooltip_text(     _("Generate UTF-8 encoding") );
	mCButton_FillForm_DropXfa.set_tooltip_text(    _("Removes the form's XFA data") );
	mCButton_FillForm_Flatten.set_tooltip_text(    _("Merges document's interactive form fields (and their data) with the document's pages") );
	mCButton_FillForm_Appearance.set_tooltip_text( _("Sets a flag that cues Reader/Acrobat to generate new field appearances based on the form field values.") );

	rSGroup_FCButton->add_widget( mLabel_DumpDataFile );
	rSGroup_FCButton->add_widget( mLabel_FdfFile );

	mFCButton_SourceFile.set_halign(   Gtk::ALIGN_START );
	mFCButton_DumpDataFile.set_halign( Gtk::ALIGN_START );
	mFCButton_FdfFile.set_halign(      Gtk::ALIGN_START );
	
	mAlign_HSeparatorA.set_padding( Pdfchain::BORDER , Pdfchain::BORDER , 0 , 0 );	// top , bottom , left , right
	mAlign_HSeparatorB.set_padding( Pdfchain::BORDER , Pdfchain::BORDER , 0 , 0 );
	mAlign_HSeparatorC.set_padding( Pdfchain::BORDER , Pdfchain::BORDER , 0 , 0 );
	mAlign_HSeparatorD.set_padding( Pdfchain::BORDER , Pdfchain::BORDER , 0 , 0 );

	mAlign_HSeparatorA.add( mHSeparator_A );
	mAlign_HSeparatorB.add( mHSeparator_B );
	mAlign_HSeparatorC.add( mHSeparator_C );
	mAlign_HSeparatorD.add( mHSeparator_D );

	mHBox_SourceFile.set_border_width( Pdfchain::BORDER );
	mHBox_SourceFile.pack_start( mLabel_SourceFile      , false , false , 0 );	// widget , expand , fill , padding
	mHBox_SourceFile.pack_start( mFCButton_SourceFile   , true  , true  , 0 );

	mHBox_UpdateInfo.pack_start( mLabel_DumpDataFile    , false , false , 0 );
	mHBox_UpdateInfo.pack_start( mFCButton_DumpDataFile , true  , true  , 0 );

	mHBox_FillForm.pack_start( mLabel_FdfFile           , false , false , 0 );
	mHBox_FillForm.pack_start( mFCButton_FdfFile        , true  , true  , 0 );
	
	mTable_Tool.set_border_width( Pdfchain::BORDER );
	mTable_Tool.set_col_spacings( Pdfchain::SPACING );
	mTable_Tool.attach( mRButton_Repair              , 0 , 5 ,  0 ,  1 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Repair
	mTable_Tool.attach( mAlign_HSeparatorA           , 0 , 5 ,  1 ,  2 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_UnpackFiles         , 0 , 5 ,  2 ,  3 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Unpack files
	mTable_Tool.attach( mAlign_HSeparatorB           , 0 , 5 ,  3 ,  4 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_Uncompress	         , 0 , 5 ,  4 ,  5 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Uncompress
	mTable_Tool.attach( mRButton_Compress            , 0 , 5 ,  5 ,  6 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Compress
	mTable_Tool.attach( mAlign_HSeparatorC           , 0 , 5 ,  6 ,  7 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_DumpDataAnnots      , 0 , 4 ,  7 ,  8 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Dump data annots
	mTable_Tool.attach( mRButton_DumpDataFields      , 0 , 4 ,  8 ,  9 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Dump data fields
	mTable_Tool.attach( mCButton_DumpDataFields_Utf8 , 4 , 5 ,  8 ,  9 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_DumpData            , 0 , 4 ,  9 , 10 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Dump data
	mTable_Tool.attach( mCButton_DumpData_Utf8       , 4 , 5 ,  9 , 10 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_UpdateInfo          , 0 , 4 , 10 , 11 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Update info
	mTable_Tool.attach( mCButton_UpdateInfo_Utf8     , 4 , 5 , 10 , 11 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mHBox_UpdateInfo             , 0 , 5 , 11 , 12 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	
	mTable_Tool.attach( mAlign_HSeparatorD           , 0 , 5 , 12 , 13 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_GenerateFdf         , 0 , 5 , 13 , 14 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Generate FDF
	mTable_Tool.attach( mRButton_FillForm            , 0 , 1 , 14 , 15 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Fill form
	mTable_Tool.attach( mCButton_FillForm_DropXfa    , 1 , 2 , 14 , 15 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mVSeparator_FillForm         , 2 , 3 , 14 , 15 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mCButton_FillForm_Appearance , 3 , 4 , 14 , 15 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mCButton_FillForm_Flatten    , 4 , 5 , 14 , 15 , Gtk::FILL | Gtk::SHRINK , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mHBox_FillForm               , 0 , 5 , 15 , 16 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );
	mTable_Tool.attach( mRButton_Flatten             , 0 , 5 , 16 , 17 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Flatten
	mTable_Tool.attach( mRButton_DropXfa             , 0 , 5 , 17 , 18 , Gtk::FILL | Gtk::EXPAND , Gtk::FILL | Gtk::SHRINK , 0 , 0 );	// Drop XFA

	mSWindow_Tool.add( mTable_Tool );

	pack_start( mHBox_SourceFile , false , false , 0 );	// widget , expand , fill , padding
	pack_start( mSWindow_Tool    , true  , true  , 0 );

	mRButton_DumpDataFields.signal_toggled().connect( sigc::mem_fun(
		*this , &cSection_Tool::onRButton_DumpDataFields_toggled ) );
		
	mRButton_DumpData.signal_toggled().connect( sigc::mem_fun(
		*this , &cSection_Tool::onRButton_DumpData_toggled ) );
		
	mRButton_UpdateInfo.signal_toggled().connect( sigc::mem_fun(
		*this , &cSection_Tool::onRButton_UpdateInfo_toggled ) );
		
	mRButton_FillForm.signal_toggled().connect( sigc::mem_fun(
		*this , &cSection_Tool::onRButton_FillForm_toggled ) );

	mCButton_FillForm_Flatten.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Tool::onCButton_FillForm_Flatten_toggled ) );

	mCButton_FillForm_Appearance.signal_toggled().connect(
		sigc::mem_fun( *this , &cSection_Tool::onCButton_FillForm_Appearance_toggled ) );

	init();
}

// Destructor
cSection_Tool::~cSection_Tool()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cSection_Tool::~cSection_Tool()";	//TEST
#endif
}


// Method (public) : clear
void
cSection_Tool::clear()
{
	mFCButton_SourceFile.clear();
	mFCButton_DumpDataFile.clear();
	mFCButton_FdfFile.clear();

	mFCDialog_SelectFolder.clear();
	mFCDialog_SaveAs_Pdf.clear();
	mFCDialog_SaveAs_Fdf.clear();
	mFCDialog_SaveAs_Dump.clear();

	init();
	return;
}


// Method (public) : create command
std::string
cSection_Tool::createCommand()
{
	std::string str_command		= "";
	std::string str_sourcefile	= "";
	std::string str_dumpfile	= "";
	std::string str_fdffile		= "";
	std::string str_output		= "";

	if ( "" != ( str_sourcefile = mFCButton_SourceFile.get_filename() ) ) {

		if ( mRButton_UnpackFiles.get_active() ) {
			switch ( mFCDialog_SelectFolder.run() ) {
				case Gtk::RESPONSE_OK:
					mFCDialog_SelectFolder.hide();
					str_output = mFCDialog_SelectFolder.get_current_folder();
					break;
				default:
					mFCDialog_SelectFolder.hide();
					break;
			}
		}
		else if ( mRButton_DumpDataAnnots.get_active() || mRButton_DumpDataFields.get_active() || mRButton_DumpData.get_active() ) {
			switch ( mFCDialog_SaveAs_Dump.run() ) {
				case Gtk::RESPONSE_OK:
					mFCDialog_SaveAs_Dump.hide();
					str_output = mFCDialog_SaveAs_Dump.get_filename();
				default:
					mFCDialog_SaveAs_Dump.hide();
					break;
			}
		}
		else if ( mRButton_GenerateFdf.get_active() ) {
			switch ( mFCDialog_SaveAs_Fdf.run() ) {
				case Gtk::RESPONSE_OK:
					mFCDialog_SaveAs_Fdf.hide();
					str_output = mFCDialog_SaveAs_Fdf.get_filename();
				default:
					mFCDialog_SaveAs_Fdf.hide();
					break;
			}
		}
		else if ( mRButton_UpdateInfo.get_active() ) {
			if ( "" != ( str_dumpfile = mFCButton_DumpDataFile.get_filename() ) ) {
				switch ( mFCDialog_SaveAs_Pdf.run() ) {
					case Gtk::RESPONSE_OK:
						mFCDialog_SaveAs_Pdf.hide();
						str_output = mFCDialog_SaveAs_Pdf.get_filename();
					default:
						mFCDialog_SaveAs_Pdf.hide();
						break;
				}
			}
		}
		else if ( mRButton_FillForm.get_active() ) {
			if ( "" != ( str_fdffile = mFCButton_FdfFile.get_filename() ) ) {
				switch ( mFCDialog_SaveAs_Pdf.run() ) {
					case Gtk::RESPONSE_OK:
						mFCDialog_SaveAs_Pdf.hide();
						str_output = mFCDialog_SaveAs_Pdf.get_filename();
					default:
						mFCDialog_SaveAs_Pdf.hide();
						break;
				}
			}
		}
		else {
			switch ( mFCDialog_SaveAs_Pdf.run() ) {
				case Gtk::RESPONSE_OK:
					mFCDialog_SaveAs_Pdf.hide();
					str_output = mFCDialog_SaveAs_Pdf.get_filename();
				default:
					mFCDialog_SaveAs_Pdf.hide();
					break;
			}
		}

		if ( "" != str_output ) {
		
			if ( mRButton_Repair.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_UnpackFiles.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::UNPACK_FILES;
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_Uncompress.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
				str_command += " " + Pdfchain::Cmd::UNCOMPRESS;
			}
			else if ( mRButton_Compress.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
				str_command += " " + Pdfchain::Cmd::COMPRESS;
			}
			else if ( mRButton_DumpDataAnnots.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::DUMP_DATA_ANNOTS;
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_DumpDataFields.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );

				if ( true == mCButton_DumpDataFields_Utf8.get_active() ) {
					str_command += " " + Pdfchain::Cmd::DUMP_DATA_FIELDS_UTF8;
				} else {
					str_command += " " + Pdfchain::Cmd::DUMP_DATA_FIELDS;
				}
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_DumpData.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				
				if ( true == mCButton_DumpData_Utf8.get_active() ) {
					str_command += " " + Pdfchain::Cmd::DUMP_DATA_UTF8;
				} else {
					str_command += " " + Pdfchain::Cmd::DUMP_DATA;
				}
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_UpdateInfo.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );

				if ( true == mCButton_UpdateInfo_Utf8.get_active() ) {
					str_command += " " + Pdfchain::Cmd::UPDATE_INFO_UTF8;
				} else {
					str_command += " " + Pdfchain::Cmd::UPDATE_INFO;
				}
				str_command += " " + Pdfchain::quote_path( str_dumpfile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_GenerateFdf.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::GENERATE_FDF;
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
			}
			else if ( mRButton_FillForm.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::FILL_FORM;
				str_command += " " + Pdfchain::quote_path( str_fdffile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );

				if ( mCButton_FillForm_DropXfa.get_active() ) 
					str_command += " " + Pdfchain::Cmd::DROP_XFA;

				if ( mCButton_FillForm_Flatten.get_active() ) 
					str_command += " " + Pdfchain::Cmd::FLATTEN;

				if ( mCButton_FillForm_Appearance.get_active() ) 
					str_command += " " + Pdfchain::Cmd::NEED_APPEARANCES;
			}
			else if ( mRButton_Flatten.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
				str_command += " " + Pdfchain::Cmd::FLATTEN;
			}
			else if ( mRButton_DropXfa.get_active() ) {
				str_command  = " " + Pdfchain::quote_path( str_sourcefile );
				str_command += " " + Pdfchain::Cmd::OUTPUT;
				str_command += " " + Pdfchain::quote_path( str_output );
				str_command += " " + Pdfchain::Cmd::DROP_XFA;
			}
		}
	}
	
	return str_command;
}


// Method (protected) : init
void
cSection_Tool::init()
{
	mRButton_Repair.set_active( true );

	mCButton_DumpDataFields_Utf8.set_active( true );
	mCButton_DumpData_Utf8.set_active( true );
	mCButton_UpdateInfo_Utf8.set_active( true );
	mCButton_FillForm_DropXfa.set_active( true );
	mCButton_FillForm_Flatten.set_active( false );

	mLabel_DumpDataFile.set_sensitive( false );
	mFCButton_DumpDataFile.set_sensitive( false );
	mLabel_FdfFile.set_sensitive( false );
	mFCButton_FdfFile.set_sensitive( false );
	mVSeparator_FillForm.set_sensitive( false );
	mCButton_DumpDataFields_Utf8.set_sensitive( false );
	mCButton_DumpData_Utf8.set_sensitive( false );
	mCButton_UpdateInfo_Utf8.set_sensitive( false );
	mCButton_FillForm_DropXfa.set_sensitive( false );
	mCButton_FillForm_Flatten.set_sensitive( false );
	mCButton_FillForm_Appearance.set_sensitive( false );

	return;
}

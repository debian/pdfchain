/*
 * window_main_attachment.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_ATTACHMENT_H__
#define __PDFCHAIN_WINDOW_MAIN_ATTACHMENT_H__

#include "pdfchain.h"
#include "store_attachment.h"
#include "dialog_filechooser.h"

class cTView_Attach;
class cToolbar_Attach;
class cSection_Attach;


/*** Tree View : Attachment ***************************************************/

class cTView_Attach : public Gtk::TreeView
{
	friend class cToolbar_Attach;
	friend class cSection_Attach;
	
	public:

		// Constructor
		         cTView_Attach( Gtk::Window& );
		virtual ~cTView_Attach();
		
		// Methodes
		void clear();
		
	protected:

		// Anchors
		Glib::RefPtr<cLStore_Attach> rLStore_Attach;
		cTMCRecord_Attach*           pTMCRecord_Attach;

		// Widgets
		Gtk::TreeViewColumn
			mTVColumn_AddPages ,
			mTVColumn_AttachFile;

		Gtk::CellRendererToggle mCRToggle_Add;
		Gtk::CellRendererText   mCRText_AttachFile;

#ifdef PDFCHAIN_TEMP
		Gtk::TreeViewColumn
			mTVColumn_AttachPath,		//TEMP
		    mTVColumn_AttachTooltip;	//TEMP

		Gtk::CellRendererText
			mCRText_AttachPath,			//TEMP
		    mCRText_AttachTooltip;		//TEMP
#endif
		
		// Signal Handler
		inline void onCRToggle_Add_toggled(
			const Glib::ustring& str_path )
		{
			rLStore_Attach->toggleAdd( Gtk::TreePath( str_path ) );
			return;
		}

		inline void onToolButton_Up_clicked() {
			rLStore_Attach->moveRowsUp(	get_selection() );
			return;
		}

		inline void onToolButton_Down_clicked() {
			
			rLStore_Attach->moveRowsDown( get_selection() );
			return;
		}

		inline void onToolButton_Add_clicked() {
			rLStore_Attach->addRows( get_selection() );
			return;
		}

		inline void onToolButton_Remove_clicked() {
			rLStore_Attach->removeRows( get_selection() );
			return;
		}
};



/*** Toolbar : Attach **********************************************************/

class
cToolbar_Attach : public Gtk::Toolbar
{
	friend class cSection_Attach;
	
	public:

		// Constructor
		         cToolbar_Attach( cTView_Attach* );
		virtual ~cToolbar_Attach();
		
		// Methodes
		void clear();

	protected:

		//
		Glib::RefPtr<Gtk::Adjustment> rAdjust_PageNumber;	// Declaration of Adjustment before SpinButton!
		Gtk::RadioToolButton::Group   mRTBGroup_AddTo;

		// Widgets
		Gtk::SpinButton mSButton_PageNumber;
		Gtk::ToolItem   mToolItem_PageNumber;

		Gtk::ToolButton
			mToolButton_Add,
			mToolButton_Remove,
			mToolButton_Up,
		    mToolButton_Down;

		Gtk::RadioToolButton
			mRTButton_AddToFile,
		    mRTButton_AddToPage;

		Gtk::SeparatorToolItem
			mSeparator_A,
		    mSeparator_B,
		    mSeparator_C;
		
		// Signal Handler
		inline void onRTButton_AddToPage_toggled() {
			mSButton_PageNumber.set_sensitive( mRTButton_AddToPage.property_active() );
			return;
		}
};



/*** Section : Attach *********************************************************/

class
cSection_Attach : public Gtk::VBox
{
	public:

		// Constructor
		         cSection_Attach( Gtk::Window& );
		virtual ~cSection_Attach();

		// Methodes
		void clear();
		std::string createCommand();

	protected:

		// Variables
		guint vPageNumbers;

		// Widgets
		Gtk::HBox           mHBox_SourceFile;
		Gtk::Label          mLabel_SourceFile;
		Gtk::ScrolledWindow mSWindow_AttachFiles;

		// Derived Widgets
		cFCButton_Pdf        mFCButton_SourceFile;
		cTView_Attach        mTView_AttachFiles;
		cToolbar_Attach      mToolbar_AttachFiles;
		cFCDialog_SaveAs_Pdf mFCDialog_SaveAs;

		// Signal Handler
		inline void onFCButton_SourceFile_file_set();
};

#endif

/*
 * dialog_filechooser_extension.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_DIALOG_FILECHOOSER_EXTENSION_H__
#define __PDFCHAIN_DIALOG_FILECHOOSER_EXTENSION_H__

#include "pdfchain.h"
#include "store.h"


/*** File Filter **************************************************************/

// File Filter : Any Files
class
cFFilter_Any : public Gtk::FileFilter
{
	public:

		cFFilter_Any() {
			
			set_name( _("Any files") );
			add_pattern( "*" );
		}

		virtual ~cFFilter_Any() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cFFilter_Any::~cFFilter_Any()";	//TEST
#endif
		}
};


// File Filter : PDF Files
class
cFFilter_Pdf : public Gtk::FileFilter
{
	public:

		cFFilter_Pdf() {
			
			set_name( _("PDF files") );
			add_mime_type( "application/pdf" );
			//add_pattern( "*.pdf" );
		}

		virtual ~cFFilter_Pdf() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cFFilter_Pdf::~cFFilter_Pdf()";	//TEST
#endif
		}
};


// File Filter : Text Files
class
cFFilter_Text : public Gtk::FileFilter
{
	public:

		cFFilter_Text() {
			
			set_name( _("Text files") );
			add_mime_type( "text/plain" );
			//add_pattern( "*.txt" );
		}

		virtual ~cFFilter_Text() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cFFilter_Text::~cFFilter_Text()";	//TEST
#endif
		}
};


// File Filter : FDF Files
class
cFFilter_Fdf : public Gtk::FileFilter
{
	public:

		cFFilter_Fdf() {
			
			set_name( _("FDF files") );
			//add_mime_type( "text/plain" );
			add_pattern( "*.fdf" );
		}

		virtual ~cFFilter_Fdf() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cFFilter_Fdf::~cFFilter_Fdf()";	//TEST
#endif
		}
};


// File Filter : Dump Files
class
cFFilter_Dump : public Gtk::FileFilter
{
	public:

		cFFilter_Dump() {
			
			set_name( _("Dump files") );
			//add_mime_type( "text/plain" );
			add_pattern( "*.dump" );
		}

		virtual ~cFFilter_Dump() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cFFilter_Dump::~cFFilter_Dump()";	//TEST
#endif
		}
};

#endif

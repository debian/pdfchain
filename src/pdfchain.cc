/*
 * pdfchain.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pdfchain.h"


/*** Global Objects ***********************************************************/

//Gtk::Window*    Pdfchain::pWindow  = NULL;
Gtk::Statusbar* Pdfchain::pSBar    = NULL;



/*** Global Functions *********************************************************/

// Function (global) : Extract filname from path
std::string
Pdfchain::extract_filename_from_path( const std::string& string )
{
	std::string				str_path	= string;
	std::string::size_type	pos			= 0;

	if ( ( pos = str_path.rfind( G_DIR_SEPARATOR ) ) != std::string::npos )
		str_path.erase( 0 , pos+1 );
	
	return str_path;
}


// Function (global) : Count page numbers of PDF file
// Alternatively: "pdftk file.pdf dump_data | grep NumberOfPages"
guint
Pdfchain::count_page_numbers( const std::string& str_path )
{
	guint message_id = 0;

	if ( NULL != Pdfchain::pSBar )
		message_id = Pdfchain::pSBar->push( _("Counting pages ...") );
	
	Glib::RefPtr<Gio::File> ref_file = Gio::File::create_for_path( str_path );
	guint pages = 0;
	
	if ( ref_file->query_exists() ) {

		Glib::RefPtr<Gio::FileInputStream> ref_stream = ref_file->read();
		gssize	read_bytes = 0;
		gchar	read_buffer[Pdfchain::Count::BUFFER_SIZE];

		Glib::ustring				str_content = "";
		Glib::ustring::size_type	pos = 0;

		do {
			read_bytes	 = ref_stream->read( read_buffer , sizeof( read_buffer ) );
			str_content	+= Glib::ustring( read_buffer , read_buffer + read_bytes );
		} while ( read_bytes );

		ref_stream->close();

		for ( pos = 0 ; Glib::ustring::npos != ( pos = str_content.find( Pdfchain::Count::TYPE_PAGE_A , pos ) ) ; pos++ , pages++ );
		for ( pos = 0 ; Glib::ustring::npos != ( pos = str_content.find( Pdfchain::Count::TYPE_PAGE_B , pos ) ) ; pos++ , pages++ );
		for ( pos = 0 ; Glib::ustring::npos != ( pos = str_content.find( Pdfchain::Count::TYPE_PAGE_C , pos ) ) ; pos++ , pages-- );
		for ( pos = 0 ; Glib::ustring::npos != ( pos = str_content.find( Pdfchain::Count::TYPE_PAGE_D , pos ) ) ; pos++ , pages-- );
	}

	if ( NULL != Pdfchain::pSBar )
		Pdfchain::pSBar->remove_message( message_id );
		
	return pages;
}


// Function (global) : Count digits of a guint octal
guint
Pdfchain::count_digits_of_guint_oct( guint guint_tmp )
{
	guint digits = 0;

	while ( 0 < guint_tmp ) {
		guint_tmp /= 8;
		++digits;
	}
	
	return digits;
}


// Function (global) : Count digits of a guint decimal
guint
Pdfchain::count_digits_of_guint_dec( guint guint_tmp )
{
	guint digits = 0;

	while ( 0 < guint_tmp ) {
		guint_tmp /= 10;
		++digits;
	}
	
	return digits;
}


// Function (global) : Count digits of a guint hexadecimal
guint
Pdfchain::count_digits_of_guint_hex( guint guint_tmp )
{
	guint digits = 0;

	while ( 0 < guint_tmp ) {
		guint_tmp /= 16;
		++digits;
	}
	
	return digits;
}


// Function (global) : Convert guint to string
std::string
Pdfchain::convert_guint_to_string( guint guint_tmp )
{
	std::string string = "";

	if ( 0 == guint_tmp ) {
		string = "0";
	}
	else {
		while ( 0 < guint_tmp ) {
			string.insert( 0 , 1 , guint_tmp % 10 + '0' );
			guint_tmp /= 10;
		}
	}
	
	return string;
}


// Function (global) : escape each special characters at string
std::string
Pdfchain::escape_string( const std::string& string )
{
	std::string				str_escape	= string;
	std::string::size_type	pos			= 0;

	// Escapes each (known) special character with '\\' (ESC) until '\0' (EOL) was found
	for ( int i=0 ; Char::EOL != Path::ESCAPE_CHARS[i] ; i++ , pos=0 ) {
		while ( std::string::npos != ( pos = str_escape.find( Path::ESCAPE_CHARS[i] , pos ) ) ) {
			str_escape.insert( pos , 1 , Char::ESC );
			pos += 2;
		}
	}

	return str_escape;
}


// Function (global) : Quotes string and escapes special path characters
std::string
Pdfchain::quote_path( const std::string& str_path )
{
	return "\"" + escape_string( str_path ) + "\"";
}


// Function (global) : Execute PDFTK command
int
Pdfchain::execute_command( const std::string& str_command )
{
	guint message_id = 0;

	if ( NULL != Pdfchain::pSBar )
		message_id = Pdfchain::pSBar->push( _("Executing command ...") );
	
	std::cout << "COMMAND: " << str_command << std::endl;

	int error_code = system( str_command.c_str() );
	
	if ( NULL != Pdfchain::pSBar )
		Pdfchain::pSBar->remove_message( message_id );

	return error_code;
}

/*
 * main.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pdfchain.h"
#include "constant.h"
#include "window_main.h"


/*
int
main(
	int argc ,
	char *argv[] )
{
	Glib::RefPtr<Gtk::Application>
		app = Gtk::Application::create( argc , argv , "pdfchain.sourceforge.net" );

	cWindow_Main mWindow_Main;

	return app->run( mWindow_Main );
}
*/

int
main ( int argc , char *argv[] )
{
	Gtk::Main kit( argc , argv );
	
	cWindow_Main
		*pWindow_Main = NULL;
		
	std::cout << std::endl << "PDF Chain " << VERSION
		<< " - A graphical user interface for the PDF Toolkit"
		<< std::endl << std::endl;

	pWindow_Main = new cWindow_Main();
	if ( pWindow_Main ) {
		kit.run( *pWindow_Main );
	}

	if( NULL != pWindow_Main ) {
		delete pWindow_Main;
		pWindow_Main = NULL;
	}

#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "end pdfchain";	//TEST
#endif
	
	return 0;
}

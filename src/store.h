/*
 * store.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_STORE_H__
#define __PDFCHAIN_STORE_H__

#include "pdfchain.h"


/*** Tree Model Column Record : Selection *************************************/

class
cTMCRecord_Selection : public Gtk::TreeModel::ColumnRecord
{
	public:

		         cTMCRecord_Selection();
		virtual ~cTMCRecord_Selection();
		
		Gtk::TreeModelColumn<guchar>		mTMColumn_ID;
		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Label;
//		Gtk::TreeModelColumn<Glib::ustring>	mTMColumn_Tooltip;
		Gtk::TreeModelColumn<std::string>	mTMColumn_Command;
};



/*** ListStore : Output ID ****************************************************/

class
cLStore_OutputId : public Gtk::ListStore
{
	public:
		         cLStore_OutputId();
		virtual ~cLStore_OutputId();

		static Glib::RefPtr<cLStore_OutputId> create();
		cTMCRecord_Selection* getTMCRecord();

		Gtk::TreeModel::iterator select( guchar );	// FIXME: use Pdfchain::Id::OutputId (enum) instead of guchar
		
	protected:
	
	cTMCRecord_Selection* pTMCRecord;
	
	void append_KeepId();
	void erase_KeepId();
};



/*** ListStore : Even Odd *****************************************************/

class
cLStore_EvenOdd : public Gtk::ListStore
{
	public:

		         cLStore_EvenOdd();
		virtual ~cLStore_EvenOdd();

		static Glib::RefPtr<cLStore_EvenOdd> create();
		cTMCRecord_Selection* getTMCRecord();

	protected:

		cTMCRecord_Selection* pTMCRecord;
};



/*** ListStore : Rotation *****************************************************/

class
cLStore_Rotation : public Gtk::ListStore
{
	public:

		         cLStore_Rotation();
		virtual ~cLStore_Rotation();

		static Glib::RefPtr<cLStore_Rotation> create();
		cTMCRecord_Selection* getTMCRecord();

	protected:
		
		cTMCRecord_Selection* pTMCRecord;
};



/*** ListStore : Counting Base ***********************************************/

class
cLStore_CountingBase : public Gtk::ListStore
{
	public:
		         cLStore_CountingBase();
		virtual ~cLStore_CountingBase();

		static Glib::RefPtr<cLStore_CountingBase> create();
		cTMCRecord_Selection* getTMCRecord();

	protected:
		
		cTMCRecord_Selection* pTMCRecord;
};


#endif

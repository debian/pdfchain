/*
 * store_attachment.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "store_attachment.h"


/*** List Store : Attachment **************************************************/

/// Constructor
cLStore_Attach::cLStore_Attach( Gtk::Window& ref_window )
:
mFCDialog( ref_window , _("Attachment - Add attachment files") ),
pTMCRecord( NULL )
{
	pTMCRecord = new cTMCRecord_Attach;
	set_column_types( *pTMCRecord );
}


// Destructor
cLStore_Attach::~cLStore_Attach()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cLStore_Attach::~cLStore_Attach()";	//TEST
#endif

	if( pTMCRecord != NULL ) { delete pTMCRecord;	pTMCRecord = NULL; }
}


// Method (public) : add new rows (files)
void
cLStore_Attach::addRows( Glib::RefPtr<Gtk::TreeSelection> ref_tree_selections )
{

	switch ( mFCDialog.run() ) {

		case Gtk::RESPONSE_OK: {
			mFCDialog.hide();

			std::string                        str_sourcepath   = "";
			std::vector<std::string>           vect_sourcepaths = mFCDialog.get_filenames();
			std::vector<std::string>::iterator iter_sourcepaths = vect_sourcepaths.begin();
			Gtk::TreeModel::iterator           iter_selections;
			Gtk::TreeModel::iterator           iter_org;
			Gtk::TreeModel::iterator           iter_new;
				
			// Get begin of TreeView selection and get TreeModel iterator of selection
			if ( 0 != ref_tree_selections->count_selected_rows() ) {
				
				std::vector<Gtk::TreeModel::Path> list_selections = ref_tree_selections->get_selected_rows();
				Gtk::TreeModel::Path              path_selections = *( list_selections.begin() );

				iter_selections = get_iter( path_selections );
			}

			// Add sourcefiles at ListStore
			while ( vect_sourcepaths.end() != iter_sourcepaths ) {
				
				str_sourcepath = *iter_sourcepaths;

				if ( iter_selections )
					iter_new = insert( iter_selections );
				else
					iter_new = append();

				(*iter_new)[ pTMCRecord->mTMColumn_Add           ] = TRUE;
				(*iter_new)[ pTMCRecord->mTMColumn_AttachFile    ] = Glib::filename_to_utf8( Pdfchain::extract_filename_from_path( str_sourcepath ) );
				(*iter_new)[ pTMCRecord->mTMColumn_AttachPath    ] = str_sourcepath;
				(*iter_new)[ pTMCRecord->mTMColumn_AttachTooltip ] = _("<b>Path: </b>") + Glib::Markup::escape_text( Glib::filename_to_utf8( str_sourcepath ) );

				iter_sourcepaths++;
			}

			break;
		}

		default:
			mFCDialog.hide();
			break;
	}

	return;
}


// Method (public) : remove selected rows
void
cLStore_Attach::removeRows( Glib::RefPtr<Gtk::TreeSelection> ref_tree_selections )
{
	std::vector<Gtk::TreeModel::Path> list_selections = ref_tree_selections->get_selected_rows();
	Gtk::TreeModel::iterator          iter_next;

	if ( list_selections.size() ) {

		// Define row to select as soon as selected rows are removed
		iter_next = get_iter( list_selections.back() );
		if ( ++iter_next == children().end() ) {
			if ( children().begin() != get_iter( list_selections.front() ) ) {
				iter_next = get_iter( list_selections.front() );
				iter_next--;
			}
		}

		// Remove selected rows
		while ( list_selections.size() ) {
			erase( get_iter( list_selections.back() ) );
			list_selections.erase( list_selections.end() );
		}

		if ( iter_next )
			ref_tree_selections->select( iter_next );
	}

	return;
}


// Method (public) : move selected rows up
void
cLStore_Attach::moveRowsUp( Glib::RefPtr<Gtk::TreeSelection> ref_tree_selections ) {

	std::vector<Gtk::TreeModel::Path> list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {
		Gtk::TreeModel::iterator iter_selections;
		Gtk::TreeModel::iterator iter_upper;

		// Move every row only if first selected row is not the first row at the ListStore
		if ( children().begin() !=  get_iter( list_selections.front() ) ) {

			while ( list_selections.size() ) {
				iter_upper = iter_selections = get_iter( list_selections.front() );
				
				iter_swap( iter_selections , --iter_upper );
				list_selections.erase( list_selections.begin() );
			}
		}
	}

	return;
}


// Method (public) : move selected rows down
void
cLStore_Attach::moveRowsDown( Glib::RefPtr<Gtk::TreeSelection> ref_tree_selections ) {

	std::vector<Gtk::TreeModel::Path> list_selections = ref_tree_selections->get_selected_rows();

	if ( list_selections.size() ) {
		Gtk::TreeModel::iterator iter_selections;
		Gtk::TreeModel::iterator iter_lower;

		// Move every row only if last selected row is not the last row at the ListStore
		if ( --children().end() != get_iter( list_selections.back() ) ) {
			
			while ( list_selections.size() ) {
				iter_lower = iter_selections = get_iter( list_selections.back() );

				iter_swap( iter_selections , ++iter_lower );
				list_selections.erase( list_selections.end() );
			}
		}
	}
	
	return;
}


// Method (public) : toggle Add
void
cLStore_Attach::toggleAdd( const Gtk::TreePath& treepath )
{
	Gtk::TreeModel::iterator iter = get_iter( treepath );
	(*iter)[pTMCRecord->mTMColumn_Add] = ! (*iter)[pTMCRecord->mTMColumn_Add];
	return;
}


// Method (public, overwrite) :  clear
void
cLStore_Attach::clear()
{
	Gtk::ListStore::clear();
	mFCDialog.clear();

	return;
}


// Method (public) : create command
std::string
cLStore_Attach::createCommand()
{
	std::string              str_command = "";
	Gtk::TreeModel::iterator iter;

	if( NULL != children() ) {
		str_command += " " + Pdfchain::Cmd::ATTACH_FILES;
	
		// Attach each file only if CheckBox is enabled
		for ( iter = children().begin() ; children().end() != iter ; ++iter )
			if ( TRUE == (*iter)[pTMCRecord->mTMColumn_Add] )
				str_command += " " + Pdfchain::quote_path( iter->get_value( pTMCRecord->mTMColumn_AttachPath ) );
	}
	
	return str_command;
}

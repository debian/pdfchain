/*
 * store_attachment.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_STORE_ATTACHMENT_H__
#define __PDFCHAIN_STORE_ATTACHMENT_H__

#include "pdfchain.h"
#include "dialog_filechooser.h"


/*** Tree Model Column Record : Attachment ************************************/

class
cTMCRecord_Attach : public Gtk::TreeModel::ColumnRecord
{
	public:

		cTMCRecord_Attach() {
			add( mTMColumn_Add );
			add( mTMColumn_AttachFile );
			add( mTMColumn_AttachPath );
			add( mTMColumn_AttachTooltip );
		}

		virtual ~cTMCRecord_Attach() {
#ifdef PDFCHAIN_TEST
			std::cout << std::endl << "cTMCRecord_Attach::~cTMCRecord_Attach()";	//TEST
#endif
		}

		Gtk::TreeModelColumn<gboolean>      mTMColumn_Add;
		Gtk::TreeModelColumn<Glib::ustring> mTMColumn_AttachFile;
		Gtk::TreeModelColumn<std::string>   mTMColumn_AttachPath;
		Gtk::TreeModelColumn<Glib::ustring> mTMColumn_AttachTooltip;
};



/*** List Store : Attachment **************************************************/

class
cLStore_Attach : public Gtk::ListStore
{
	public:
		         cLStore_Attach( Gtk::Window& );
		virtual ~cLStore_Attach();
		
		static Glib::RefPtr<cLStore_Attach>	create( Gtk::Window& ref_window ) {
			return Glib::RefPtr<cLStore_Attach>( new cLStore_Attach( ref_window ) );
		}
		
		cTMCRecord_Attach*	getTMCRecord() {
			return pTMCRecord;
		}

//		void clear();	// Use method of motherclass instead only. Nothing else to do.

		void addRows      ( Glib::RefPtr<Gtk::TreeSelection> );
		void removeRows   ( Glib::RefPtr<Gtk::TreeSelection> );
		void moveRowsUp   ( Glib::RefPtr<Gtk::TreeSelection> );
		void moveRowsDown ( Glib::RefPtr<Gtk::TreeSelection> );

		void toggleAdd( const Gtk::TreePath& );

		void clear();
		std::string createCommand();

	protected:

		cTMCRecord_Attach*    pTMCRecord;
		cFCDialog_AddFile_Any mFCDialog;
};


#endif

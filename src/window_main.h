/*
 * window_main.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_H__
#define __PDFCHAIN_WINDOW_MAIN_H__

#include "pdfchain.h"
#include "dialog.h"
#include "window_main_cat.h"
#include "window_main_burst.h"
#include "window_main_bgst.h"
#include "window_main_attachment.h"
#include "window_main_tool.h"
#include "window_main_permission.h"

class cNotebook_Sections;
class cHBox_Execute;
class cWindow_Main;


/*** Notebook : Sections ******************************************************/

class
cNotebook_Sections : public Gtk::Notebook
{
	friend class cWindow_Main;
	
	public:
	
		// Constructor
		         cNotebook_Sections( Gtk::Window& );
		virtual ~cNotebook_Sections();
		
		// Methodes
		void clear();
		
	protected:
	
		// Derived Widgets (Sections)
		cSection_Cat    mSection_Cat;
		cSection_Burst  mSection_Burst;
		cSection_BgSt   mSection_BgSt;
		cSection_Attach mSection_Attach;
		cSection_Tool   mSection_Tool;
		
		// Widgets
		Gtk::Label
			mLabel_Cat,
			mLabel_Burst,
			mLabel_BgSt,
			mLabel_Attach,
			mLabel_Tool;

		Gtk::Alignment
			mAlign_Cat,
			mAlign_Burst,
			mAlign_BgSt,
			mAlign_Attach,
			mAlign_Tool;
};



/*** HBox : Execute ***********************************************************/

class
cHBox_Execute : public Gtk::HBox
{
	friend class cWindow_Main;
	
	public:
	
		// Constructor
		         cHBox_Execute();
		virtual ~cHBox_Execute();

	protected:

		// Widgets
		Gtk::Statusbar
			mSBar_Main;

		Gtk::Button
			mButton_Execute;

		Gtk::ToggleButton
			mTButton_Permission;
#ifndef PDFCHAIN_GTKMM_OLD
		Gtk::MenuButton
			mMButton;

		Gtk::Menu
			mMenu;

		Gtk::ImageMenuItem
			mIMItem_About ,
			mIMItem_Help ,
			mIMItem_Clear;

		Gtk::SeparatorMenuItem
			mMISeparator_A;
#else
		Gtk::Image
			mImg_About ,
			mImg_Help ,
			mImg_Clear;

		Gtk::Button
			mButton_About ,
			mButton_Help ,
			mButton_Clear;
#endif
};



/*** Window : Main ************************************************************/

class
cWindow_Main : public Gtk::Window
{
	public:
	
		// Constructor
		         cWindow_Main();
		virtual ~cWindow_Main();
		
		// Methodes
		void clear();
		
	protected:
	
		// Methodes
		void init();

		void confirmErrorCode( const int );

		// Signal Handler
		inline void onNotebook_Sections_switch_page( const Widget* , const guint );
		inline void onIMItem_About_activated();
		inline void onIMItem_Help_activated();
		inline void onIMItem_Clear_activated();
		inline void onTButton_Permission_toggled();
		inline void onButton_Execute_clicked();

		// Signal Connector
		sigc::connection mConnection_NotebookSections_signal_switch_page;

		// Derived Widgets
		cNotebook_Sections
			mNotebook_Sections;
			
		cSection_Permission
			mSection_Permission;
			
		cHBox_Execute
			mHBox_Execute;
			
		cMDialog_PdftkError
			mMDialog_PdftkError;
			
		cDialog_About
			mDialog_About;

		// Widgets
		Gtk::VBox
			mVBox_Main;
			
		Gtk::Alignment
			mAlign_Sections,
			mAlign_Permission,
			mAlign_Execute;
};


#endif

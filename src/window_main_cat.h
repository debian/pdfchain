/*
 * window_main_cat.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_CAT_H__
#define __PDFCHAIN_WINDOW_MAIN_CAT_H__

#include "pdfchain.h"
#include "store.h"
#include "store_cat.h"

class cCBox_OutputId;
class cPMenu_Cat;
class cTView_Cat;
class cToolbar_Cat;
class cSection_Cat;


/*** Combobox : Output ID *****************************************************/

class
cCBox_OutputId : public Gtk::ComboBox
{
	friend class cSection_Cat;

	public:
		         cCBox_OutputId();
		virtual ~cCBox_OutputId();
		
		void clear();
		
	protected:

		guchar vOutputId;
		
		Glib::RefPtr<cLStore_OutputId> rLStore;
		Gtk::CellRendererText          mCRText;
		cTMCRecord_Selection*          pTMCRecord;
		
		void onFileNumber_changed( guint );
};



/*** Popup Menu : Cat *********************************************************/

class
cPMenu_Cat : public Gtk::Menu
{
	friend class cTView_Cat;

	public:

		// Constructor
		         cPMenu_Cat( cTView_Cat* );
		virtual ~cPMenu_Cat();

		// Signal Emitter
		sigc::signal<void , Pdfchain::Id::EvenOdd> signalEvenOdd_activate() {
			return mSignal_EvenOdd_activate;
		}

		sigc::signal<void , Pdfchain::Id::Rotation> signalRotation_activate() {
			return mSignal_Rotation_activate;
		}

	protected:

		// Widgets
		Gtk::MenuItem
			mMItem_PagesAll,
			mMItem_PagesEven,
			mMItem_PagesOdd,
			mMItem_RotNorth,
			mMItem_RotEast,
			mMItem_RotSouth,
			mMItem_RotWest,
			mMItem_SelectAll,
			mMItem_SelectNone,
			mMItem_SelectInvert;

		Gtk::SeparatorMenuItem
			mMISeparator;

		// Signal Proxies 
		sigc::signal<void , Pdfchain::Id::EvenOdd> mSignal_EvenOdd_activate;
		sigc::signal<void , Pdfchain::Id::Rotation> mSignal_Rotation_activate;

		// Signal Handler
		void onMItem_EvenOdd_All_activate() {
			mSignal_EvenOdd_activate.emit( Pdfchain::Id::PAGES_ALL );		
			return;
		}

		void onMItem_EvenOdd_Even_activate() {
			mSignal_EvenOdd_activate.emit( Pdfchain::Id::PAGES_EVEN );		
			return;
		}

		void onMItem_EvenOdd_Odd_activate() {
			mSignal_EvenOdd_activate.emit( Pdfchain::Id::PAGES_ODD );		
			return;
		}

		void onMItem_Rotation_North_activate() {
			mSignal_Rotation_activate.emit( Pdfchain::Id::ROTATION_N );		
			return;
		}

		void onMItem_Rotation_East_activate() {
			mSignal_Rotation_activate.emit( Pdfchain::Id::ROTATION_E );		
			return;
		}

		void onMItem_Rotation_South_activate() {
			mSignal_Rotation_activate.emit( Pdfchain::Id::ROTATION_S );		
			return;
		}

		void onMItem_Rotation_West_activate() {
			mSignal_Rotation_activate.emit( Pdfchain::Id::ROTATION_W );		
			return;
		}
};



/*** Tree View : Cat **********************************************************/

class cTView_Cat : public Gtk::TreeView
{
	friend class cToolbar_Cat;
	friend class cSection_Cat;

	public:

		// Constructor
		         cTView_Cat( Gtk::Window& );
		virtual ~cTView_Cat();

		// Methodes
		void clear();

	protected:

		// Anchors
		Glib::RefPtr<cLStore_Cat>      rLStore_Cat;
		Glib::RefPtr<cLStore_EvenOdd>  rLStore_EvenOdd;
		Glib::RefPtr<cLStore_Rotation> rLStore_Rotation;

		cTMCRecord_Cat* pTMCRecord_Cat;

		// Extendet Widgets
		cPMenu_Cat mPMenu_Cat;

		// Widgets

		Gtk::Label
			mLabel_Add ,
			mLabel_Pages ,
			mLabel_EvenOdd ,
			mLabel_Rotation ,
			mLabel_SourceFile ,
			mLabel_SourcePath ,
			mLabel_Password ,
			mLabel_PageNumbers;

		Gtk::TreeViewColumn
			mTVColumn_Add,
			mTVColumn_Pages,
			mTVColumn_EvenOdd,
			mTVColumn_Rotation,
			mTVColumn_SourceFile,
			mTVColumn_SourcePath,
			mTVColumn_Password,
			mTVColumn_PageNumbers;
		
		Gtk::CellRendererToggle
			mCRToggle_Add;

		Gtk::CellRendererCombo
			mCRCombo_EvenOdd,
			mCRCombo_Rotation;

		Gtk::CellRendererText
			mCRText_Pages,
			mCRText_SourceFile,
			mCRText_SourcePath,
			mCRText_Password,
			mCRText_PageNumbers;

#ifdef PDFCHAIN_TEMP
		Gtk::TreeViewColumn
			mTVColumn_Id,					//TEMP
			mTVColumn_Tooltip,				//TEMP
			mTVColumn_Commands,				//TEMP
			mTVColumn_Colors;				//TEMP

		Gtk::CellRendererText
			mCRText_Id,						//TEMP
			mCRText_Tooltip,				//TEMP
			mCRText_Cmd_Handle,				//TEMP
			mCRText_Cmd_EvenOdd,			//TEMP
			mCRText_Cmd_Rotation;			//TEMP

		Gtk::CellRendererPixbuf
			mCRPixbuf_Color_Pages,			//TEMP
			mCRPixbuf_Color_PageNumbers;	//TEMP
#endif

		// Signal Handler - Mouse Button Event
		virtual bool on_button_press_event( GdkEventButton* );	// overwrite

		// Signal Handler - Tree View
		inline void onCRToggle_Add_toggled( const Glib::ustring& str_path ) {
			rLStore_Cat->toggleAdd( Gtk::TreeModel::Path( str_path ) );
			return;
		}
		
		inline void onCRText_Pages_edited(
			const Glib::ustring& str_path ,
			const Glib::ustring& str_pages )
		{
			rLStore_Cat->editPages( Gtk::TreeModel::Path( str_path ) , str_pages );
			return;
		}
		
		inline void onCRCombo_EvenOdd_changed(
			const Glib::ustring& str_path ,
			const Gtk::TreeModel::iterator& iter_combo )
		{
			cTMCRecord_Selection* pt_record = rLStore_EvenOdd->getTMCRecord();
			rLStore_Cat->changeEvenOdd( Gtk::TreeModel::Path( str_path ) ,
				static_cast<Glib::ustring>( (*iter_combo)[pt_record->mTMColumn_Label] ) ,
				static_cast<Glib::ustring>( (*iter_combo)[pt_record->mTMColumn_Command] ) );

			return;
		}
		
		inline void onCRCombo_Rotation_changed(
			const Glib::ustring& str_path ,
			const Gtk::TreeModel::iterator& iter_combo )
		{
			cTMCRecord_Selection* pt_record = rLStore_Rotation->getTMCRecord();
			rLStore_Cat->changeRotation( Gtk::TreeModel::Path( str_path ),
				static_cast<Glib::ustring>( (*iter_combo)[pt_record->mTMColumn_Label] ) ,
				static_cast<Glib::ustring>( (*iter_combo)[pt_record->mTMColumn_Command] ) );

			return;
		}
		
		inline void onCRText_Password_edited(
			const Glib::ustring& str_path ,
			const Glib::ustring& str_password )
		{
			rLStore_Cat->editPassword( Gtk::TreeModel::Path( str_path ) , str_password );
			return;
		}

		// Signal Handler - Toolbar
		inline void onToolButton_Up_clicked() {
			rLStore_Cat->moveRows_Up( get_selection() );	
			return;
		}
		
		inline void onToolButton_Down_clicked() {
			rLStore_Cat->moveRows_Down( get_selection() );
			return;
		}
		
		inline void onToolButton_Top_clicked() {
			rLStore_Cat->moveRows_ToTop( get_selection() );
			return;
		}
		
		inline void onToolButton_Bottom_clicked() {
			rLStore_Cat->moveRows_ToBottom( get_selection() );
			return;
		}
		
		inline void onToolButton_Add_clicked() {
			rLStore_Cat->addRows( get_selection() );
			return;
		}
		
		inline void onToolButton_Remove_clicked() {
			rLStore_Cat->removeRows( get_selection() );
			return;
		}
		
		inline void onToolButton_Copy_clicked() {
			rLStore_Cat->copyRows( get_selection() );
			return;
		}

		// Signal Handler - Popup Menu
		inline void onMItems_EvenOdd_activated( Pdfchain::Id::EvenOdd );
		inline void onMItems_Rotation_activated( Pdfchain::Id::Rotation );
		inline void onMItem_SelectAll_activated();
		inline void onMItem_SelectNone_activated();
		inline void onMItem_SelectInvert_activated();
};



/*** Toolbar : Cat ************************************************************/

class
cToolbar_Cat : public Gtk::Toolbar
{
	friend class cSection_Cat;

	public:
		         cToolbar_Cat( cTView_Cat* );
		virtual ~cToolbar_Cat();
		
		void clear();

	protected:

		cCBox_OutputId
			mCBox_OutputId;

		Gtk::ToolButton
			mToolButton_Add,
			mToolButton_Remove,
			mToolButton_Copy,
			mToolButton_Up,
			mToolButton_Down,
			mToolButton_Top,
			mToolButton_Bottom;

		Gtk::ToggleToolButton
			mTToolButton_Shuffle;

		Gtk::ToolItem
			mToolItem_OutputId;

		Gtk::SeparatorToolItem
			mSeparator_A,
			mSeparator_B,
			mSeparator_C;
};



/*** Section : Cat ************************************************************/

class
cSection_Cat : public Gtk::VBox
{
	public:
		         cSection_Cat( Gtk::Window& );
		virtual ~cSection_Cat();

		void clear();
		std::string createCommand();

	protected:

		Gtk::ScrolledWindow  mSWindow;
		cTView_Cat   mTView;
		cToolbar_Cat mToolbar;
		cFCDialog_SaveAs_Pdf mFCDialog_SaveAs;
		
		//void init();
};

#endif


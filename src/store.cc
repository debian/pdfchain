/*
 * store.cc
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "store.h"


/*** Tree Model Column Record : Selection *************************************/

// Constructor
cTMCRecord_Selection::cTMCRecord_Selection()
{
	add( mTMColumn_ID );
	add( mTMColumn_Label );
//	add( mTMColumn_Tooltip );
	add( mTMColumn_Command );
}


// Destructor
cTMCRecord_Selection::~cTMCRecord_Selection() {
#ifdef PDFCHAIN_TEST
		std::cout << std::endl << "cTMCRecord_Selection::~cTMCRecord_Selection()";	//TEST
#endif
}



/*** ListStore : Output ID ****************************************************/

// Constructor
cLStore_OutputId::cLStore_OutputId()
:
pTMCRecord( NULL )
{

	pTMCRecord = new cTMCRecord_Selection();
	set_column_types( *pTMCRecord );
	
	Gtk::TreeModel::iterator iter;
	
	iter = append();
	(*iter)[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ID_NEW;
	(*iter)[ pTMCRecord->mTMColumn_Label   ] = _("Create new ID");
	(*iter)[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ID_NEW;

	iter = append();
	(*iter)[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ID_FIRST;
	(*iter)[ pTMCRecord->mTMColumn_Label   ] = _("Keep ID of first PDF file");
	(*iter)[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ID_FIRST;

	iter = append();
	(*iter)[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ID_FINAL;
	(*iter)[ pTMCRecord->mTMColumn_Label   ] = _("Keep ID of final PDF file");
	(*iter)[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ID_FINAL;
}


// Destructor (virtual)
cLStore_OutputId::~cLStore_OutputId()
{
#ifdef PDFCHAIN_TEST
		std::cout << std::endl << "cLStore_OutputId::~cLStore_OutputId()";	//TEST
#endif

	if ( pTMCRecord != NULL ) {
		delete pTMCRecord;
		pTMCRecord = NULL;
	}
}


// Method (public, static) : create 
Glib::RefPtr<cLStore_OutputId>
cLStore_OutputId::create()
{
	return Glib::RefPtr<cLStore_OutputId>( new cLStore_OutputId() );
}


// Method (public) : get TreeModelColumnRecord
cTMCRecord_Selection*
cLStore_OutputId::getTMCRecord()
{
	return pTMCRecord;
}


// Method (public) : select
Gtk::TreeModel::iterator
cLStore_OutputId::select( guchar output_id )	// FIXME: use Pdfchain::Id::OutputId (enum) instead of guchar
{
	Gtk::TreeModel::iterator iter;
	
	if ( Pdfchain::Id::ID_KEEP == output_id )	append_KeepId();
	else										erase_KeepId();

	iter = children().begin();
	while ( children().end() != iter ) {
		if ( (*iter)[pTMCRecord->mTMColumn_ID] == output_id )
			break;
		iter++;
	}

	if ( children().end() == iter )
		iter = children().begin();

	return iter;
}


// Method (protected) : append KEEP ID
void
cLStore_OutputId::append_KeepId()
{
	Gtk::TreeModel::iterator iter = children().begin();

	iter = prepend();
	(*iter)[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ID_KEEP;
	(*iter)[ pTMCRecord->mTMColumn_Label   ] = _("Keep ID");
	(*iter)[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ID_KEEP;

	return;
}


// Method (protected) : erase KEEP ID
void
cLStore_OutputId::erase_KeepId()
{
	Gtk::TreeModel::iterator iter = children().begin();
	
	while ( children().end() != iter ) {
		if ( (*iter)[pTMCRecord->mTMColumn_ID] == Pdfchain::Id::ID_KEEP ) {
			iter = erase( iter );
			continue;
		}
		iter++;
	}

	return;
}


/*** ListStore : Even Odd *****************************************************/

// Constructor
cLStore_EvenOdd::cLStore_EvenOdd()
:
	pTMCRecord( NULL )
{
	pTMCRecord	= new cTMCRecord_Selection();
	set_column_types( *pTMCRecord );

	Gtk::TreeModel::Row	row;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::PAGES_ALL;
	row[ pTMCRecord->mTMColumn_Label   ] = _("all");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::PAGES_ALL;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::PAGES_EVEN;
	row[ pTMCRecord->mTMColumn_Label   ] = _("even");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::PAGES_EVEN;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::PAGES_ODD;
	row[ pTMCRecord->mTMColumn_Label   ] = _("odd");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::PAGES_ODD;
}


// Destructor (virtual)
cLStore_EvenOdd::~cLStore_EvenOdd()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cLStore_EvenOdd::~cLStore_EvenOdd()";	//TEST
#endif
	
	if ( pTMCRecord != NULL ) { delete pTMCRecord;	pTMCRecord = NULL; }
}


// Method (public, static) : create
Glib::RefPtr<cLStore_EvenOdd>
cLStore_EvenOdd::create()
{
	return Glib::RefPtr<cLStore_EvenOdd>( new cLStore_EvenOdd() );
}


// Method (public) : get TreeModelColumnRecord
cTMCRecord_Selection*
cLStore_EvenOdd::getTMCRecord()
{
	return pTMCRecord;
}



/*** ListStore : Rotation *****************************************************/

// Constructor
cLStore_Rotation::cLStore_Rotation()
:
	pTMCRecord( NULL )
{
	pTMCRecord	= new cTMCRecord_Selection();
	set_column_types( *pTMCRecord );

	Gtk::TreeModel::Row	row;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ROTATION_N;
	row[ pTMCRecord->mTMColumn_Label   ] = _("0°");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ROTATION_N;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ROTATION_E;
	row[ pTMCRecord->mTMColumn_Label   ] = _("90°");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ROTATION_E;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ROTATION_S;
	row[ pTMCRecord->mTMColumn_Label   ] = _("180°");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ROTATION_S;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::ROTATION_W;
	row[ pTMCRecord->mTMColumn_Label   ] = _("270°");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::ROTATION_W;
}


// Destructor (virtual)
cLStore_Rotation::~cLStore_Rotation()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cLStore_Rotation::~cLStore_Rotation()";	//TEST
#endif
	
	if ( pTMCRecord != NULL ) { delete pTMCRecord;	pTMCRecord = NULL; }
}


// Method (public, static) : create
Glib::RefPtr<cLStore_Rotation>
cLStore_Rotation::create()
{
	return Glib::RefPtr<cLStore_Rotation>( new cLStore_Rotation() );
}


// Method (public) : get TreeModelColumnRecord
cTMCRecord_Selection*
cLStore_Rotation::getTMCRecord()
{
	return pTMCRecord;
}



/*** ListStore : Counting Base ***********************************************/

// Constructor
cLStore_CountingBase::cLStore_CountingBase()
:
	pTMCRecord( NULL )
{
	pTMCRecord	= new cTMCRecord_Selection();
	set_column_types( *pTMCRecord );

	Gtk::TreeModel::Row	row;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::COUNT_OCT;
	row[ pTMCRecord->mTMColumn_Label   ] = _("Octal");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::COUNT_OCT;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::COUNT_DEC;
	row[ pTMCRecord->mTMColumn_Label   ] = _("Decimal");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::COUNT_DEC;

	row = *( this->append() );
	row[ pTMCRecord->mTMColumn_ID      ] = Pdfchain::Id::COUNT_HEX;
	row[ pTMCRecord->mTMColumn_Label   ] = _("Hexadecimal");
	row[ pTMCRecord->mTMColumn_Command ] = Pdfchain::Cmd::COUNT_HEX;
}


// Destructor (virtual)
cLStore_CountingBase::~cLStore_CountingBase()
{
#ifdef PDFCHAIN_TEST
	std::cout << std::endl << "cLStore_CountingBase::~cLStore_CountingBase()";	//TEST
#endif
	
	if ( pTMCRecord != NULL ) { delete pTMCRecord;	pTMCRecord = NULL; }
}


// Method (public, static) : create
Glib::RefPtr<cLStore_CountingBase>
cLStore_CountingBase::create()
{
	return Glib::RefPtr<cLStore_CountingBase>( new cLStore_CountingBase() );
}


// Method (public) : get TreeModelColumnRecord
cTMCRecord_Selection*
cLStore_CountingBase::getTMCRecord()
{
	return pTMCRecord;
}

/*
 * window_main_tool.h
 * Copyright (C) Martin Singer 2009-2013 <m_power3@users.sourceforge.net>
 * 
 * pdfchain is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * pdfchain is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PDFCHAIN_WINDOW_MAIN_TOOL_H__
#define __PDFCHAIN_WINDOW_MAIN_TOOL_H__

#include "pdfchain.h"
#include "dialog_filechooser.h"


/*** Section : Tool ***********************************************************/

class
cSection_Tool : public Gtk::VBox
{
	public:
		         cSection_Tool( Gtk::Window& );
		virtual ~cSection_Tool();

		void clear();
		std::string createCommand();
		
	protected:
	
		void init();

		Glib::RefPtr<Gtk::SizeGroup> rSGroup_FCButton;

		Gtk::HBox
			mHBox_SourceFile ,
			mHBox_UpdateInfo ,
			mHBox_FillForm;

		Gtk::ScrolledWindow
			mSWindow_Tool;

		Gtk::Table
			mTable_Tool;

		Gtk::Label
			mLabel_SourceFile ,
			mLabel_DumpDataFile ,
			mLabel_FdfFile;

		Gtk::RadioButton
			mRButton_Repair ,
			mRButton_UnpackFiles ,
			mRButton_Compress ,
			mRButton_Uncompress ,
			mRButton_DumpDataAnnots ,
			mRButton_DumpDataFields ,
			mRButton_DumpData ,
			mRButton_UpdateInfo ,
			mRButton_GenerateFdf ,
			mRButton_FillForm ,
			mRButton_Flatten ,
			mRButton_DropXfa;

		Gtk::RadioButtonGroup
			mRBGroup_Tool;

		Gtk::CheckButton
			mCButton_DumpDataFields_Utf8 ,
			mCButton_DumpData_Utf8 ,
			mCButton_UpdateInfo_Utf8 ,
			mCButton_FillForm_DropXfa ,
			mCButton_FillForm_Flatten ,
			mCButton_FillForm_Appearance;

		Gtk::Separator
			mHSeparator_A ,
			mHSeparator_B ,
			mHSeparator_C ,
			mHSeparator_D ,
			mVSeparator_FillForm;

		Gtk::Alignment
			mAlign_HSeparatorA ,
			mAlign_HSeparatorB ,
			mAlign_HSeparatorC ,
			mAlign_HSeparatorD;

		cFCButton_Pdf  mFCButton_SourceFile;
		cFCButton_Dump mFCButton_DumpDataFile;
		cFCButton_Fdf  mFCButton_FdfFile;
		
		cFCDialog_SelectFolder mFCDialog_SelectFolder;
		cFCDialog_SaveAs_Pdf   mFCDialog_SaveAs_Pdf;
		cFCDialog_SaveAs_Fdf   mFCDialog_SaveAs_Fdf;
		cFCDialog_SaveAs_Dump  mFCDialog_SaveAs_Dump;

		void onRButton_DumpDataFields_toggled() {
			mCButton_DumpDataFields_Utf8.set_sensitive( mRButton_DumpDataFields.get_active() );
			return;
		}

		void onRButton_DumpData_toggled() {
			mCButton_DumpData_Utf8.set_sensitive( mRButton_DumpData.get_active() );
			return;
		}
		
		void onRButton_UpdateInfo_toggled() {
			mCButton_UpdateInfo_Utf8.set_sensitive(	mRButton_UpdateInfo.get_active() );
			mLabel_DumpDataFile.set_sensitive(		mRButton_UpdateInfo.get_active() );
			mFCButton_DumpDataFile.set_sensitive(	mRButton_UpdateInfo.get_active() );
			return;
		}

		void onRButton_FillForm_toggled() {
			mLabel_FdfFile.set_sensitive(				mRButton_FillForm.get_active() );
			mFCButton_FdfFile.set_sensitive(			mRButton_FillForm.get_active() );
			mVSeparator_FillForm.set_sensitive(			mRButton_FillForm.get_active() );
			mCButton_FillForm_DropXfa.set_sensitive(	mRButton_FillForm.get_active() );
			mCButton_FillForm_Flatten.set_sensitive(	mRButton_FillForm.get_active() );
			mCButton_FillForm_Appearance.set_sensitive(	mRButton_FillForm.get_active() );
			return;
		}

		void onCButton_FillForm_Flatten_toggled() {
			if( mCButton_FillForm_Flatten.get_active() )
				mCButton_FillForm_Appearance.set_active( false );
			return;
		}

		void onCButton_FillForm_Appearance_toggled() {
			if( mCButton_FillForm_Appearance.get_active() )
				mCButton_FillForm_Flatten.set_active( false );
			return;
		}

};


#endif
